#!/bin/sh
# 获取当前脚本文件的相对路径
script_relative_path=$0

# 获取当前工作目录的绝对路径
working_directory=$(pwd)

composer_directory="$working_directory/vendor/autoload.php"

# 获取脚本文件的绝对路径
# script_absolute_path=$(cd $(dirname $script_relative_path) && pwd)/$(basename $script_relative_path)

# echo "脚本文件的绝对路径：$script_absolute_path"
if [ ! -f $composer_directory ]; then
    # 运行 composer install
    php  "$working_directory/composer.phar" install
fi

wait4x mysql root:0epbavnfpCR750t3Ms4Tw@tcp\(172.16.1.3:3306\)/lsv --timeout 120s -- /smproxy/bin/SMProxy start
/bin/sh

