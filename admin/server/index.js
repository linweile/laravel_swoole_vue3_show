const axios = require("axios");
const path = require("path");
const dotenv = require("dotenv");
const colors = require("picocolors");

const config = Object.assign(
	dotenv.config().parsed,
	dotenv.config({ path: path.resolve(process.cwd(), ".env." + process.env.NODE_ENV) }).parsed
);
axios
	.get(`${config.VITE_API_PROXY_URL ? config.VITE_API_PROXY_URL : config.VITE_API_URL}${config.VITE_PREFIX_API}/dev/check`)
	.then(function (response) {
		if (response.status === 200) {
			if (response.data.code !== 200) {
				console.log(response.config.url);
				console.log(response.data);
				return;
			}
			console.log(
				colors.yellow(process.env.NODE_ENV + "环境"),
				colors.yellow(" 账号:" + response.data.data.username),
				colors.yellow(" 密码:" + response.data.data.password)
			);
		} else {
			console.log(response.status);
			console.log(response.data);
			console.log(response.config.url);
		}
	})
	.catch(function (error) {
		console.log(error);
	});

