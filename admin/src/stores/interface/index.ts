import { Upload } from "@/api/interface/upload";
/* themeConfigProp */

export interface ThemeConfigProps {
	layout: string;
	primary: string;
	isDark: boolean;
	isGrey: boolean;
	isCollapse: boolean;
	isWeak: boolean;
	breadcrumb: boolean;
	breadcrumbIcon: boolean;
	tabs: boolean;
	tabsIcon: boolean;
	footer: boolean;
	maximize: boolean;
}

/* GlobalState */
export interface GlobalState {
	token: string;
	refreshToken: string;
	userInfo: {
		nickName: string;
		portrait: Portrait;
		username: string;
		realName: string;
		phone: string;
	};
	casting: Casting;
	assemblySize: string;
	language: string;
	themeConfig: ThemeConfigProps;
}

/* tabsMenuProps */
export interface TabsMenuProps {
	icon: string;
	title: string;
	path: string;
	name: string;
	close: boolean;
}

/* TabsState */
export interface TabsState {
	tabsMenuList: TabsMenuProps[];
}

/* AuthState */
export interface AuthState {
	routePath: string;
	authButtonList: {
		[key: string]: string[];
	};
	authMenuList: Menu.MenuOptions[];
}

/* keepAliveState */
export interface keepAliveState {
	keepAliveName: string[];
}

export interface StsState {
	oss: undefined | Upload.File.OssConfig;
	upload: undefined | Upload.File.UploadConfig;
	dir_prefix: undefined | Upload.File.DirPrefix;
	admins_id:undefined | string;
	timeOut: undefined | NodeJS.Timeout;
	capacity:undefined|number;
	max_capacity:undefined|number;
}
