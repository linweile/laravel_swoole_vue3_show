import { defineStore } from "pinia";
import { AuthState } from "@/stores/interface";
import { getFlatArr, getShowMenuList, getAllBreadcrumbList } from "@/utils/util";
import { homeRouter } from "@/routers/modules/staticRouter";
// AuthStore
export const AuthStore = defineStore({
	id: "AuthState",
	state: (): AuthState => ({
		// 当前页面的 router path，用来做按钮权限筛选
		routePath: "",
		// 按钮权限列表
		authButtonList: {},
		// 菜单权限列表
		authMenuList: []
	}),
	getters: {
		// 按钮权限列表
		authButtonListGet: state => state.authButtonList,
		// 后端返回的菜单列表 ==> 这里没有经过任何处理
		authMenuListGet: state => state.authMenuList,
		// 后端返回的菜单列表 ==> 左侧菜单栏渲染，需要去除 isHide == true
		showMenuListGet: state => getShowMenuList([...homeRouter, ...state.authMenuList]),
		// 扁平化之后的一维数组路由，主要用来添加动态路由
		flatMenuListGet: state => getFlatArr(state.authMenuList),
		// 所有面包屑导航列表
		breadcrumbListGet: state => getAllBreadcrumbList(state.authMenuList)
	},
	actions: {
		// getAuthButtonList
		setAuthButtonList(data: { [key: string]: string[] }) {
			this.authButtonList = data;
		},
		// getAuthMenuList
		setAuthMenuList(data: Menus.MenuOptions[]) {
			this.authMenuList = data;
		},
		clearAll(){
			this.authButtonList = { }
			// 菜单权限列表
			this.authMenuList = []
		},
		// setRouteName
		setRoutePath(path: string) {
			this.routePath = path;
		}
	}
});
