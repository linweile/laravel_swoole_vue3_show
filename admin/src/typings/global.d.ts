// import { RouteLocationRaw } from "vue-router";
// * Menu
declare namespace Menu {
	interface MenuOptions {
		path: string;
		name: string;
		component?: string | (() => Promise<any>);
		redirect?: string;
		meta: MetaProps;
		children?: MenuOptions[];
	}
	interface MetaProps {
		icon: string;
		title: string;
		activeMenu?: string;
		isLink?: string;
		isHide: boolean;
		isFull: boolean;
		isAffix: boolean;
		isKeepAlive: boolean;
	}
}
interface FormDataCreate {
	[key: string]: string | number | undefined | File;
}

// * Menu
declare namespace Menus {
	interface Basic {
		path: string;
		name: string;
		title: string;
		icon: string;
		redirect: string;
		isLink: string;
		isHide: 1 | 0;
		isFull: 1 | 0;
		isAffix: 1 | 0;
		isKeepAlive: 1 | 0;
	}
	interface ApiData extends Basic {
		component: string;
	}
	interface Dev extends ApiData {
		children?: Array<Static>;
	}
	interface Result extends Basic {
		component?: string | (() => Promise<any>);
		children?: Array<Result>;
	}

	interface MenuOptions {
		path: string;
		name: string;
		component?: string | (() => Promise<any>);
		redirect?: string;
		meta: MetaProps;
		children?: MenuOptions[];
	}
	interface MetaProps {
		icon: string;
		title: string;
		activeMenu?: string;
		isLink?: string;
		isHide: boolean;
		isFull: boolean;
		isAffix: boolean;
		isKeepAlive: boolean;
	}
}

// * Vite
declare type Recordable<T = any> = Record<string, T>;

declare type FileTypes = {
	type: string;
	name: string;
};

declare type Option = {
	title: string;
	value: number|string;
};
declare type Casting = "admins"|"dev";
declare type Portrait = {full_url:string;value:string};

declare interface ViteEnv {
	VITE_API_URL: string;
	VITE_PORT: number;
	VITE_OPEN: boolean;
	VITE_GLOB_APP_TITLE: string;
	VITE_DROP_CONSOLE: boolean;
	VITE_PROXY_URL: string;
	VITE_BUILD_GZIP: boolean;
	VITE_REPORT: boolean;
	VITE_API_PROXY_URL: string;
	VITE_PREFIX_API: string;
}
