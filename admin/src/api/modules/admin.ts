import {type Ids } from "@/api/interface/index";
import {type Admin } from "@/api/interface/admin";
import { PORT1 } from "@/api/config/servicePort";
import http from "@/api";
import md5 from "js-md5";
import { createForm } from "@/utils/util";

/**
 * @name 用户管理模块
 */

export const add = (params: Admin.Add) => {
	return http.post(
		PORT1 + `/admin/add`,
		createForm(params, (key: string, item: any) => (key === "password" ? md5(item) : item))
	);
};

export const edit = (params: Admin.Edit) => {
	return http.put(
		PORT1 + `/admin/edit`,
		createForm(params, (key: string, item: any) => (key === "password" ? md5(item) : item))
	);
};

export const editUserInfo = (params: Admin.EditUserInfo) => {
	return http.put(
		PORT1 + `/admin/editUserInfo`,
		params
	);
};
export const editPass = (params: Admin.EditPass) => {
	return http.put(
		PORT1 + `/admin/editPass`,
		params
	);
};

export const list = (params: Admin.PageReq) => {
	return http.get<Array<Admin.List>>(PORT1 + `/admin/list`, params);
};
export const status = (params: Admin.Status) => {
	return http.put(PORT1 + `/admin/status`, params);
};
export const del = (params: Ids) => {
	return http.delete(PORT1 + `/admin/del`, params);
};

export const operationLog = (params: Admin.OperationLog.PageReq) => {
	return http.get<Admin.OperationLog.PageRes>(PORT1 + `/admin/operation/log/list`, params);
};
