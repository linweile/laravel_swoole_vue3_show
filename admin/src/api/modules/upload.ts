import { Ids } from "@/api/interface/index";
import { Upload } from "@/api/interface/upload";
import { PORT1 } from "@/api/config/servicePort";
import http from "@/api";
import quietHttp from "@/api/quiet";

import { createForm } from "@/utils/util";
// import { ResPage } from "@/api/interface";
/**
 * @name 文件上传模块
 */
// * 图片上传
export const uploadDirImg = (params: FormData) => {
	return http.post<Upload.ResFileUrl>(PORT1 + `/file/upload/image`, params);
};

export const dirAdd = (params: Upload.Dir.Form) => {
	return http.post(PORT1 + `/upload/dir/add`, params);
};
export const dirEdit = (params: Upload.Dir.Form, dir_id: string) => {
	return http.put(PORT1 + `/upload/dir/edit`, { ...params, dir_id });
};
export const dirAll = (is_common: 1 | 0) => {
	return http.get<Upload.Dir.All[]>(PORT1 + `/upload/dir/all`, { is_common }, { headers: { noLoading: true } });
};
export const dirDel = (ids: Ids, is_common: 1 | 0) => {
	return http.delete(PORT1 + `/upload/dir/del`, { ...ids, is_common });
};

export const fileFirst = (params: Upload.File.FirstReq) => {
	return quietHttp.post<Upload.File.FirstRes>(PORT1 + `/upload/file/first`, params, { headers: { noLoading: true } });
};

export const schedule = (params: Upload.File.ScheduleReq) => {
	return http.put(PORT1 + `/upload/file/schedule`, params, { headers: { noLoading: true } });
};
export const followUp = (params: Upload.File.FollowUpReq) => {
	return http.post<Upload.File.FollowUpRes>(PORT1 + `/upload/file/followUp`, createForm(params));
};

export const getConfig = (loading:boolean = false) => {
	return http.get<Upload.File.GetConfig>(PORT1 + `/upload/file/getConfig`, {}, { headers: { noLoading: !loading  } });
};

export const stsMsg = (params: Upload.File.StsMsgReq) => {
	return http.get<Upload.File.StsMsg>(PORT1 + `/upload/file/stsMsg`, params);
};

export const fileList = (params:Upload.File.ListReq) =>{
	return http.get<Upload.File.FileResPage<Upload.File.ListRes>>(PORT1 + `/upload/file/list`, params, { headers: { noLoading: true } });
}

export const fileDel = (params: Upload.File.DelReq) =>{
	return http.delete(PORT1 + `/upload/file/del`, params);
}
