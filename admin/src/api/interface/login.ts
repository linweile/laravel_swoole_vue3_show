// * 登录模块
export namespace Login {
	export interface ReqLoginForm {
		username: string;
		password: string;
	}
	export interface ResLogin {
		access_token: string;
	}
	export interface ResAuthButtons {
		[key: string]: string[];
	}
	export interface CheckRes {
		info: {
			nickName: string;
			portrait: {
				full_url: string;
				value: string;
			};
			username:string;
			realName:string;
			phone:string;
		};
		role_id: number;
		menu: Array<Menus.Result>;
		buttons: { [key: string]: Array<string> };
	}
}
