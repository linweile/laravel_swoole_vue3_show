import { ReqPage } from "./index";
export namespace Admin {
	export interface Form {
		username: string;
		password?: string;
		nickName: string;
		realName: string;
		phone: string;
		status: 1 | 0;
		role_id: number;
	}
	export interface EditUserInfo {
		username: string;
		nickName: string;
		realName: string;
		phone: string;
		portrait?:string;
	}
	export interface EditPass {
		password: string;
		ori_password:string;
	}
	export namespace OperationLog {
		export interface PageReq extends ReqPage {
			nickName?: string;
			realName?: string;
			log_id?: string;
			opeartion_time?: string;
			menu_title?: string;
			ip?: string;
			router_path?: string;
		}
		export interface PageRes {
			auth_name: string;
			ip: string;
			log_id: number;
			menu_title: string;
			nickName: string;
			operation_time: string;
			realName: string;
			router_path: string;
		}
	}
	export interface Add extends Form {
		portrait?: string;
	}
	export interface Edit extends Form {
		portrait?:  string;
		admins_id: string;
	}
	export interface List extends Form {
		portrait: {
			full_url: string;
			value: string;
		};
		role:{
			roleName:string;
      role_id:number;
		},
		// role_name: string;
		admins_id: string;
	}
	export interface PageReq extends ReqPage {
		role_name?: string;
		role_id?: number;
		nickName?: string;
		realName?: string;
	}
	export interface Status {
		admins_id: string;
		status: 1 | 0;
	}
}
