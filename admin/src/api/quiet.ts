import { ResultEnum } from '@/enums/httpEnum'
import type { AxiosRequestConfig, AxiosResponse } from 'axios'
import { AxiosError } from 'axios'
import { BaseRequestHttp, config } from './helper/axios'
import { checkStatus } from './helper/checkStatus'
import { ElMessage } from "element-plus";

/**
 * pinia 错误使用说明示例
 * https://github.com/vuejs/pinia/discussions/971
 * https://github.com/vuejs/pinia/discussions/664#discussioncomment-1329898
 * https://pinia.vuejs.org/core-concepts/outside-component-usage.html#single-page-applications
 */

class RequestHttp extends BaseRequestHttp {
	public constructor(config: AxiosRequestConfig) {

		const response = (response: AxiosResponse) => {}
		const error = (error: AxiosError) => Promise.reject(error);
		// 实例化axios
		super(config, response, error);
	}
}

export default new RequestHttp(config);
