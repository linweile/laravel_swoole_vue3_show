import axios, { AxiosInstance, AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";
import { showFullScreenLoading, tryHideFullScreenLoading } from "@/config/serviceLoading";
import { ResultData } from "@/api/interface";
import { ResultEnum, Header } from "@/enums/httpEnum";
import { checkStatus } from "./checkStatus";
import { ElMessage } from "element-plus";
import { GlobalStore } from "@/stores";
import { AuthStore } from "@/stores/modules/auth";
import { LOGIN_URL } from "@/config/config";
import { openEln } from "@/config/elnotification";

import router from "@/routers";

/**
 * pinia 错误使用说明示例
 * https://github.com/vuejs/pinia/discussions/971
 * https://github.com/vuejs/pinia/discussions/664#discussioncomment-1329898
 * https://pinia.vuejs.org/core-concepts/outside-component-usage.html#single-page-applications
 */

export const config = {
	// 默认地址请求地址，可在 .env 开头文件中修改
	baseURL: import.meta.env.VITE_API_URL as string,
	// 设置超时时间（10s）
	timeout: ResultEnum.TIMEOUT as number,
	// 跨域时候允许携带凭证
	withCredentials: true
};

export abstract class BaseRequestHttp {
	service: AxiosInstance;
	public constructor(config: AxiosRequestConfig, responseFunc: (response: AxiosResponse) => any, errorFunc: (error: AxiosError) => any = ()=>{}) {
		// 实例化axios
		this.service = axios.create(config);

		/**
		 * @description 请求拦截器
		 * 客户端发送请求 -> [请求拦截器] -> 服务器
		 * token校验(JWT) : 接受服务器返回的token,存储到vuex/pinia/本地储存当中
		 */
		this.service.interceptors.request.use(
			(config: AxiosRequestConfig) => {
				const globalStore = GlobalStore();
				const authStore = AuthStore();

				// * 如果当前请求不需要显示 loading,在 api 服务中通过指定的第三个参数: { headers: { noLoading: true } }来控制不显示loading，参见loginApi
				config.headers!.noLoading || showFullScreenLoading();
				const token: any = {};
				token[Header.LOGIN_KEY] = globalStore.token;
				token[Header.REFRESH_KEY] = globalStore.refreshToken;
				token[Header.ROUTER_PATH] = authStore.routePath;
				return { ...config, headers: { ...config.headers, ...token } };
			},
			(error: AxiosError) => {
				return Promise.reject(error);
			}
		);

		/**
		 * @description 响应拦截器
		 *  服务器换返回信息 -> [拦截统一处理] -> 客户端JS获取到信息
		 */
		this.service.interceptors.response.use(
			(response: AxiosResponse) => {
				const { data, headers } = response;
				const globalStore = GlobalStore();
				// * 在请求结束后，并关闭请求 loading
				tryHideFullScreenLoading();
				// * 登陆失效（code == 599）

				if (data.code == ResultEnum.OVERDUE) {
					// ElMessage.error(data.msg);
					openEln(data.msg);
					globalStore.setToken("");
					router.replace(LOGIN_URL);
					return Promise.reject(data);
				}

				// * 全局错误信息拦截（防止下载文件得时候返回数据流，没有code，直接报错）
				// if (data.code && data.code !== ResultEnum.SUCCESS) {
				// 	ElMessage.error(data.msg);
				// 	return Promise.reject(data);
				// }

				let error = responseFunc(response);
        
				if (error instanceof Promise)	return error;
				
				
		

				if (headers[Header.LOGIN_KEY]) globalStore.setToken(headers[Header.LOGIN_KEY]); //登录token
				if (headers[Header.REFRESH_KEY]) globalStore.setRefreshToken(headers[Header.REFRESH_KEY]); //刷新token

				// * 成功请求（在页面上除非特殊情况，否则不用处理失败逻辑）
				return data;
			},
			async (error: AxiosError) => {
				return errorFunc(error);
			}
		);
	}

	// * 常用请求方法封装
	get<T = {}>(url: string, params?: object, _object = {}): Promise<ResultData<T>> {
		return this.service.get(url, { params, ..._object });
	}
	post<T = {}>(url: string, params?: object, _object = {}): Promise<ResultData<T>> {
		return this.service.post(url, params, _object);
	}
	put<T = {}>(url: string, params?: object, _object = {}): Promise<ResultData<T>> {
		return this.service.put(url, params, _object);
	}
	delete<T = {}>(url: string, params?: any, _object = {}): Promise<ResultData<T>> {
		return this.service.delete(url, { params, ..._object });
	}
	download(url: string, params?: object, _object = {}): Promise<BlobPart> {
		return this.service.post(url, params, _object);
	}
}

// export default new RequestHttp(config);
