import { ElNotification } from "element-plus";

let nowEln: boolean = false;
export const openEln = ( msg: string) => {
	if (!nowEln) {
		nowEln = true;
		ElNotification({
			title:'请重新登录' ,
			message: msg,
			type: "warning",
			duration: 3000,
			onClose: () => nowEln = false,
		});
	}
}
