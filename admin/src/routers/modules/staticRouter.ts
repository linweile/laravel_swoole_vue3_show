import { RouteRecordRaw } from "vue-router";
import { HOME_URL, LOGIN_URL } from "@/config/config";
import devRouterJson from "./json/devRouter.json";
import homeRouterJson from "./json/homeRouter.json";
import commonRouterJson from "./json/commonRouter.json";
/**
 * staticRouter(静态路由)
 */
export const staticRouter: RouteRecordRaw[] = [
	{
		path: "/",
		redirect: HOME_URL
	},
	{
		path: LOGIN_URL,
		name: "login",
		component: () => import("@/views/login/index.vue"),
		meta: {
			title: "登录"
		}
	},

	{
		path: "/layout",
		name: "layout",
		component: () => import("@/layouts/index.vue"),
		// component: () => import("@/layouts/indexAsync.vue"),
		redirect: HOME_URL,
		children: []
	}
];
/**
 * homeRouter(首页) 在头部添加栏目 固定在tabs
 */
export const homeRouter: Menus.MenuOptions[] = homeRouterJson;

/**
 * commonRouter(素材库) 在头部添加栏目 
 */
export const commonRouter: Menus.MenuOptions[] = commonRouterJson;

/**
 * devRouter(开发者)
 */
export const devRouter: Menus.MenuOptions[] = devRouterJson;

/**
 * errorRouter(错误页面路由)
 */
export const errorRouter:RouteRecordRaw[] = [
	{
		path: "/403",
		name: "403",
		component: () => import("@/components/ErrorMessage/403.vue"),
		meta: {
			title: "403页面"
		}
	},
	{
		path: "/404",
		name: "404",
		component: () => import("@/components/ErrorMessage/404.vue"),
		meta: {
			title: "404页面"
		}
	},
	{
		path: "/500",
		name: "500",
		component: () => import("@/components/ErrorMessage/500.vue"),
		meta: {
			title: "500页面"
		}
	}
];

/**
 * notFoundRouter(找不到路由)
 */
export const notFoundRouter = {
	path: "/:pathMatch(.*)*",
	name: "notFound",
	redirect: { name: "404" }
};
