import mitt from "mitt";
type Events = {
  loadImageFromFile: File;
  loadImageFromURL: string;
  openThemeDrawer: void;
	uploadFile: { is_common: 1 | 0, dir_id: string  , dir_link:string };
	openImg: { imgArr: Array<{ title: string, url: string }>, showIndex?: number };
	uploadFirstEvent: { file: File };
	uploadSuccess: void;
};
const mittBus = mitt<Events>();

export default mittBus;
