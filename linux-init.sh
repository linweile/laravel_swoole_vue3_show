#!/bin/bash
#linux 系统下的需要修改的权限命令
cp ./lumen/.env.production ./lumen/.env
cp ./lumen/supervisord.conf.production ./lumen/supervisord.conf
chmod 400 ./volumes/mongo/key/keyFile.key
chmod 400 ./volumes/mongo_replica/key/keyFile.key
chmod +x ./volumes/mysql/start.sh
chmod +x ./volumes/mongo/start.sh
chmod +x ./volumes/mongo_replica/start.sh
chmod +x ./volumes/rabbitmq/start.sh
chmod +x ./volumes/memcached/start.sh
chmod -R 777 ./volumes/rabbitmq/logs



