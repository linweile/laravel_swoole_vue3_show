const chokidar = require('chokidar');
const { exec } = require('child_process');
const path = require('path')

var timer = undefined;
var first = true;
var first_timer = null;
var swooleDir = path.resolve(__dirname,'../');
console.log(swooleDir);


chokidar.watch(swooleDir, {
    ignored: [`${swooleDir}/vendor/.`, `${swooleDir}/node_modules/.`,`${swooleDir}/public/.` ,`${swooleDir}/storage/.`]
}).on('all', (event, path) => {
    if (first) {
        clearTimeout(first_timer)
        first_timer = setTimeout(() => {
            first = false;
            console.log('watch start');
        }, 3000)
    } else {
        console.log(event+':'+path);
        clearTimeout(timer)
        timer = setTimeout(() => {
            exec('cd '+ swooleDir +'  &&  php artisan swoole:http reload', {}, () => {
                console.log('reload')
            })
        }, 1000)
    }
});
