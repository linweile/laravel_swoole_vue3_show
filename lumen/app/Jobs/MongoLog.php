<?php
namespace App\Jobs;

use App\Models\Mongo\Server\ErrorLog;
use Illuminate\Support\Facades\Log;

class MongoLog extends Job
{




    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        public readonly string  $date,     // ['database' => 数据库名 'table' => 表名 'func' => 操作名 'params': 传入参数]
        public readonly array   $params,   // ['database' => 数据库名 'table' => 表名 'func' => 操作名 'params': 传入参数]

    ) {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

            try {
                //加入mongodb
                ErrorLog::create( $this->date )->insertOne($this->params);

            } catch (\Exception $e) {
                //mongodb 错误时加入记事本
                Log::channel('systemerror')->error(json_encode(  [ 'data' => $this->params , 'mongo_error' => $e->getMessage()  ] , JSON_UNESCAPED_UNICODE ));

            }
    }
}
