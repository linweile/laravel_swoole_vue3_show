<?php
namespace App\Jobs;

use App\Common\Helper;
use App\Models\Mongo\Server\ErrorLog;
use App\Common\Http\Basics\Error;
use Illuminate\Support\Facades\Log;
// use App\Models\Mongo\BaseMongo;
class MongoIndexReset extends Job
{

    public $delay = 20; // 延迟执行时间，单位为秒

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        public readonly string  $mongoServerName,
        public readonly int|string|null $tableAfter,
        public readonly int|string $dbAfter,

    ) {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
            try {
               $this->mongoServerName::resetIndex($this->tableAfter,$this->dbAfter);  //重置索引
            } catch (\Exception $e) {
                //mongodb 错误时加入记事本
                Log::channel('mongo_index')->error(json_encode(  ['line' => $e->getLine() ,'file' => $e->getFile() , 'error' => $e->getMessage() ,'mongoServerName' => $this->mongoServerName , 'tableAfter' => $this->tableAfter , 'dbAfter' => $this->tableAfter ] , JSON_UNESCAPED_UNICODE ));
            }
    }
}
