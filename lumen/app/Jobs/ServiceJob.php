<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Log;

class ServiceJob extends Job
{




    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        public readonly array   $class_func,      // 类名
        public readonly array   $handle,            // 操作 array(['func' => string , 'params' => array( 111,222) ])
    ) {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {

            $class =  call_user_func(array($this->class_func[0], $this->class_func[1]), ...(isset($this->class_func[2]) && array_is_list($this->class_func[2]) ?   $this->class_func[2] : []));

            foreach ($this->handle as $val) {
                ['func' => $func] = $val;
                $class = $class->$func(...(!isset($val['params']) || $val['params'] === null ? [] : $val['params']));
            }

        } catch (\Exception $e) {
            Log::channel('servicejob')->error(json_encode(['date' => date('Y-m-d H:i:s'), 'line' => $e->getLine(), 'error' => $e->getMessage(), 'class_func' => $this->class_func, 'handle' => $this->handle], JSON_UNESCAPED_UNICODE));
        }
    }
}
