<?php

namespace App\Jobs;


use App\Models\Mysql\Orm\Lsv\AdminsLog;


class AdminOperation extends Job
{




    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        public readonly array   $params,            //请求参数
        public readonly int     $admins_id,         //集合名称
        public readonly string  $api,               //请求的api
        public readonly string  $auth_name,         //权限名称
        public readonly string  $router_path,       //发请求的页面地址
        public readonly string  $ip,                //ip地址
        public readonly array   $sql,               //所有mysql语句
        public readonly string  $operation_time,    //操作时间
        public readonly string  $menu_title,        //栏目标题
        public readonly array   $enum               //枚举name
    )
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       $model = new AdminsLog;
       $model->sql = $this->sql;
       $model->params = $this->params;
       $model->admins_id = $this->admins_id;
       $model->api =  $this->api;
       $model->auth_name =  $this->auth_name;
       $model->router_path = $this->router_path;
       $model->ip = $this->ip;
       $model->operation_time = $this->operation_time;
       $model->menu_title = $this->menu_title;
       $model->enum = $this->enum;
       $model->save();
    }
}
