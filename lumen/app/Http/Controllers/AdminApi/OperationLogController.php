<?php

namespace App\Http\Controllers\AdminApi;

use App\Http\Requests\AdminApi\OperationLogReq;
use App\Modules\Admin\Dev\OperationLogModule as Module;

class OperationLogController extends BaseController
{

    /* 列表 */
    public function list(OperationLogReq $request)
    {
        return $this->loadModule(Module::class, $request, function ($params) {
            if (isset($params['api'])) $params['api']   =  substr($params['api'], 0, 1) === '/' ? substr($params['api'], 1) : $params['api'];
            return $params;
        });
    }

    public function adminlist(OperationLogReq $request)
    {
        return $this->loadModule(
            [Module::class, 'list'],
            $request,
            null,
            array(
                [
                    'admins_log.log_id',
                    'admins_log.auth_name',
                    'admins_log.ip',
                    'admins_log.menu_title',
                    'admins_log.operation_time',
                    'admins.nickName',
                    'admins.realName'
                ]
            )
        );
    }
}
