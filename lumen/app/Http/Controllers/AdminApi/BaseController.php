<?php

namespace App\Http\Controllers\AdminApi;

use App\Common\Enum\{
    Header,
    Attributes
};
use App\Jobs\ServiceJob;
use Illuminate\Http\Request;
use App\Services\Admin\Operation;

use App\Modules\BaseModule;
use Illuminate\Support\Facades\DB;
use App\Common\Enum\Http;
use App\Common\Helper;

abstract class BaseController extends \App\Http\Controllers\Controller
{
    public array $sql  = [];  //贮存sql的记录
    public int $is_addLog = 0;  //是否加入日志
    /**
     * @description:   对应传进来的类 -> 路由动作的方法名 在 App\Modules\BaseModule 里解析各种参数
     * @param {*} $moduleClass    moudules文件夹里的类
     * @param {Request} $request
     * @param {null} $formatFunc
     * @param {array} $funcParams
     * @return {*}
     */
    public final  function loadModule(string|array $moduleClass, Request $request, \Closure|null $formatFunc = null, array $funcParams = [])
    {

        $this->watchDb($request);
        return  $this->loadModules($moduleClass, $request, $formatFunc, $funcParams,$this->addAdminLog(...));
    }

    public final function watchDb(Request $request){
        $this->is_addLog = $request->get(Attributes::ADD_LOG->value, 0);
         // * 监听sql语句(排除查询) 加入日志 *
        if($this->is_addLog) DB::listen(  fn ($sql) =>   $this->sql[] = ['sql' => $sql->sql,'params' => $sql->bindings,'time' => $sql->time] );
    }

    public final function addAdminLog(BaseModule $module, Request $request)
    {
        $loginInfo = $request->get(Attributes::LOGIN_INFO->value, []);

        if (
            !empty($loginInfo) &&
            $module->result->getHttpVal() === Http::SUCCESS->value &&
            $module->result->getTrueCodeName() === 'SUCCESS' &&
            $this->is_addLog &&
            isset($loginInfo['admins_id'])
        ) {
            // (new Operation(
            //     $request->input(),
            //     $loginInfo['admins_id'],
            //     $request->path(),
            //     $request->get(Attributes::AUTH_NAME->value, ""),
            //     $request->header(Header::ADMIN_ROUTER_PATH->value, ""),
            //     $request->ip(),
            //     $this->sql,
            //     date('y-m-d H:i:s'),
            //     $request->get(Attributes::MENU_TITLE->value, ""),
            //     [
            //         'class' =>  $module->result->getHttpEnum()::class,
            //         'name' =>$module->result->getHttpName(),
            //         'trueCodeName' => $module->result->getTrueCodeName(),
            //         'code' => $module->result->getHttpVal(),
            //     ]
            // ))->handle();
            dispatch((new ServiceJob(
                [
                Operation::class,
                'async',
                [
                $request->input(),
                $loginInfo['admins_id'],
                $request->path(),
                $request->get(Attributes::AUTH_NAME->value, ""),
                $request->header(Header::ADMIN_ROUTER_PATH->value, ""),
                Helper::getRealIp($request),
                $this->sql,
                date('y-m-d H:i:s'),
                $request->get(Attributes::MENU_TITLE->value, ""),
                [
                    'class' =>  $module->result->getHttpEnum()::class,
                    'name' =>$module->result->getHttpName(),
                    'trueCodeName' => $module->result->getTrueCodeName(),
                    'code' => $module->result->getHttpVal(),
                ]
                ]
            ],
               array( ['func' => 'handle'] )
            ))->onConnection('rabbitmq')->onQueue('operation_log'));
            // dispatch(new AdminOperation(
            //     $request->input(),
            //     $loginInfo['admins_id'],
            //     $request->path(),
            //     $request->get(Attributes::AUTH_NAME->value, ""),
            //     $request->header(Header::ADMIN_ROUTER_PATH->value, ""),
            //     $request->ip(),
            //     $this->sql,
            //     date('y-m-d H:i:s'),
            //     $request->get(Attributes::MENU_TITLE->value, ""),
            //     [
            //         'class' =>  $module->result->getHttpEnum()::class,
            //         'name' =>$module->result->getHttpName(),
            //         'trueCodeName' => $module->result->getTrueCodeName(),
            //         'code' => $module->result->getHttpVal(),
            //     ]
            // )->onConnection('rabbitmq')->onQueue('operation_log'));     //加入操作日志

            // operation_log
        }
    }
}
