<?php

namespace App\Http\Controllers\AdminApi;
use App\Http\Requests\AdminApi\AdminReq;
use App\Modules\Admin\AdminModule as Module;

class AdminController extends BaseController
{
    /* 后台登录 */
    public function login(AdminReq $request){  return $this->loadModule(Module::class,$request); }

    /* 检查登录状态 */
    public function checkLogin(AdminReq $request){ return $this->loadModule(Module::class,$request); }

    /**  修改自己的信息 */
    public function editUserInfo(AdminReq $request) {  return $this->loadModule(Module::class, $request);}

    /**  修改自己的密码 */
    public function editPass(AdminReq $request) {  return $this->loadModule(Module::class, $request);}

    /**  列表 */
    public function list(AdminReq $request){ return $this->loadModule(Module::class, $request); }

    /**  添加 */
    public function add(AdminReq $request){ return $this->loadModule(Module::class, $request); }

    /**  删除 */
    public function del(AdminReq $request){ return $this->loadModule(Module::class, $request); }

    /**  修改 */
    public function edit(AdminReq $request){ return $this->loadModule(Module::class, $request); }

    /**  修改状态 */
    public function status(AdminReq $request){ return $this->loadModule(Module::class, $request); }
}
