<?php

namespace App\Http\Controllers\AdminApi\Dev;

use App\Http\Requests\AdminApi\Dev\MenuApiReq;
use App\Modules\Admin\Dev\MenuApiModule as Module;

class MenuApiController extends \App\Http\Controllers\AdminApi\BaseController
{
    /* 添加 */
    public function add(MenuApiReq $request)  { return $this->loadModule(Module::class, $request); }
    /* 编辑 */
    public function edit(MenuApiReq $request)  { return $this->loadModule(Module::class, $request); }
    /* 是否添加日志 */
    public function addLog(MenuApiReq $request)  { return $this->loadModule(Module::class, $request); }
    /* 删除 */
    public function del(MenuApiReq $request)  { return $this->loadModule(Module::class, $request); }
    /* 列表 */
    public function list(MenuApiReq $request)  { return $this->loadModule(Module::class, $request); }

}
