<?php

namespace App\Http\Controllers\AdminApi\Dev;

use App\Http\Requests\AdminApi\Dev\ErrorLogReq;
use App\Modules\Admin\Dev\ErrorLogModule as Module;

class ErrorLogController extends BaseController
{
    /* 错误日志 */
    public function list(ErrorLogReq $request)
    {
        return $this->loadModule(Module::class, $request, function ($params) {
            $params['ymd']      = isset( $params['ymd']  ) ?  str_ireplace('-', "", $params['ymd'])  : date('Ymd') ;
            $params['pageSize'] = (int)$params['pageSize'];
            $params['pageNum']  = (int)$params['pageNum'];
            return $params;
        });
    }


    public function date(ErrorLogReq  $request){
        return $this->loadModule(Module::class, $request);
    }




}
