<?php

namespace App\Http\Controllers\AdminApi\Dev;
use App\Http\Requests\AdminApi\Dev\DevReq;
use App\Modules\Admin\Dev\AccountModule as Module;

class AccountController extends BaseController
{
    /* 检查开发者账号密码 */
    public function check(DevReq $request){  return $this->loadModule(Module::class,$request); }


}
