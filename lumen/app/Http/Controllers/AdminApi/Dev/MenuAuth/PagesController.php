<?php

namespace App\Http\Controllers\AdminApi\Dev\MenuAuth;

use App\Http\Requests\AdminApi\Dev\MenuAuth\PagesReq;
use App\Modules\Admin\Dev\MenuAuth\PagesModule as Module;

class PagesController extends \App\Http\Controllers\AdminApi\Dev\BaseController
{
    /* 添加 */
    public function add(PagesReq $request)  { return $this->loadModule(Module::class, $request); }
    /* 删除 */
    public function del(PagesReq $request)  { return $this->loadModule(Module::class, $request); }
    /* 编辑 */
    public function edit(PagesReq $request) { return $this->loadModule(Module::class, $request); }
}
