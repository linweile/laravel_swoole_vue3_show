<?php

namespace App\Http\Controllers\AdminApi\Dev\MenuAuth;

use App\Http\Requests\AdminApi\Dev\MenuAuth\ApiReq;
use App\Modules\Admin\Dev\MenuAuth\ApiModule as Module;

class ApiController extends \App\Http\Controllers\AdminApi\Dev\BaseController
{
    /* 添加 */
    public function add(ApiReq $request)  { return $this->loadModule(Module::class, $request); }
    /* 删除 */
    public function del(ApiReq $request)  { return $this->loadModule(Module::class, $request); }
    /* 编辑 */
    public function edit(ApiReq $request) { return $this->loadModule(Module::class, $request); }
    /* 是否添加日志 */
    public function addLog(ApiReq $request) { return $this->loadModule(Module::class, $request); }
}
