<?php

namespace App\Http\Controllers\AdminApi\Dev\MenuAuth;

use App\Http\Requests\AdminApi\Dev\MenuAuth\ButtonsReq;
use App\Modules\Admin\Dev\MenuAuth\ButtonsModule as Module;

class ButtonsController extends \App\Http\Controllers\AdminApi\Dev\BaseController
{
    /* 添加 */
    public function add(ButtonsReq $request)  { return $this->loadModule(Module::class, $request); }
    /* 删除 */
    public function del(ButtonsReq $request)  { return $this->loadModule(Module::class, $request); }
    /* 编辑 */
    public function edit(ButtonsReq $request) { return $this->loadModule(Module::class, $request); }
}
