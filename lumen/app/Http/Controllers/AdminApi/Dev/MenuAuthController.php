<?php

namespace App\Http\Controllers\AdminApi\Dev;

use App\Http\Requests\AdminApi\Dev\MenuAuthReq;
use App\Modules\Admin\Dev\MenuAuthModule as Module;

class MenuAuthController extends BaseController
{
    /* 添加 */
    public function add(MenuAuthReq $request)  { return $this->loadModule(Module::class, $request); }
    /* 列表 */
    public function list(MenuAuthReq $request)  { return $this->loadModule(Module::class, $request); }
    /* 删除 */
    public function del(MenuAuthReq $request)  { return $this->loadModule(Module::class, $request); }
    /* 编辑 */
    public function edit(MenuAuthReq $request) { return $this->loadModule(Module::class, $request); }


}
