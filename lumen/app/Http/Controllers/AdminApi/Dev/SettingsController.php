<?php

namespace App\Http\Controllers\AdminApi\Dev;
use App\Http\Requests\AdminApi\Dev\SettingsReq;
use App\Modules\Admin\Dev\SettingsModule as Module;
class SettingsController extends BaseController
{
    /* 设置配置 */
    public function setItem(SettingsReq $request){ return $this->loadModule(Module::class,$request); }

    /* 获取配置 */
    public function getItem(SettingsReq $request){ return $this->loadModule(Module::class,$request); }


}
