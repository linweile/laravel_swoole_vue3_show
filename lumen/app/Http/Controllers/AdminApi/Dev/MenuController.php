<?php

namespace App\Http\Controllers\AdminApi\Dev;

use App\Http\Requests\AdminApi\Dev\MenuReq;
use App\Modules\Admin\Dev\MenuModule as Module;
class MenuController extends BaseController
{

    /* 添加 */
    public function add(MenuReq $request)  { return $this->loadModule(Module::class, $request); }
    /* 列表 */
    public function all(MenuReq $request)  { return $this->loadModule(Module::class, $request); }
    /* 删除 */
    public function del(MenuReq $request)  { return $this->loadModule(Module::class, $request); }
    /* 编辑 */
    public function edit(MenuReq $request) { return $this->loadModule(Module::class, $request); }


}
