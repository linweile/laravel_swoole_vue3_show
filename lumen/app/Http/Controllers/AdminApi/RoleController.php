<?php

namespace App\Http\Controllers\AdminApi;
use App\Http\Requests\AdminApi\RoleReq;
use App\Modules\Admin\RoleModule as Module;


class RoleController extends BaseController
{
    /**  树数据 */
    public function treeData(RoleReq $request){ return $this->loadModule(Module::class, $request); }

    /**  列表 */
    public function list(RoleReq $request){ return $this->loadModule(Module::class, $request); }

    /**  添加 */
    public function add(RoleReq $request){ return $this->loadModule(Module::class, $request); }

    /**  删除 */
    public function del(RoleReq $request){ return $this->loadModule(Module::class, $request); }

    /**  修改 */
    public function edit(RoleReq $request){ return $this->loadModule(Module::class, $request); }

    /**  设置权限 */
    public function set(RoleReq $request){ return $this->loadModule(Module::class, $request); }

    /**  所有 */
    public function all(RoleReq $request){ return $this->loadModule(Module::class, $request); }


}
