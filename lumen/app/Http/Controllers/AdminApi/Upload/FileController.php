<?php

namespace App\Http\Controllers\AdminApi\Upload;

use App\Http\Requests\AdminApi\Upload\FileReq;
use App\Modules\Admin\Upload\FileModule as Module;

class FileController extends \App\Http\Controllers\AdminApi\BaseController
{
    /* 第一次上传文件 */
    public function first(FileReq $request)
    {
        return $this->loadModule(Module::class, $request);
    }

    /* 修改文件上传进度 */
    public function schedule(FileReq $request)
    {
        return $this->loadModule(Module::class, $request);
    }

    /* 后续上传文件 */
    public function followUp(FileReq $request)
    {
        return $this->loadModule(Module::class, $request);
    }

    /* 获取配置 */
    public function getConfig(FileReq $request)
    {
        return $this->loadModule(Module::class, $request);
    }

    /* 文件列表 */
    public function list(FileReq $request)
    {
        return $this->loadModule(Module::class, $request, function ($params) {
            if (isset($params['file_type'])) {
                $file_type_arr  =    explode(',', $params['file_type']);
                $params['file_type'] =   array_filter($file_type_arr, fn ($item) => $item);
            } else {
                $params['file_type'] = [];
            }
            $params['keyWord'] = $params['keyWord'] ?? '';
            return $params;
        });
    }

    public function del(FileReq $request){
        return $this->loadModule(Module::class, $request , function($params){
            $params['real_del'] = (int)$params['real_del'];
            return $params;
        });
    }




    public function getStsConfig(FileReq $request)
    {
        return $this->loadModule(Module::class, $request);
    }

    public function stsMsg(FileReq $request)
    {
        return $this->loadModule(Module::class, $request);
    }






}
