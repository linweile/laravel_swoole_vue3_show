<?php

namespace App\Http\Controllers\AdminApi\Upload;

use App\Http\Requests\AdminApi\Upload\ScheduleReq;
use App\Modules\Admin\Upload\FileModule as Module;

class ScheduleController extends \App\Http\Controllers\AdminApi\BaseController
{
    /* 第一次上传文件 */
    public function ready(ScheduleReq $request)
    {
        return $this->loadModule(Module::class, $request);
    }

    // /* 后续上传文件 */
    // public function followUp(FileReq $request)
    // {
    //     return $this->loadModule(Module::class, $request);
    // }

    // /* 获取配置 */
    // public function getConfig(FileReq $request)
    // {
    //     return $this->loadModule(Module::class, $request);
    // }

    // public function getStsConfig(FileReq $request)
    // {
    //     return $this->loadModule(Module::class, $request);
    // }

    // public function stsMsg(FileReq $request)
    // {
    //     return $this->loadModule(Module::class, $request);
    // }



    // public function followUp(FileReq $request)  { return $this->loadModule(Module::class, $request);  }



}
