<?php

namespace App\Http\Controllers\AdminApi\Upload;
use App\Http\Requests\AdminApi\Upload\DirReq;
use App\Modules\Admin\Upload\DirModule as Module;
class DirController extends \App\Http\Controllers\AdminApi\BaseController
{

    /**  列表 */
    public function all(DirReq $request){ return $this->loadModule(Module::class, $request); }

    /**  添加 */
    public function add(DirReq $request){ return $this->loadModule(Module::class, $request); }

    /**  删除 */
    public function del(DirReq $request){ return $this->loadModule(Module::class, $request); }

    /**  修改 */
    public function edit(DirReq $request){ return $this->loadModule(Module::class, $request); }

}
