<?php

namespace App\Http\Requests\AdminApi\Dev;

use App\Rules\{NotZero, Ymd};

class ErrorLogReq extends BaseReq
{


    public function __construct(
        public array  $rule = [
            'pageSize' => ['required', new NotZero],
            'pageNum' => ['required', new NotZero],
            'ymd' => [new Ymd],
        ]
    ) { }




    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function scene()
    {
        return [
            'list' => ['pageSize', 'pageNum','ymd'],
        ];
    }
}
