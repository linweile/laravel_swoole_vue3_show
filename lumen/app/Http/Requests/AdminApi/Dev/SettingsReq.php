<?php

namespace App\Http\Requests\AdminApi\Dev;

use App\Rules\{NotZero, InField, HaveZero, ZeroOne, ArrValidator};

class SettingsReq extends \App\Http\Requests\AdminApi\Dev\BaseReq
{

    public function __construct(protected $rule = [
        'upload_slice_size' => ['required', "numeric", "min:100", "max:1024", new NotZero],
        'upload_max_size' => ['required', "numeric", "min:0", "max:204800", new HaveZero],
        'library_max_capacity' => ['required', "numeric", "min:1" ],
        'upload_file_type' => ["present", "array", new ArrValidator(["type" => ['required', 'string'], "name" => ['required', 'string']])],
        'upload_check_file_type' => ["required", new ZeroOne],
        'oss_start' => ["required", new ZeroOne],
        'oss_access_key_id' => ["string", "max:255"],
        'oss_access_key_secret' => ["string", "max:255"],
        'oss_endpoint' => ["string", "max:255"],
        'oss_bucket' => ["string", "max:255"],
        'oss_role_arr' => ["string", "max:255"],
        'prefix_url' => [ "string", 'max:255'],

    ])
    {
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function scene()
    {
        return [
            'setItem' => array_keys($this->rule),
            'getItem' => ['field' =>'required', new InField(array_keys($this->rule))],
        ];
    }
}
