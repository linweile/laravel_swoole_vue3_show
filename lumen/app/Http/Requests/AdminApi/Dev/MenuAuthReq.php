<?php

namespace App\Http\Requests\AdminApi\Dev;
use App\Rules\NotZero;
class MenuAuthReq extends BaseReq
{

    public function __construct(
        protected $rule = [
            'menu_id' =>   ['required','numeric',new NotZero],
            'auth_id' =>   ['required',new NotZero],
            'auth_name' => ['required','max:255'],
            'auth_sort' => ['required','numeric',new NotZero],
            'pageSize'  => ['required','numeric',new NotZero],
            'pageNum'  => ['required','numeric',new NotZero]
        ]
    ) { }



    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function scene()
    {
        return [
            'add'  => ['menu_id','auth_name','auth_sort'],
            'edit'  => ['menu_id','auth_name','auth_sort','auth_id'],
            'list'  => ['menu_id','pageSize','pageNum'],
        ];
    }
}
