<?php

namespace App\Http\Requests\AdminApi\Dev;
use App\Rules\{
    NotZero,
    ZeroOne
};
class MenuApiReq extends BaseReq
{

    public function __construct(
        protected $rule = [
            'api' =>   ['required','max:255'],
            'menu_id' =>   ['required',new NotZero],
            'add_log' => ['required',new ZeroOne],
            'api_name' => ['required' ,'max:255'],
            'pageSize' => ['required' , new NotZero],
            'pageNum' => ['required' , new NotZero],
        ]
    ) { }



    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function scene()
    {
        return [
            'add'  => ['api','menu_id','add_log','api_name'],
            'list'  => ['pageSize','pageNum','menu_id'],
            'edit'  => ['auth_id','api','api_id','menu_id','add_log'],
            'addLog'  => ['api_id','add_log'],
            'del'  => ['ids'],
        ];
    }
}
