<?php

namespace App\Http\Requests\AdminApi\Dev\MenuAuth;
use App\Rules\{
    NotZero,
    ZeroOne
};
class ApiReq extends \App\Http\Requests\AdminApi\Dev\BaseReq
{

    public function __construct(
        protected $rule = [
            'auth_id' =>   ['required','numeric',new NotZero],
            'menu_id' =>   ['required','numeric',new NotZero],
            'api' => ['required','max:255'],
            'add_log' => ['required', new ZeroOne],
            'api_id' => ['required','numeric',new NotZero]
        ]
    ) { }



    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function scene()
    {
        return [
            'add'  => ['auth_id','api','menu_id','add_log'],
            'edit'  => ['auth_id','api','api_id','add_log'],
            'addLog'  => ['api_id','add_log'],
            'del'  => ['api_id'],
        ];
    }
}
