<?php

namespace App\Http\Requests\AdminApi\Dev\MenuAuth;
use App\Rules\{NotZero,HaveZero,ZeroOne};
class PagesReq extends \App\Http\Requests\AdminApi\Dev\BaseReq
{

    public function __construct(
        protected $rule = [
            'auth_id' =>   ['required','numeric',new NotZero],
            'pages_id' =>   ['required','numeric',new NotZero],
            'path' => 'required|max:255',
            'component' => 'max:255',
            'name' => 'max:255',
            'redirect' => 'max:255',
            'title' => ['required', 'max:20'],
            'icon' => ['max:20'],
            'isLink' => ['max:512'],
            'isFull' => ['required', 'numeric', new ZeroOne],
            'isAffix' => ['required', 'numeric', new ZeroOne],
            'isKeepAlive' => ['required', 'numeric', new ZeroOne],
        ]
    ) { }



    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function scene()
    {
        return [
            'add'  => [ 'auth_id','menu_pid','path', 'component', 'name', 'redirect', 'title', 'icon', 'isLink', 'isFull', 'isAffix', 'isKeepAlive'],
            'edit'  => ['menu_id','path', 'component', 'name', 'redirect', 'title', 'icon', 'isLink', 'isFull', 'isAffix', 'isKeepAlive'],
            'del'  => ['menu_id'],
        ];
    }
}
