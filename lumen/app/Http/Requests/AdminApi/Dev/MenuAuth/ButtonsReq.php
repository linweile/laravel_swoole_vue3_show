<?php

namespace App\Http\Requests\AdminApi\Dev\MenuAuth;
use App\Rules\NotZero;
class ButtonsReq extends \App\Http\Requests\AdminApi\Dev\BaseReq
{

    public function __construct(
        protected $rule = [
            'auth_id' =>   ['required','numeric',new NotZero],
            'buttons' => ['required','max:255'],
            'buttons_id' =>   ['required','numeric',new NotZero],

        ]
    ) { }



    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function scene()
    {
        return [
            'add'  => ['auth_id','buttons'],
            'edit'  => ['auth_id','buttons','buttons_id'],
            'del'  => ['buttons_id'],
        ];
    }
}
