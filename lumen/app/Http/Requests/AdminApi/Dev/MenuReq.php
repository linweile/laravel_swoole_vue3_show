<?php

namespace App\Http\Requests\AdminApi\Dev;

use App\Rules\HaveZero;
use App\Rules\ZeroOne;
use App\Rules\NotZero;

class MenuReq extends BaseReq
{

    public function __construct(
        protected $rule = [
            'menu_pid' => ['required', 'numeric', new HaveZero],
            'menu_id' => ['required', new NotZero],
            'path' => 'required|max:255',
            'component' => 'max:255',
            'name' => 'max:255',
            'redirect' => 'max:255',
            'menu_sort' => ['required', 'numeric', new HaveZero],
            'title' => ['required', 'max:20'],
            'icon' => ['max:20'],
            'isLink' => ['max:512'],
            'isHide' => ['required', 'numeric', new ZeroOne],
            'isFull' => ['required', 'numeric', new ZeroOne],
            'isAffix' => ['required', 'numeric', new ZeroOne],
            'isKeepAlive' => ['required', 'numeric', new ZeroOne],
        ]
    ) {
    }



    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function scene()
    {
        return [
            'add'  => ['menu_pid', 'path', 'component', 'name', 'redirect', 'menu_sort', 'title', 'icon', 'isLink', 'isHide', 'isFull', 'isAffix', 'isKeepAlive'],
            'edit' => ['menu_pid', 'menu_id', 'path', 'component', 'name', 'redirect', 'menu_sort', 'title', 'icon', 'isLink', 'isHide', 'isFull', 'isAffix', 'isKeepAlive'],
            'all'  => [],
            'del'  => ['menu_id'],
        ];
    }
}
