<?php

namespace App\Http\Requests\AdminApi\Dev;


class DevReq extends BaseReq
{

    public function __construct(protected $rule = [])
    {
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function scene()
    {
        return [];
    }
}
