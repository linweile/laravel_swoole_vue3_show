<?php

namespace App\Http\Requests\AdminApi\Upload;

use App\Rules\{NotZero, ZeroOne, HaveZero,ArrayType};

class DirReq extends \App\Http\Requests\AdminApi\BaseReq
{

    public function __construct(protected $rule = [
       'dir_name'=>['required','max:30'],
       'dir_id'=>['required', 'string'],
       'dir_pid'=> [ 'required', new HaveZero],
       'is_common' => ['required', new ZeroOne],
       'dir_sort' => ['required', new HaveZero],
       'ids' => ['required', 'array', new ArrayType('string')]
    ])
    {
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function scene()
    {
        return [
            'add'   => ['dir_name', 'dir_pid','is_common','dir_sort'],
            'edit'  => ['dir_id','dir_name','is_common','dir_sort'],
            'del'   => ['ids','is_common'],
            'all'   => ['is_common'],
        ];
    }
}
