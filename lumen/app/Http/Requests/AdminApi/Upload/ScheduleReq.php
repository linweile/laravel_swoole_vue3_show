<?php

namespace App\Http\Requests\AdminApi\Upload;

use App\Rules\{NotZero, ZeroOne, HaveZero, ArrValidator, InField};
// use App\Services\Settings\Server as SettingServer;

class ScheduleReq extends \App\Http\Requests\AdminApi\BaseReq
{

    public function __construct(
        protected $rule = [
            'file' => ['required', 'file'],
            'file_name' => ['required', 'string', 'max:255'],
            'dir_id' => ['required', new HaveZero],
            'sizes' => ['required', new NotZero],

            // 'is_common' => ['required', new ZeroOne],
            // 'oid' => ['required', 'string', 'min:20', 'max:50'],
            // 'file_list' => [
            //     'required', 'array', new ArrValidator([
            //         'file_name' => ['required', 'string', 'max:255'],
            //         'file_type' => ['required', 'string'],
            //         'size' => ['required', 'numeric', new NotZero, 'min:0'],
            //     ])
            // ]
        ]
    ) {
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function scene()
    {
        return [
            'ready'   => ['file', 'file_name', 'dir_id' , 'sizes']
        ];
    }
}
