<?php

namespace App\Http\Requests\AdminApi\Upload;

use App\Rules\{NotZero, ZeroOne, HaveZero, ArrValidator,InField , CommaVaild , OrVaild , InArr};
use App\Services\Settings\Server as SettingServer;

class FileReq extends \App\Http\Requests\AdminApi\BaseReq
{

    public function __construct(
        protected $rule = [
            'file' => ['required', 'file'],
            'file_name' => ['required', 'string', 'max:255'],
            'dir_id' => ['required', 'string' , new OrVaild([
                [ 'min:20','max:50'],
                [  new InArr(['0'] , true )  ],
            ])  ],
            'dir_link' => [
            'required',
            'string' ,
             new OrVaild([
                new CommaVaild([ 'min:20', 'max:50']),
                [  new InArr(['0' , '-1'] , true )  ],
            ])
           ],
            'sizes' => ['required', new HaveZero],
            'file_size' => ['required', new HaveZero],
            'is_common' => ['required', new ZeroOne],
            'oid' => ['required','string','min:20','max:50'],
            'schedule' => ['required', 'numeric' ,'min:0' ,'max:1'],
            'file_type' => ['string','max:255'],
            'keyWord' => [ 'string', 'max:255'],
            'pageSize' => ['required', new NotZero],
            'pageNum' => ['required', new NotZero],
            'real_del' => ['required', new ZeroOne],
            	// 'dir_id': Number(dir_id),       //文件夹id
				// 		'file_size': file.size,    //文件大小
				// 		'is_common': is_common,        //1公共 0非公共
				// 		'file_type': suffix ? suffix : '',    //文件类型
				// 		'file_name': file.name,    //文件名称
            'file_list' => [
                'required', 'array', new ArrValidator([
                    'file_name' => ['required', 'string', 'max:255'],
                    'file_type' => ['required', 'string'],
                    'size' => ['required', 'numeric', new NotZero,'min:0' ],
            ])],
            'oids' => ['required', 'array' ,new ArrValidator(['required', 'string', 'min:20', 'max:50'],36)]
        ]
    ) {

    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function scene()
    {
        return [
            'first'   => [ 'is_common', 'dir_id','file_name','file_size' , 'file_type'],
            'followUp'   => [ 'oid','file'],
            'readdition'   => ['file'],
            'list' => ['dir_link','is_common' , 'pageSize' , 'pageNum' , 'keyWord' , 'file_type' => ['string','max:255']],
            'schedule'   => ['oid', 'schedule' ,'is_common'],
            'stsMsg' => ['is_common', 'file_size' , 'dir_id' , 'file_type' , 'file_name'],
            'del' => ['oids', 'is_common', 'real_del'],
        ];
    }
}
