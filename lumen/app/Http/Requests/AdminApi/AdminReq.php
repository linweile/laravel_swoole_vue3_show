<?php

namespace App\Http\Requests\AdminApi;

use App\Rules\{NotZero, ZeroOne, HaveZero, Phone,FileString,IsArrayList};

class AdminReq extends BaseReq
{

    public function __construct(protected $rule = [
        'pageSize' => ['required', new NotZero],
        'admins_id' => ['required', 'string','size:21'],
        'pageNum'  => ['required', new NotZero],
        'username' => ['required','max:20','min:5'],
        'password' => 'required|string|size:32',
        'ori_password' => 'required|string|size:32', //原密码
        'nickName' => 'required|string|max:20', //用户昵称
        'status'   => ['required', new ZeroOne],
        'role_id'  => ['required', new NotZero],
        'phone'    => ['required', new Phone],
        'realName' => ['string', 'max:20'],
        'portrait' => [ new FileString(255,200)],
        'ids' => [ 'required','array',new IsArrayList(['required','string','size:21'],null,100)],
    ])
    { }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function scene()
    {
        return [
            'login' => ['username', 'password'],
            'list'  => ['pageSize', 'pageNum', 'username' => ['max:50'], 'nickName' => ['max:50'], 'realName' => ['max:50'], 'role_id' => [new HaveZero]],
            'add'   => ['username', 'password', 'status', 'role_id', 'portrait', 'nickName', 'phone', 'realName'],
            'edit'   => ['admins_id','username', 'password' => ['string','size:32'] , 'status', 'role_id', 'portrait', 'nickName', 'phone', 'realName'],
            'status'   => ['admins_id', 'status'],
            'editUserInfo'   => ['username','portrait', 'nickName', 'phone', 'realName'],
            'del' => ['ids'],
            'editPass' => ['password', 'ori_password']
        ];
    }
}
