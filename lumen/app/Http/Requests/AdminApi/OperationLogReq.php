<?php

namespace App\Http\Requests\AdminApi;

use App\Rules\{NotZero, Ymd};

class OperationLogReq extends BaseReq
{

    public function __construct(
        protected $rule = [
            'pageSize' => ['required', new NotZero],
            'pageNum' => ['required', new NotZero],
            'log_id' => ['max:50'],
            'menu_title' => ['max:50'],
            'auth_name' => ['max:50'],
            'nickName' => ['max:50'],
            'realName' => ['max:50'],
            'router_path' => ['max:50'],
            'api' => ['max:50'],
            'ip' => ['max:50'],
            'operation_time' => [new Ymd],
        ]
    ) {
    }



    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function scene()
    {
        return [
            'list'  => ['pageSize', 'pageNum', 'ip','log_id', 'api','router_path', 'menu_title', 'auth_name', 'nickName', 'realName', 'operation_time'],
            'adminList' =>   ['pageSize', 'pageNum', 'ip','log_id','router_path', 'menu_title', 'auth_name', 'nickName', 'realName', 'operation_time']
        ];
    }
}
