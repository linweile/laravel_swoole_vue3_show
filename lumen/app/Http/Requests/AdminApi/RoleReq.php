<?php

namespace App\Http\Requests\AdminApi;

use App\Rules\{NotZero, ArrayType};

class RoleReq extends BaseReq
{


    public function __construct(
        protected $rule = [
            'pageSize'  => ['required', 'numeric', new NotZero],
            'pageNum'  => ['required', 'numeric', new NotZero],
            'role_id'  => ['required', new NotZero],
            'role_name'  => ['required', 'max:255'],
            'role_describe'  => ['max:512'],
            'role_sort'  => ['required', 'numeric'],
            'role_json'  => ['required', 'array', new ArrayType('array', 'number')],
            'ids' => ['required', 'array', new ArrayType('number', 'number')]
        ]
    ) {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function scene()
    {
        return [
            'list' => ['pageSize', 'pageNum','role_name'=>[
                'max:50'
            ]],
            'add' => ['role_name', 'role_sort', 'role_describe'],
            'edit' => ['role_name', 'role_sort', 'role_describe','role_id'],
            'set' => ['role_json','role_id'],
            'del' => ['ids']
        ];
    }
}
