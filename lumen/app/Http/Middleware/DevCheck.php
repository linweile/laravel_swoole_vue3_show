<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Common\Enum\Attributes;
use App\Common\Http\Basics\Dev;
use App\Common\Enum\Http;
use App\Common\Helper;
use App\Services\Info\TraitUse as InfoTraitUse;
use App\Models\Mysql\Orm\Lsv\Admins;

class DevCheck
{
    use InfoTraitUse;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $userInfo = $this->getSetInfoReq($request, Attributes::LOGIN_INFO, 'admins_id', Admins::class, ['role_id'], fn () => null);
        if (!$userInfo) return Helper::toHttpEnum(Dev::DEV_ACCOUNT_NOT_EXIST );     //账号删除
        if ($userInfo['role_id'] !== -1) return Helper::toHttpEnum(Dev::NOT_DEV_ROLE  ); //账号没有开发者权限
        return $next($request);
    }
}
