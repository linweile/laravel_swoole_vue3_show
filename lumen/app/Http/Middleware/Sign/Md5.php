<?php

namespace App\Http\Middleware\Sign;
use Closure;
use Illuminate\Http\Request;
use App\Services\Sign\Server as Sign;
use App\Common\Helper;
use App\Common\Http\Basics\SignError;
class Md5 extends Base
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $sign = $this->checkSignHeader($request);   //检查签名头部是否存在
        if($sign === false )  return   Helper::toHttpEnum(SignError::SIGN_ERROR);
        $params = $this->checkParams($request);     //检查参数
        if($params === false ) return   Helper::toHttpEnum(SignError::PARAMS_ERROR);
        /* 检查md5 加密 */
        if(strlen($sign) !== 32 || !Sign::md5($params, $sign)->checkSign())  return  Helper::toHttpEnum(SignError::SIGN_ERROR);
        return $next($request);
    }
}
