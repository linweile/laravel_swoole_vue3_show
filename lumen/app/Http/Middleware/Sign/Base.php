<?php

namespace App\Http\Middleware\Sign;

use Illuminate\Http\Request;
use App\Common\{
    Helper,
    Enum\Header,
};

abstract class Base
{
    protected function checkSignHeader(Request $request, Header $header = Header::SIGN): false|string
    {
        $sign =  $request->header($header->value);
        return $sign  ?   $sign : false;
    }
    protected function checkParams(Request $request): false|array
    {
        $params =  $request->input();
        $now = time();
        if (
            !isset($params['timestamp'])  ||
            !is_numeric($params['timestamp'])  ||
            strlen((string)$params['timestamp']) !== 10 || //必须为10位时间戳
            $now - 60 > $params['timestamp']  ||     //发送时间只能为60秒前
            $params['timestamp'] > $now              //不能超过现在时间

        )   return false; //时间戳错误

        return $params;
    }
}
