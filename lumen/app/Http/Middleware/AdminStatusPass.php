<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Common\Enum\Attributes;
use App\Common\Http\Basics\StatusPass;
use App\Common\Enum\Http;
use App\Common\Helper;
use App\Services\Info\TraitUse;
use App\Models\Mysql\Orm\Lsv\Admins;

class AdminStatusPass
{
    use TraitUse;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $userInfo = $this->getSetInfoReq($request, Attributes::LOGIN_INFO, 'admins_id', Admins::class, ['status', 'password'], fn () => null);
        if (!$userInfo) return Helper::toHttpEnum(StatusPass::ACCOUNT_NOT_EXIST  );      //账号不存在
        if ($userInfo['status'] !== 1) return Helper::toHttpEnum(StatusPass::DISABLE  ); //账号禁用
        $jwtInfo =  $request->get(Attributes::JWT_INFO->value, []);
        if (isset($jwtInfo['password']) && $jwtInfo['password'] !== $userInfo['password']) return Helper::toHttpEnum(StatusPass::PASS_CHANGE); //账号密码发生改变
        return $next($request);
    }
}
