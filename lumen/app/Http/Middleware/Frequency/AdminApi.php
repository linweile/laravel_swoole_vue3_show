<?php

namespace App\Http\Middleware\Frequency;

use App\Common\Helper;
use App\Common\Http\Basics\Frequency;
use Closure;
use Illuminate\Http\Request;
use App\Services\Frequency\Server;
use App\Utils\Frequency\Status;

class AdminApi extends Base
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {


        return Server::client()->check($request, fn ($status, $ttl = null) =>
        match ($status) {
            Status::success => $next($request),
            Status::ip_banned => Helper::toHttpEnum(Frequency::IP_BANNED, ['code' => Status::ip_banned->value]),
            Status::ip_colling => Helper::toHttpEnum(Frequency::FREQUENT_REQUEST_MID, ['ttl' => $ttl]),
        });
    }
}
