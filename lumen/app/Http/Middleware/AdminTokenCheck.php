<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Services\JWT\Server as JWTServer;
use App\Common\Enum\Attributes;
use App\Common\Enum\Header;
use App\Common\Http\Basics\Error;
// use App\Common\Enum\ResultMsg;

use App\Common\Helper;
class AdminTokenCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        return JWTServer::adminLogin()->check($request, Header::ADMIN_LOGIN_TOKEN, function ($userInfo) use ( $request, $next) {
            if(!isset($userInfo['admins_id']))  return Helper::toHttpEnum(Error::ADMINS_ID_NOT_DEFIND);
            $request->attributes->add([Attributes::LOGIN_INFO->value => $userInfo]); //登录信息
            $request->attributes->add([Attributes::JWT_INFO->value => $userInfo]);   //JWT信息
            return $next($request);
        });
    }
}
