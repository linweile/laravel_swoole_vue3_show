<?php

namespace App\Http\Middleware\Referer;


use Closure;
use Illuminate\Http\Request;
use App\Common\Helper;
// use App\Common\Http\Basics\Wechat;
use App\Services\Referer\Server as Referer;

// use App\Common\Enum\Referer;
use App\Utils\CheckReferer\{Control, Status};
use App\Common\Http\Basics\Referer as RefererHttpEnum;

class AdminApi
{

    // private string $refererPrefix = 'https://servicewechat.com';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        if (config('app.debug') === true) return $next($request);
        $status =   Referer::admin($request)->getStatus();
        return match ($status) {
            Status::SUCCESS => $next($request),
            Status::NOT_DEFIENED => Helper::toHttpEnum(RefererHttpEnum::REQ_ILLEGAL),
            Status::ERROR => Helper::toHttpEnum(RefererHttpEnum::REQ_ILLEGAL),
        };
    }
}
