<?php

/** @var \Laravel\Lumen\Routing\Router $router */

$router->group(
    [ 'middleware' => ['adminReferer', 'adminFrequency'] ],
    function () use ($router) {

        $router->group(['prefix' => 'admin'], function () use ($router) {
            $router->get('login',  'AdminController@login');
            $router->get('check/login', ['middleware' => ['adminTokenCheck', 'adminRefreshToken', 'adminStatusPass'], 'uses' => 'AdminController@checkLogin']);  //检查登录状态
        });
        //检查开发者账号
        $router->group(['prefix' => 'dev',  'namespace' => 'Dev'], function () use ($router) {
            $router->get('account/check', 'AccountController@check');
            $router->get('test', 'AccountController@test');
        });


        // //检查两个token
        $router->group([
            'middleware' => ['adminTokenCheck', 'adminRefreshToken', 'adminStatusPass']
        ], function () use ($router) {

            $router->group(['prefix' => 'upload', 'namespace' => 'Upload'], function () use ($router) {
                $router->group(['prefix' => 'dir'], function () use ($router) {
                    $router->post('add', 'DirController@add');
                    $router->put('edit',  'DirController@edit');
                    $router->delete('del',  'DirController@del');
                    $router->get('all',  'DirController@all');
                });
                $router->group(['prefix' => 'file'], function ($router) {
                    $router->post('first',  'FileController@first');
                    $router->put('schedule',  'FileController@schedule');
                    $router->post('followUp',  'FileController@followUp');
                    // $router->put('edit',  'FileController@edit');
                    $router->get('getConfig',  'FileController@getConfig');
                    $router->get('stsMsg',  'FileController@stsMsg');

                    $router->get('list',  'FileController@list');

                    $router->delete('del',  'FileController@del');
                });
            });

            $router->group(['middleware' => ['adminCheckApiAuth']], function () use ($router) {

                $router->group(['prefix' => 'admin'], function () use ($router) {
                    $router->post('add', 'AdminController@add');
                    $router->get('list',  'AdminController@list');
                    $router->put('status',  'AdminController@status');
                    $router->put('edit',  'AdminController@edit');
                    $router->put('editPass',  'AdminController@editPass');
                    $router->put('editUserInfo',  'AdminController@editUserInfo');
                    $router->delete('del',  'AdminController@del');
                    $router->group(['prefix' => 'operation/log'], function () use ($router) {
                        $router->get('list',  'OperationLogController@adminList');
                    });
                });

                $router->group(['prefix' => 'role'], function () use ($router) {
                    $router->get('treeData',  'RoleController@treeData');
                    $router->get('all',  'RoleController@all');
                    $router->post('add', 'RoleController@add');
                    $router->get('list',  'RoleController@list');
                    $router->delete('del', 'RoleController@del');
                    $router->put('edit',  'RoleController@edit');
                    $router->put('set',  'RoleController@set');
                });
            });


            $router->group(['prefix' => 'dev', 'namespace' => 'Dev'], function () use ($router) {



                $router->group([
                    'middleware' => ['adminTokenCheck', 'adminRefreshToken', 'adminStatusPass']
                ], function () use ($router) {

                    //开发者接口
                    $router->group(['middleware' => ['devCheck']], function () use ($router) {
                        //开发者接口
                        $router->group(['prefix' => 'menu'], function () use ($router) {
                            $router->post('add', 'MenuController@add');
                            $router->get('all', 'MenuController@all');
                            $router->delete('del', 'MenuController@del');
                            $router->put('edit', 'MenuController@edit');

                            $router->group(['prefix' => 'api'], function () use ($router) {
                                $router->post('add', 'MenuApiController@add');
                                $router->put('edit', 'MenuApiController@edit');
                                $router->put('addLog', 'MenuApiController@addLog');
                                $router->get('list', 'MenuApiController@list');
                                $router->delete('del', 'MenuApiController@del');
                            });

                            $router->group(['prefix' => 'auth'], function ()  use ($router) {

                                $router->post('add',  'MenuAuthController@add');
                                $router->get('list',  'MenuAuthController@list');
                                $router->delete('del', 'MenuAuthController@del');
                                $router->put('edit',  'MenuAuthController@edit');

                                $router->group(['namespace' => 'MenuAuth'], function () use ($router) {

                                    $router->group(['prefix' => 'buttons'], function () use ($router) {
                                        $router->post('add', 'ButtonsController@add');
                                        $router->delete('del', 'ButtonsController@del');
                                        $router->put('edit',  'ButtonsController@edit');
                                    });
                                    $router->group(['prefix' => 'api'], function () use ($router) {
                                        $router->post('add',  'ApiController@add');
                                        $router->delete('del',  'ApiController@del');
                                        $router->put('edit',  'ApiController@edit');
                                        $router->put('addLog',  'ApiController@addLog');
                                    });

                                    $router->group(['prefix' => 'pages'], function () use ($router) {
                                        $router->post('add',  'PagesController@add');
                                        $router->delete('del', 'PagesController@del');
                                        $router->put('edit',  'PagesController@edit');
                                    });
                                });
                            });
                        });

                        $router->group(['prefix' => 'settings'], function () use ($router) {
                            $router->get('getItem',  'SettingsController@getItem');
                            $router->put('setItem',  'SettingsController@setItem');
                        });


                        $router->group(['prefix' => 'error/log'], function () use ($router) {
                            $router->get('list',  'ErrorLogController@list');
                            $router->get('date',  'ErrorLogController@date');
                        });

                        $router->group(['prefix' => 'operation/log'], function () use ($router) {
                            $router->get('list', 'OperationLogController@list');
                        });
                    });
                });
            });
        });
    }
);
