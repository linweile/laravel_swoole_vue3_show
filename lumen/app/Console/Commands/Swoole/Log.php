<?php

namespace App\Console\Commands\Swoole;

use Illuminate\Console\Command;

class Log extends Command
{

    protected $signature = 'swoole:log {action : clear}';

    protected $description = '清空日志一分钟清一次';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        file_put_contents(base_path(). '/storage/logs/swoole_http.log','');
        file_put_contents(base_path(). '/storage/logs/laravel.log','');
    }
}
