<?php

namespace App\Console\Commands\Dev;

use Illuminate\Console\Command;
use App\Services\Admin\Dev as DevServer;

class Check extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dev:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '检查账号';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
         $dev = DevServer::check();
         echo "账号:{$dev['username']}  密码:{$dev['password']}\n";
    }


}
