<?php
namespace App\Modules\Interfaces;
use Illuminate\Http\Request;


interface Module {
    public function __construct(Request $request);
}
