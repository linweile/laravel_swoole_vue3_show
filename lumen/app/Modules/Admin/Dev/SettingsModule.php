<?php

namespace App\Modules\Admin\Dev;

use App\Modules\Admin\BaseModule;
use App\Services\Settings\Server as SettingServer;
use App\Common\Helper;
use App\Common\Http\Basics\Error;
use App\Common\Http\Basics\Success;
// use App\Common\Http\Basics\Fail;

class SettingsModule extends BaseModule
{

    public function setItem()
    {
        SettingServer::dev()->setItem($this->params);
        return $this->outputEnum(Success::SET_SUCCESS);
    }

    public function getItem()
    {
        return $this->output(SettingServer::dev()->getItem($this->params['field']), Success::SEL_SUCCESS);
    }
}
