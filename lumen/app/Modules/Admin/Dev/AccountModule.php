<?php

namespace App\Modules\Admin\Dev;

use App\Services\Admin\Dev;
use  App\Common\Http\Basics\Success;
use  App\Common\Http\Basics\Fail;
class AccountModule extends BaseModule
{
    public function check()
    {
        if(!config('app.debug')) return $this->outputEnum(Fail::NOT_DEV_ENV);
        return $this->output(Dev::check(),Success::SEL_SUCCESS);
    }
}
