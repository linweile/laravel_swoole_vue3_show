<?php

namespace App\Modules\Admin\Dev;

use  App\Dao\Lsv\AdminLogDao\Orm as AdminLogDao;

class OperationLogModule extends BaseModule
{
    public function list($field = ['admins_log.*','admins.nickName','admins.realName'])
    {
        $dao = (new AdminLogDao)->joinAdmin();

        if(isset($this->params['log_id']) &&  $this->params['log_id'] !== null)  $dao =  $dao->whereLogId((int)$this->params['log_id']) ;

        if(isset($this->params['menu_title']) &&  $this->params['menu_title'] !== null)  $dao =  $dao->whereMenuTitle((string)$this->params['menu_title']) ;

        if(isset($this->params['router_path']) &&  $this->params['router_path'] !== null)  $dao =  $dao->whereRouterPath((string)$this->params['router_path']) ;

        if(isset($this->params['api']) &&  $this->params['api'] !== null)  $dao =  $dao->whereApi((string)$this->params['api']) ;

        if(isset($this->params['operation_time']) &&  $this->params['operation_time'] !== null)  $dao =  $dao->whereOperationTime((string)$this->params['operation_time']) ;

        if(isset($this->params['auth_name']) &&  $this->params['auth_name'] !== null)  $dao =  $dao->whereAuthName((string)$this->params['auth_name']) ;

        if(isset($this->params['ip']) &&  $this->params['ip'] !== null)  $dao =  $dao->whereIp((string)$this->params['ip']) ;

        if(isset($this->params['nickName']) &&  $this->params['nickName'] !== null)  $dao =  $dao->whereAdminsNickName((string)$this->params['nickName']) ;

        if(isset($this->params['realName']) &&  $this->params['realName'] !== null)  $dao =  $dao->whereAdminsRealName((string)$this->params['realName']) ;

        return $this->outputList(
            $dao->end()->count(),
            $dao->end()->get($field)->toArray(),

        );
    }
}
