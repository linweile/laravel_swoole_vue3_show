<?php

namespace App\Modules\Admin\Dev;

use  App\Dao\Lsv\{MenuApiDao, MenuAuth\ApiDao};

use App\Common\Http\Basics\Success;
use App\Common\Http\Basics\Fail;
use  App\Dao\Lsv\MenuAuthApiDao\Orm as MenuAuthApiDao;
class MenuApiModule extends BaseModule
{

    public function add()
    {
        if((new MenuAuthApiDao)->whereMenuAuthApi($this->params['api'])->whereMenuId($this->params['menu_id'])->end()->exists())  return $this->outputEnum(Fail::API_IS_EXIST);
        (new MenuAuthApiDao)->add($this->params);
        return $this->outputEnum(Success::ADD_SUCCESS);
    }

    public function list()
    {
        $menuApiDao  =  (new  MenuAuthApiDao)->whereAuthId(null)->whereMenuId($this->params['menu_id']);
        return $this->outputList(
            $menuApiDao->end()->count(),
            $menuApiDao->pages((int)$this->params['pageNum'],(int)$this->params['pageSize'])->end()->get(['api_id','menu_id','api','add_log','api_name'])->toArray(),
        );
    }

    public function edit()
    {
        if ( (new MenuAuthApiDao)->whereMenuId($this->params['menu_id'])->whereMenuAuthApi($this->params['api'])->whereMenuApiId($this->params['api_id'])->end()->exists() ) return $this->outputEnum(Fail::API_IS_EXIST);
        (new  MenuAuthApiDao)->whereApiId($this->params['api_id'])->edit($this->params);
        return $this->outputEnum(Success::EDIT_SUCCESS);
    }

    public function addLog()
    {
        (new  MenuAuthApiDao)->whereApiId($this->params['api_id'])->addLog($this->params);
        return $this->outputEnum( Success::EDIT_SUCCESS);
    }

    public function del()
    {
        (new  MenuAuthApiDao)->whereInApiId($this->params['ids'])->end()->delete();
        return $this->outputEnum( Success::DEL_SUCCESS);
    }
}
