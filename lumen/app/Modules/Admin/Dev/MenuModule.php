<?php

namespace App\Modules\Admin\Dev;

use App\Common\Http\Basics\Fail;
// use  App\Dao\Lsv\MenuDao;
use  App\Dao\Lsv\MenuDao\Orm as MenuDao;
use App\Common\Http\Basics\Success;
use App\Common\Helper;


class MenuModule extends BaseModule
{

    public function add()
    {
        if ((new MenuDao)->wherePath($this->params['path'])->end()->exists())  return $this->outputEnum(Fail::MENU_IS_EXIST);

        (new MenuDao)->add($this->params);

        return $this->outputEnum(Success::ADD_SUCCESS);
    }

    public function all()
    {
        return $this->output(
            Helper::createTree( (new MenuDao)->whereAuthId(null)->menuSort()->menuIdSort()->end()->get(
                [
                    'menu_id',
                    'menu_pid',
                    'path',
                    'name',
                    'redirect',
                    'component',
                    'menu_sort',
                    'icon',
                    'title',
                    'isLink',
                    'isHide',
                    'isFull',
                    'isAffix',
                    'isKeepAlive'
                ]
            )->toArray(),'menu_id','menu_pid')
            ,
            Success::SEL_SUCCESS
        );
    }

    public function del()
    {
        (new MenuDao)->whereMenuId($this->params['menu_id'])->end()->delete();

        return $this->outputEnum(Success::DEL_SUCCESS);
    }
    public function edit()
    {
        if ((new MenuDao)->wherePath( $this->params['path'] )->whereNotMenuId( $this->params['menu_id'] )->end()->exists() ) return $this->outputEnum(Fail::MENU_IS_EXIST);

        (new MenuDao)->whereMenuId( $this->params['menu_id'] )->edit($this->params);

        return $this->outputEnum(Success::EDIT_SUCCESS);
    }
}
