<?php

namespace App\Modules\Admin\Dev;

use  App\Dao\Logs\ErrorDao;
use App\Common\Http\Basics\Success;

class ErrorLogModule extends BaseModule
{

    public function list()
    {
        return $this->outputList(
            ErrorDao::total($this->params),
            ErrorDao::list($this->params)
        );
    }


    public function date()
    {
        return $this->output( ['date'=>ErrorDao::getTableName()] , Success::SEL_SUCCESS);
    }
}
