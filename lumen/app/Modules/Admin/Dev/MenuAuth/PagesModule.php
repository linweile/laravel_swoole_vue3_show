<?php

namespace  App\Modules\Admin\Dev\MenuAuth;

use  App\Dao\Lsv\MenuDao\Orm as MenuDao;
use  App\Common\Http\Basics\Success;


class PagesModule extends \App\Modules\Admin\BaseModule
{

    public function add()
    {
        (new MenuDao)->add($this->params);
        return $this->outputEnum( Success::ADD_SUCCESS);
    }
    public function del()
    {
        (new MenuDao)->whereAuth()->whereMenuId((int)$this->params['menu_id'])->end()->delete();
        return $this->outputEnum(Success::DEL_SUCCESS);
    }
    public function edit()
    {
        (new MenuDao)->whereMenuId((int)$this->params['menu_id'])->edit($this->params);
        return $this->outputEnum(Success::EDIT_SUCCESS);
    }
}
