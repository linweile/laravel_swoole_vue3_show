<?php

namespace  App\Modules\Admin\Dev\MenuAuth;

use  App\Dao\Lsv\MenuAuthApiDao\Orm as MenuAuthApiDao;
// use App\Common\Enum\{ResultMsg};
use App\Common\Http\Basics\Success;
use App\Common\Http\Basics\Fail;

class ApiModule extends \App\Modules\Admin\BaseModule
{

    public function add()
    {

        $res = (new MenuAuthApiDao)->whereApi($this->params['api'])->end()->first(['api_id']);

        if ($res) return $this->outputEnum(Fail::API_IS_EXIST_ID,['api_id'=>$res->api_id]);

        (new MenuAuthApiDao)->add($this->params);

        return $this->outputEnum(Success::ADD_SUCCESS);
    }


    public function del()
    {
        (new MenuAuthApiDao)->whereApiId($this->params['api_id'])->end()->delete();
        return $this->outputEnum(Success::DEL_SUCCESS);
    }

    public function edit()
    {
        $res = (new MenuAuthApiDao)->whereApi($this->params['api'])->whereNotApiId($this->params['api_id'])->end()->first(['api_id']);
        if ($res) return $this->outputEnum(Fail::API_IS_EXIST_ID,['api_id' => $res->api_id]);
        (new MenuAuthApiDao)->whereApiId($this->params['api_id'])->edit($this->params) ;
        return $this->outputEnum(Success::EDIT_SUCCESS);
    }

    public function addLog()
    {
        (new MenuAuthApiDao)->whereApiId($this->params['api_id'])->addLog($this->params) ;
        return $this->outputEnum(Success::EDIT_SUCCESS);
    }
}
