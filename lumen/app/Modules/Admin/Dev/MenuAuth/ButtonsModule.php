<?php

namespace  App\Modules\Admin\Dev\MenuAuth;

use  App\Dao\Lsv\MenuAuthButtonDao\Orm as MenuAuthButtonDao;
use  App\Common\Http\Basics\Success;

class ButtonsModule extends \App\Modules\Admin\BaseModule
{

    public function add()
    {
        (new MenuAuthButtonDao)->add($this->params);
        return $this->outputEnum(Success::ADD_SUCCESS);
    }
    public function del()
    {
        (new MenuAuthButtonDao)->whereButtonsId($this->params['buttons_id'])->end()->delete();
        return $this->outputEnum(Success::DEL_SUCCESS);
    }
    public function edit()
    {
        (new MenuAuthButtonDao)->whereButtonsId($this->params['buttons_id'])->edit($this->params);
        return $this->outputEnum(Success::EDIT_SUCCESS);
    }
}
