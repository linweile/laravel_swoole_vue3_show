<?php

namespace App\Modules\Admin\Dev;

use  App\Dao\Lsv\MenuAuthDao\Orm as MenuAuthDao;
use App\Common\Http\Basics\Success;
class MenuAuthModule extends BaseModule
{

    public function add()
    {
        (new MenuAuthDao)->add($this->params);
        return $this->outputEnum( Success::ADD_SUCCESS);
    }

    public function list()
    {

        $dao =  (new MenuAuthDao)->whereMenuId($this->params['menu_id']);
        return $this->outputList(
            $dao->end()->count(),
            $dao->withButtons()->withApi()->withPages()->authIdSort()->authIdSort()->pages($this->params['pageNum'],$this->params['pageSize'])->end()->get()->toArray(),
        );
    }

    public function del()
    {
        (new MenuAuthDao)->whereAuthId($this->params['auth_id'])->end()->delete();
        return $this->outputEnum(Success::DEL_SUCCESS);
    }
    public function edit()
    {
        (new MenuAuthDao)->whereAuthId($this->params['auth_id'])->edit($this->params);
        return $this->outputEnum( Success::EDIT_SUCCESS);
    }
}
