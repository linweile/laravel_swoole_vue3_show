<?php

namespace App\Modules\Admin;

use App\Services\Info\TraitFunc as TraitFuncInfo;
use App\Models\Mysql\Orm\Lsv\Admins;
abstract class BaseModule extends \App\Modules\BaseModule
{
    use TraitFuncInfo;

    protected final function getAdminInfo(array $field = []){
        return   $this->getInfo('admins_id',Admins::class,$field);
    }
    protected final function getAdminId(){
        return   $this->getId('admins_id');
    }
}
