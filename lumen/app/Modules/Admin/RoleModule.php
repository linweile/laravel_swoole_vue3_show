<?php

namespace App\Modules\Admin;

use  App\Dao\Lsv\RoleDao\Orm as RoleDao;
use  App\Dao\Lsv\MenuDao\Orm as MenuDao;
use App\Common\Http\Basics\Success;
use App\Common\Helper;



class RoleModule extends BaseModule
{

    public function treeData()
    {
        $result = (new MenuDao)->whereMenu()->withAuth()->menuSort()->menuIdSort()->end()->get(
            [
                'menu_id',
                'title',
                'menu_pid',
            ]
        )->toArray();
        return  $this->output(Helper::createTree($result, 'menu_id', 'menu_pid', 'children', function ($item) {
            if ($item['menu_pid']  === null) $item['menu_pid'] =  0;
            return $item;
        }), Success::SEL_SUCCESS);
    }



    public function add()
    {
        (new RoleDao)->add($this->params);
        return $this->outputEnum(Success::ADD_SUCCESS);
    }

    public function list()
    {
        $dao =  new RoleDao;
        if (isset($this->params['role_name']))  $dao = $dao->roleNameLike($this->params['role_name']);
        return $this->outputList(
            $dao->end()->count(),
            $dao->orderByRoleSort()->orderByRoleId()->pages($this->params['pageNum'], $this->params['pageSize'])->end()->get(),
        );
    }

    public function del()
    {
        (new RoleDao)->del($this->params['ids']);
        return $this->outputEnum(Success::DEL_SUCCESS);
    }

    public function edit()
    {
        (new RoleDao)->edit($this->params, $this->params['role_id']);
        return $this->outputEnum(Success::EDIT_SUCCESS);
    }

    public function set()
    {
        (new RoleDao)->set($this->params, $this->params['role_id']);
        return $this->outputEnum(Success::SET_SUCCESS);
    }

    public function all()
    {
        return $this->output((new RoleDao)->orderByRoleSort()->orderByRoleId()->end()->get(['role_name', 'role_id'])->toArray(), Success::SEL_SUCCESS);
    }
}
