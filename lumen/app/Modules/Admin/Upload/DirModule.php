<?php

namespace App\Modules\Admin\Upload;

use  App\Dao\Lsv\UploadDirDao\Orm as UploadDirDao;
use  App\Common\Http\Basics\Success;
use  App\Common\Helper;
use App\Common\Http\Basics\Fail;
use App\Common\Http\Basics\Upload;
use  App\Dao\Mongo\AdminsDir;

class DirModule extends \App\Modules\Admin\BaseModule
{

    public function add()
    {
        $res = AdminsDir::add($this->getAdminId(), $this->params['is_common'] , $this->params );

        if($res instanceof Upload ) return $this->outputEnum($res);

        return $this->outputEnum(Success::ADD_SUCCESS);
    }
    public function edit()
    {
        // (new UploadDirDao)->whereDirId($this->params['dir_id'])->whereAdminsIdOrNull($this->getAdminId())->edit($this->params);
        AdminsDir::edit($this->getAdminId(), $this->params['is_common'], $this->params);
        return $this->outputEnum(Success::EDIT_SUCCESS);
    }
    public function del()
    {
        // (new UploadDirDao)->whereInDirId($this->params['ids'])->whereAdminsIdOrNull($this->getAdminId())->end()->delete();
        AdminsDir::delete($this->getAdminId(), $this->params['is_common'], $this-> params['ids']);
        return $this->outputEnum(Success::DEL_SUCCESS);
    }

    public function all()
    {
        $all = AdminsDir::all($this->getAdminId(), $this->params['is_common']);
        return $this->output( Helper::createTree($all,'dir_id','dir_pid','children',function($item){
            $item['dir_link'] = $item['dir_link'] ?  $item['dir_link'] . ',' . $item['dir_id'] : $item['dir_id']  ;
            return $item;
        },false,'0'), Success::SEL_SUCCESS);
    }


}
