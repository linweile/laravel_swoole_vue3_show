<?php

namespace App\Rules;


class HaveZero  extends BaseRules
{



    /**
     * 判断是否通过验证规则
     *
     * @param  string  $attribute 检测的字段
     * @param  mixed   $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if( $value === null ) return true;

        return preg_match('/(^[1-9][0-9]+$)|(^[1-9]$)|0/',$value);
    }

    /**
     * 获取校验错误信息
     *
     * @return string
     */
    public function message()
    {
        return ':attribute含0整数';
    }
}
