<?php

namespace App\Rules;


use Illuminate\Contracts\Validation\ImplicitRule;


class NotZero  implements ImplicitRule
{



    /**
     * 判断是否通过验证规则
     *
     * @param  string  $attribute 检测的字段
     * @param  mixed   $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($value === null ) return true;
        return preg_match('/(^[1-9][0-9]+$)|(^[1-9]$)/',$value)  ;
    }

    /**
     * 获取校验错误信息
     *
     * @return string
     */
    public function message()
    {
        return ':attribute非0开头整数';
    }
}
