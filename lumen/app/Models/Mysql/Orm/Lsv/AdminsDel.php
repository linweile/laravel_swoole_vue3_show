<?php

namespace App\Models\Mysql\Orm\Lsv;

use App\Common\Helper;

class AdminsDel extends BaseLsv
{

    public function getPortraitAttribute()
    {
        return  ['full_url' => Helper::baseUrl($this->attributes['portrait'] ? $this->attributes['portrait'] : '/web/images/avatar.jpg'),'value' => $this->attributes['portrait']];
    }
}
