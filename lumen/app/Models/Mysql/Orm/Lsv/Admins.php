<?php

namespace App\Models\Mysql\Orm\Lsv;

use App\Common\Helper;

class Admins extends BaseLsv
{
    public $incrementing = false;

    public function getPortraitAttribute()
    {
        return  ['full_url' => $this->attributes['portrait'] ? $this->attributes['portrait']  : Helper::baseUrl('/web/images/avatar.jpg')  ,'value' => $this->attributes['portrait']];
    }

    public function role(){
        return  $this->hasOne(Role::class,'role_id','role_id')->select(['role_name','role_id']);
    }
}
