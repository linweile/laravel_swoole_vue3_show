<?php

namespace App\Models\Mysql\Orm\Lsv;
use App\Casts\Serialize;

class AdminsLog extends BaseLsv
{
    protected $casts = [
        'params' =>  Serialize::class,
        'sql' =>  Serialize::class,
        'enum' =>  Serialize::class,
    ];
}
