<?php

namespace App\Models\Mysql\Orm\Lsv;
use Illuminate\Database\Eloquent\SoftDeletes;

class UploadDir extends BaseLsv
{
    use SoftDeletes;
    protected  $casts = [
        'dir_pid' => 'integer',
    ];
    public function getDirPidAttribute():int
    {
          return $this->attributes['dir_pid']  === null ?  0 : (int)$this->attributes['dir_pid'];
    }
}
