<?php

namespace App\Models\Mysql\Orm\Lsv;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Casts\Serialize;

class Role extends BaseLsv
{
    use SoftDeletes;

    protected $casts = [
        'role_json' =>  Serialize::class,
    ];
}
