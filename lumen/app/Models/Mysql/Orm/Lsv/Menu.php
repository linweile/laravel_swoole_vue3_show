<?php
namespace App\Models\Mysql\Orm\Lsv;

class Menu extends BaseLsv{



    public function getMenuPidAttribute():int
    {
          return $this->attributes['menu_pid']  === null ?  0 : (int)$this->attributes['menu_pid'];
    }

    /**
     * 栏目的权限
     */
    public function auth()
    {

        return $this->hasMany(MenuAuth::class,'menu_id','menu_id')->orderBy('auth_sort','desc')->orderBy('auth_id','desc')->select(['auth_id','auth_name','menu_id']);
    }

}
