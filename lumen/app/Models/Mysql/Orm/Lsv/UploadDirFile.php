<?php

namespace App\Models\Mysql\Orm\Lsv;

use Illuminate\Database\Eloquent\SoftDeletes;

class UploadDirFile extends BaseLsv
{
    use SoftDeletes;

    /**
     * 指明模型的ID是否自动递增。
     *
     * @var bool
     */
    public $incrementing = false;
    /**
     * 自动递增ID的数据类型。
     *
     * @var string
     */
    protected $keyType = 'string';
}
