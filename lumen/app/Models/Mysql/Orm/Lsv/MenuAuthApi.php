<?php
namespace App\Models\Mysql\Orm\Lsv;
use App\Common\Helper;
use App\Common\Enum\Prefix;
class MenuAuthApi extends BaseLsv{

    // , DB::raw('IFNULL(lsv_menu_auth.auth_name,lsv_menu_auth_api.api_name) as auth_name')

    // public function getAuthNameAttribute(){
    //     return $this->attributes['lsv_menu_auth.auth_name'] ?? "11";
    // }

    public function getApiAttribute()
    {
        return ['full_url' => Helper::baseUrl(\AppRouter::ADMIN_API->prefix().'/'.$this->attributes['api'] ), 'value' => $this->attributes['api'] ] ;
    }
}
