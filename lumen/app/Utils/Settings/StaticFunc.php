<?php
namespace App\Utils\Settings;

use App\Common\Helper;

trait StaticFunc {
    public static function all()
    {
        $default = [];
        foreach (static::cases() as $val)   $default[$val->name] =  Helper::casts($val->casts(), $val->default());
        return  $default;
    }
}
