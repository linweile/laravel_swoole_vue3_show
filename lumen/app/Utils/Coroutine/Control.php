<?php

namespace App\Utils\Coroutine;

use Closure;
use Swoole\Coroutine;
use Swoole\Coroutine\Channel;
use App\Common\Helper;
class Control
{
    /**
     * @description:    需要做的并行任务操作
     * @param  array  $coroutine  回调数组执行并行任务
     * @return {*}
     */
    // public function __construct(
    //     public readonly array $coroutine,
    // ) {
    //     // foreach ($this->coroutine as $val)  if (!$val instanceof Closure)   Helper::throwError('必须是回调函数才能调用该类');
    // }

    /**
     * @description:  开始执行并行任务 按照数组形式回调结果
     * @return array  并行任务回调
     */
//     public function start(){
//         $channel = new Channel(1);
//         foreach ( $this->coroutine as $key => $val ){
//               Coroutine::create(function() use($channel,$key,$val) {
//                 $result = $val();
//                 $channel->push([$key,$result]);
//               });
//         }
//         $array = [];
//         while(1) {
//             $data = $channel->pop(-1);
//             if ($data) {
//                 $array[$data[0]] = $data[1];
//                 if(count($array) === count($this->coroutine)){
//                     $channel->close();
//                     break;
//                 }
//             } else {
//                 break;
//             }
//         }

//         return $array;

//     }
}
