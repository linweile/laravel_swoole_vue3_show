<?php
namespace App\Common\Enum;
//内部状态码错误
enum Http: int
{
  case  SUCCESS = 200;               //成功返回的参数
//   case  FAIL = 100;                //失败返回的参数
  case  LOGINOUT = 10002;             //登录过期返回的参数
//   case  SIGN_ERROR = -2;             //签名错误
//   case  FILE_ERROR  = 10001;       //内部php文件错误
}

