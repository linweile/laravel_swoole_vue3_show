<?php
namespace App\Common\Enum;
//内部状态码错误
enum Header: string
{
  case  ADMIN_LOGIN_TOKEN = "admin-access-token";                   //后台登录token
  case  ADMIN_REFRESH_TOKEN = "admin-refresh-token";                //后台刷新token
  case  ADMIN_ROUTER_PATH = "admin-router-path";                    //后台当前页面路径
  case  REFERER = 'referer';                                        //referer
  case  SIGN = "sign";                                              //签名
  case  REAL_IP = 'x-real-ip';                                      //真实ip
}

