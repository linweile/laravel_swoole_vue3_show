<?php

namespace App\Common\Enum;
//内部状态码错误
enum OssDirPrefix: string
{
    case  ADMINS = 'admins';
    case  COMMON =  'common';
}
