<?php

namespace App\Common\Http\Basics;

use App\Common\Http\Interface\Index;
use App\Common\Enum\TrueCode;

enum Fail implements Index
{
    case  PASS_ERROR ;
    case  INFO_NOT_DEFINED;
    case  DISABLE;
    case  ACCOUNT_IS_EXIST   ;
    case  MENU_IS_EXIST   ;
    case  API_IS_EXIST  ;
    case  API_IS_EXIST_ID  ;
    case  NOT_DEV_ROLE ;
    case  NOT_DEL_SELF ;
    case  NOT_DEV_ENV;
    case  ORI_PASS_ERROR;


    public function text(): string
    {
        /* 成功时定义 */
        return match ($this) {
            Fail::PASS_ERROR   => '账号密码错误',
            Fail::INFO_NOT_DEFINED   => '用户信息不存在',
            Fail::DISABLE   => '该账号已禁用',
            Fail::ACCOUNT_IS_EXIST   => '该账号已存在',
            Fail::MENU_IS_EXIST   => '该页面路径已存在',
            Fail::API_IS_EXIST  => '该栏目已存在该API',
            Fail::API_IS_EXIST_ID  => '该栏目已存在该API id:${api_id}',
            Fail::NOT_DEV_ROLE => '该账号不是开发者',
            Fail::NOT_DEL_SELF => '不能删除自己',
            Fail::NOT_DEV_ENV => '不是开发环境',
            Fail::ORI_PASS_ERROR => '原密码错误',
        };
    }

    public function code(): int
    {
        /* 成功时定义 */
        return match ($this) {
            Fail::PASS_ERROR   => 10004,
            Fail::ACCOUNT_IS_EXIST   => 10007,
            Fail::MENU_IS_EXIST   => 10008,
            Fail::API_IS_EXIST  => 10009,
            Fail::NOT_DEV_ROLE => 10010,
            Fail::INFO_NOT_DEFINED => 10012,
            Fail::DISABLE => 10013,
            Fail::API_IS_EXIST_ID  => 10009,
            Fail::NOT_DEL_SELF  => 10010,
            Fail::NOT_DEV_ENV  => 10011,
            Fail::ORI_PASS_ERROR  => 10012,
        };
    }

    public function trueCode(): TrueCode
    {
        return match ($this) {
            default => TrueCode::SUCCESS
        };
    }
}
