<?php

namespace App\Common\Http\Basics;

use App\Common\Http\Interface\Index;
use App\Common\Enum\TrueCode;
use App\Common\Enum\Http;

enum Upload implements Index
{
    case  UPLOAD_FILE_TYPE_ERROR;
    case  UPLOAD_FILE_SIZE_ERROR;
    case  UPLOAD_CAP_OVER;
    case  UPLOAD_FIRST_SUCCESS;
    case  SCHEDULE_CHANGE_SUCCESS;
    case  P_DIR_NOT_DEFINED;
    case  DIR_MORE_TWO_LEVE;

    public function text(): string
    {
        return match ($this) {
            Upload::UPLOAD_FILE_TYPE_ERROR => '上传文件类型错误',
            Upload::UPLOAD_FILE_SIZE_ERROR => '上传文件大小错误',
            Upload::UPLOAD_FIRST_SUCCESS => '第一次上传成功',
            Upload::P_DIR_NOT_DEFINED => '父级文件夹未定义',
            Upload::DIR_MORE_TWO_LEVE => '文件夹最多两级',
            Upload::SCHEDULE_CHANGE_SUCCESS => '修改上传进度成功',
            Upload::UPLOAD_CAP_OVER => '文件总容量不足，超出{cap}，请删除素材库文件',
        };
    }

    public function code(): int
    {
        return match ($this) {
            Upload::UPLOAD_FILE_TYPE_ERROR => 70001,
            Upload::UPLOAD_FILE_SIZE_ERROR => 70002,
            Upload::UPLOAD_FIRST_SUCCESS => Http::SUCCESS->value,
            Upload::P_DIR_NOT_DEFINED => 70003,
            Upload::DIR_MORE_TWO_LEVE => 70004,
            Upload::UPLOAD_CAP_OVER => 70005,
            Upload::SCHEDULE_CHANGE_SUCCESS => Http::SUCCESS->value,
        };
    }

    public function trueCode(): TrueCode
    {
        return match ($this) {
            default => TrueCode::SUCCESS
        };
    }
}
