<?php

namespace App\Common\Http\Basics;

use App\Common\Http\Interface\Index;
use App\Common\Enum\TrueCode;

enum Frequency implements Index
{

    case   NUM_ERROR;

    case   SECOND_ERROR;

    case   WAIT_TIME_ERROR;

    case  FREQUENT_REQUEST;

    case  FREQUENT_REQUEST_MID;

    case  IP_BANNED;

    public function text(): string
    {
        /* 成功时定义 */
        return match ($this) {
            Frequency::NUM_ERROR => 'num 必须大于 0',

            Frequency::SECOND_ERROR => 'second 必须大于 0',

            Frequency::WAIT_TIME_ERROR => 'wait time 必须为list解构数组',

            Frequency::FREQUENT_REQUEST => '请求频繁，请稍后重试',
            Frequency::FREQUENT_REQUEST_MID => '请求频繁，请稍后重试',
            Frequency::IP_BANNED => '恶意发送请求,ip已被封禁',
        };
    }

    public function code(): int
    {
        /* 成功时定义 */
        return match ($this) {
            Frequency::NUM_ERROR => 70001,

            Frequency::SECOND_ERROR => 70002,

            Frequency::WAIT_TIME_ERROR => 70003,

            Frequency::FREQUENT_REQUEST => 70004,

            Frequency::FREQUENT_REQUEST_MID => 70005,

            Frequency::IP_BANNED =>  70006,
        };
    }

    public function trueCode(): TrueCode
    {
        return match ($this) {
            Frequency::NUM_ERROR => TrueCode::SERVER_ERROR,
            Frequency::SECOND_ERROR => TrueCode::SERVER_ERROR,
            Frequency::WAIT_TIME_ERROR => TrueCode::SERVER_ERROR,
            default => TrueCode::SUCCESS
        };
    }
}
