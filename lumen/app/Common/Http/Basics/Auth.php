<?php

namespace App\Common\Http\Basics;

use App\Common\Http\Interface\Index;
use App\Common\Enum\TrueCode;

enum Auth implements Index
{
    case  ADMINS_ROUTER_PATH_NOT_DEFINED;
    case  NOT_AUTH;


    public function text(): string
    {
        return match ($this) {
            Auth::ADMINS_ROUTER_PATH_NOT_DEFINED => '没有传当前页面路径',
            Auth::NOT_AUTH => '没有该接口权限',

        };
    }

    public function code(): int
    {
        return match ($this) {
            Auth::ADMINS_ROUTER_PATH_NOT_DEFINED => 40301,
            Auth::NOT_AUTH => 40302,

        };
    }

    public function trueCode(): TrueCode
    {
        return match ($this) {
            default => TrueCode::FORBIDDEN
        };
    }
}
