<?php

namespace App\Common\Http\Basics;

use App\Common\Http\Interface\Index;
use App\Common\Enum\TrueCode;

enum Error implements Index
{
    case  MESSAGE;
    case  SERVER_ERROR;
    case  FILE_READ_ERROR;
    case  NOT_TMP_PATH;
    case  NOT_FILE_TYPE;
    case  ADMINS_ID_NOT_DEFIND;
    case  ADMINS_STATUS_NOT_DEFIND;
    case  MODULE_PARAMS_ERROR;
    case  ORM_CLASS_ERROR;
    case  DEV_ACCOUNT_CREATE_FAIL;
    case  UPLOAD_FILE_TYPE_ERROR;
    case  UPLOAD_FILE_SIZE_ERROR;
    case  NOT_FIND_FILE_OID;
    case  MOVE_FILE_ERROR;
    case  MODULES_FUNC_NOT_FIND;
        /* 403时定义 */
    case  NOT_AUTH;

        /* 404时定义 */
    case  API_NOT_FIND;

        /* 405时定义 */
    case  API_METHOD_ERROR;

    case  FORM_VAILD_ERROR;

    public function text(): string
    {
        /* 成功时定义 */
        return match ($this) {
            Error::MESSAGE => '{message}',
            Error::SERVER_ERROR => '系统错误',
            Error::FILE_READ_ERROR => '文件读取错误,可能是没有权限',
            Error::NOT_TMP_PATH =>  '文件临时路径不存在',
            Error::NOT_FILE_TYPE =>  '文件类型不为空',
            Error::ADMINS_ID_NOT_DEFIND => '没有admins_id字段',
            Error::ADMINS_STATUS_NOT_DEFIND => '没有status字段',
            Error::MODULE_PARAMS_ERROR => 'module参数错误',
            Error::ORM_CLASS_ERROR => '传入的orm类错误',
            Error::DEV_ACCOUNT_CREATE_FAIL => '开发者账号生成失败',

            Error::UPLOAD_FILE_TYPE_ERROR => '{file_name}文件类型暂不支持上传',
            Error::UPLOAD_FILE_SIZE_ERROR => '{file_name}文件大小暂不支持上传',
            Error::NOT_FIND_FILE_OID => '上传失败,请重新上传',
            Error::MOVE_FILE_ERROR => '文件移动失败,可能是权限不够',
            Error::MODULES_FUNC_NOT_FIND =>  '{class_name}没有{func}方法',
            /* 403时定义 */
            Error::NOT_AUTH => '没有该接口权限',

            /* 404时定义 */
            Error::API_NOT_FIND => '该资源不存在~',

            /* 405时定义 */
            Error::API_METHOD_ERROR => '{method}请求方式错误',

            Error::FORM_VAILD_ERROR => '表单验证错误',


        };
    }

    public function code(): int
    {
        /* 成功时定义 */
        return match ($this) {
            Error::SERVER_ERROR => 50012,
            Error::FILE_READ_ERROR => 50013,
            Error::NOT_TMP_PATH =>  50014,
            Error::NOT_FILE_TYPE =>  50015,
            Error::ADMINS_ID_NOT_DEFIND => 50016,
            Error::ADMINS_STATUS_NOT_DEFIND =>  50017,
            Error::MODULE_PARAMS_ERROR => 50020,
            Error::ORM_CLASS_ERROR => 50021,
            Error::DEV_ACCOUNT_CREATE_FAIL => 50022,
            Error::UPLOAD_FILE_TYPE_ERROR => 50025,
            Error::UPLOAD_FILE_SIZE_ERROR => 50026,
            Error::NOT_FIND_FILE_OID => 50027,
            Error::MOVE_FILE_ERROR => 50028,
            Error::MODULES_FUNC_NOT_FIND =>  50029,
            /* 403时定义 */
            Error::NOT_AUTH => 50030,

            /* 404时定义 */
            Error::API_NOT_FIND => 50031,

            /* 405时定义 */
            Error::API_METHOD_ERROR => 50032,

            Error::FORM_VAILD_ERROR => 50033,
            Error::MESSAGE => 50034,
        };
    }

    public function trueCode(): TrueCode
    {
        return match ($this) {
            Error::NOT_AUTH => TrueCode::FORBIDDEN,
            Error::API_NOT_FIND => TrueCode::NOT_FOUND,
            Error::API_METHOD_ERROR => TrueCode::NOT_ALLOWED,
            Error::FORM_VAILD_ERROR => TrueCode::DATA_ERROR,
            default => TrueCode::SERVER_ERROR
        };
    }
}
