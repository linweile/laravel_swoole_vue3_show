<?php

namespace App\Common\Http\Basics;

use App\Common\Http\Interface\Index;
use App\Common\Enum\TrueCode;
use App\Common\Enum\Http;

enum StatusPass implements Index
{
    case  ACCOUNT_NOT_EXIST;
    case  DISABLE;
    case  PASS_CHANGE;


    public function text(): string
    {
        return match ($this) {
            StatusPass::ACCOUNT_NOT_EXIST => '该账号不存在',
            StatusPass::DISABLE => '该账号已禁用',
            StatusPass::PASS_CHANGE => '该账号密码已发生改变',

        };
    }

    public function code(): int
    {
        return match ($this) {
            default => Http::LOGINOUT->value
        };
    }

    public function trueCode(): TrueCode
    {
        return match ($this) {
            default => TrueCode::SUCCESS
        };
    }
}
