<?php

namespace App\Common\Http\Basics;

use App\Common\Http\Interface\Index;
use App\Common\Enum\TrueCode;

enum Token implements Index
{
    case  TOKEN_NOT_HAVE ; //token不存在
    case  LOGIN_OUT   ;
    case  TOKEN_ERROR ;

    public function text(): string
    {
        /* 成功时定义 */
        return match ($this) {
            Token::TOKEN_NOT_HAVE => '非法请求',
            Token::LOGIN_OUT => '登录已过期',
            Token::TOKEN_ERROR => 'token错误',
        };
    }

    public function code(): int
    {
        /* 成功时定义 */
        return match ($this) {
            Token::TOKEN_NOT_HAVE => 10001,
            Token::LOGIN_OUT =>  \App\Common\Enum\Http::LOGINOUT->value,
            Token::TOKEN_ERROR => 10003
        };
    }

    public function trueCode(): TrueCode
    {
        return match ($this) {
            Token::TOKEN_NOT_HAVE => TrueCode::BAD_REQUEST,
            Token::TOKEN_ERROR => TrueCode::BAD_REQUEST,
            default => TrueCode::SUCCESS
        };
    }
}
