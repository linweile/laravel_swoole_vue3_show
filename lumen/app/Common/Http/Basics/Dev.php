<?php

namespace App\Common\Http\Basics;

use App\Common\Http\Interface\Index;
use App\Common\Enum\TrueCode;
use App\Common\Enum\Http;

enum Dev implements Index
{
    case  DEV_ACCOUNT_NOT_EXIST;
    case  NOT_DEV_ROLE;


    public function text(): string
    {
        return match ($this) {
            Dev::DEV_ACCOUNT_NOT_EXIST => '账号不存在',
            Dev::NOT_DEV_ROLE => '没有开发者权限',


        };
    }

    public function code(): int
    {
        return match ($this) {
            Dev::DEV_ACCOUNT_NOT_EXIST => Http::LOGINOUT->value,
            Dev::NOT_DEV_ROLE => 40399,
        };
    }

    public function trueCode(): TrueCode
    {
        return match ($this) {
            Dev::NOT_DEV_ROLE => TrueCode::FORBIDDEN,
            default => TrueCode::SUCCESS
        };
    }
}
