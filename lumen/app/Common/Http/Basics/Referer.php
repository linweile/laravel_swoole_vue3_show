<?php

namespace App\Common\Http\Basics;

use App\Common\Http\Interface\Index;
use App\Common\Enum\TrueCode;

enum Referer implements Index
{

    case  REQ_ILLEGAL;  //请求不合法

    public function text(): string
    {
        /* 成功时定义 */
        return match ($this) {
            Referer::REQ_ILLEGAL => '资源不存在~',
        };
    }

    public function code(): int
    {
        /* 成功时定义 */
        return match ($this) {
            Referer::REQ_ILLEGAL => 91001,
        };
    }

    public function trueCode(): TrueCode
    {
        return match ($this) {
            Referer::REQ_ILLEGAL => TrueCode::NOT_FOUND,
            default => TrueCode::BAD_REQUEST
        };
    }
}
