<?php

namespace App\Common\Http\Basics;

use App\Common\Http\Interface\Index;
use App\Common\Enum\TrueCode;
use App\Common\Enum\Http;

enum SignError implements Index
{
    case  SIGN_ERROR;
    case  PARAMS_ERROR;

    public function text(): string
    {
        return match ($this) {
            SignError::SIGN_ERROR => '签名错误',
            SignError::PARAMS_ERROR => '参数错误',

        };
    }

    public function code(): int
    {
        return match ($this) {
            SignError::SIGN_ERROR => 40311,
            SignError::SIGN_ERROR => 40312,
        };
    }

    public function trueCode(): TrueCode
    {
        return match ($this) {
            default => TrueCode::FORBIDDEN
        };
    }
}
