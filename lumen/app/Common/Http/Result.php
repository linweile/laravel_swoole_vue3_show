<?php

namespace App\Common\Http;

// use App\Common\Enum\ResultMsg;
// use App\Common\Enum\Http;
use App\Common\Http\Interface\Index as Http;
use App\Common\Http\Basics\Success;
use App\Common\Enum\TrueCode;
//内部状态码错误
class Result
{

    /**
      *  @param mixed $data 返回的数据
      *  @param Http  $http 返回的信息
      *  @param array $header   响应头信息
      *  @param array $replace  需要替换的字段
      *  @param \App\Common\Enum\TrueCode $trueCode 网页状态码
      **/
    public function __construct(
        private   mixed      $data = [],
        private   Http       $http = Success::NOT_DEFIND,
        private   array      $header = [],
        private   array      $replace = []
    ) {
    }


    public function getMessage(): string
    {
        $msg = $this->http->text();
        foreach ($this->replace as $key => $val) $msg = str_replace('{' . $key . '}', $val, $msg);
        return $msg;
    }

    public function getReplace():array{
         return $this->replace;
    }

    public function getHttpVal(): int
    {
        return $this->http->code();
    }

    public function getTrueCodeVal(): int
    {
        return $this->http->trueCode()->value;
    }

    public function getHttpName(): string
    {
        return $this->http->name;
    }

    public function getHttpEnum(): Http
    {
        return $this->http;
    }

    public function getTrueCodeName(): string
    {
        return $this->http->trueCode()->name;
    }


    public function getHeader(): array
    {
        return $this->header;
    }



    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): array
    {
        return $this->data = $data;
    }



    public function addHeader(array $header)
    {
        $this->header = array_merge($this->header,$header);
    }

    public function setHttp(Http $http,array|null $replace = null)
    {
        $this->http =  $http;
        if($replace) $this->replace =  $replace;
    }



    // public function setData(array $data)
    // {
    //     $this->data = $data;
    // }

    // public function setHttp(Http $http)
    // {
    //     $this->http = $http;
    // }

    // public function setTrueCode(TrueCode $data)
    // {
    //     $this->trueCode = $data;
    // }
}
