<?php
namespace App\Common\Http\Interface;
use App\Common\Enum\TrueCode;
interface Index{
    public function text():string ;

    public function code():int ;

    public function trueCode():TrueCode ;
}
