<?php

namespace App\Services;
use App\Utils\JWT\Control;
// use  App\Dao\Lsv\AdminDao;
// use App\Utils\Alibaba\Sms\Control;

abstract class BaseService
{

    public static function adminLogin() :Control
    {
        return new Control('adminLogin',-1,7 * 24 * 3600)  ;
    }

    public static function async(...$params)
    {
        return new static(...$params);
    }

}
