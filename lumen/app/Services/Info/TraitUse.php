<?php

namespace App\Services\Info;


use App\Common\Enum\Attributes;
use Illuminate\Http\Request;

trait  TraitUse
{
    public array $loginInfo = [];
    use TraitFunc;
    public function getSetInfoReq(Request $request, Attributes $attr ,string $primary_key,string $orm, array $field = [],\Closure|null $nullRes = null)
    {
        if($nullRes !== null ) $this->nullRes = $nullRes;
        $this->loginInfo  = $request->get($attr->value, []);  //中间件那里解析到的获取登录信息
        $userInfo =  $this->getInfo($primary_key,$orm ,$field);
        $request->attributes->add([$attr->value => $this->info]);             //重新保存用户信息
        return  $userInfo;
    }
}
