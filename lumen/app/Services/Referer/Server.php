<?php

namespace App\Services\Referer;
use App\Utils\CheckReferer\Control;
use App\Common\Enum\Referer;
class Server{
    public static function admin(\Illuminate\Http\Request $request ){
        return  new Control($request, Referer::ADMIN);
    }
}
