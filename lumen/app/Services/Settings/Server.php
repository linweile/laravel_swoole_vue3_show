<?php

namespace App\Services\Settings;

use App\Services\Settings\Default\Dev as DevDefault;
use App\Utils\Settings\Control;
use App\Models\Mysql\Orm\Lsv\DevSettings;

class Server
{
    public static function dev(): Control
    {
        return new Control(DevDefault::class, new DevSettings);
    }
}
