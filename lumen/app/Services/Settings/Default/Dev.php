<?php

namespace App\Services\Settings\Default;
use App\Utils\Settings\Interfaces\ControlInterface;
use App\Utils\Settings\StaticFunc;
enum Dev implements ControlInterface
{
    use StaticFunc;
    /* 配置参数 数据库lsv_dev_settings表有字段注释 */
    /**
     * @description: 文件切片大小
     */
    case upload_slice_size;

    /**
     * @description: 素材库最大容量
     */
    case library_max_capacity;
    /**
     * @description: 地址前缀
     */
    case prefix_url;
    /**
     * @description: 文件上传最大大小
     */
    case upload_max_size;
    /**
     * @description: 文件上传类型
     */
    case upload_file_type;
    /**
     * @description: 是否检查文件上传类型
     */
    case upload_check_file_type;

    /**
     * @description: 是否开启oss校验
     */
    case oss_start;
    /**
     * @description: oss_access_key_id
     */
    case oss_access_key_id;
    /**
     * @description: oss_access_key_secret
     */
    case oss_access_key_secret;
    /**
     * @description: oss_endpoint
     */
    case oss_endpoint;
    /**
     * @description: oss_bucket
     */
    case oss_bucket;

    /**
     * @description: oss_role_arr
     */
    case oss_role_arr;




    /* 配置默认值 */
    public function default()
    {
        return match ($this) {
            Dev::upload_slice_size => 100,
            Dev::upload_max_size => 204800,
            Dev::prefix_url => "",
            Dev::upload_file_type => array(['type' => 'image' , 'name' => '图片']),
            Dev::upload_check_file_type => 1,
            Dev::oss_start => 0,
            Dev::oss_access_key_id => "",
            Dev::oss_access_key_secret => "",
            Dev::oss_endpoint => "",
            Dev::oss_bucket => "",
            Dev::oss_role_arr => "",
            Dev::library_max_capacity => 1024 * 1024,
        };
    }

    /*配置属性转换 在 app/Common/Function.php 文件里conversion 函数 */
    public function casts():string
    {
        return match ($this) {
            Dev::upload_slice_size => 'integer',
            Dev::upload_max_size => 'integer',
            Dev::upload_file_type => 'serialize',
            Dev::upload_check_file_type => 'integer',
            Dev::oss_start => 'integer',
            Dev::oss_access_key_id => "string",
            Dev::prefix_url => "string",
            Dev::oss_access_key_secret => "string",
            Dev::oss_endpoint => "string",
            Dev::oss_bucket => "string",
            Dev::oss_role_arr => "string",
            Dev::library_max_capacity => "integer",
            default => 'self'
        };
    }





}
