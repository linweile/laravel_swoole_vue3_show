<?php

namespace App\Services\JWT;
use App\Utils\JWT\Control;

class Server extends \App\Services\BaseService
{
    /**
     * @description:  后台管理员登录token
     * @return Control
     */
    public static function adminLogin() :Control
    {
        return new Control('admin_login',-1,7 * 24 * 3600)  ;
    }

     /**
     * @description:  后台管理员刷新token
     * @return Control
     */
    public static function adminRefresh() :Control
    {
        return new Control('admin_refresh',-1,60 * 30)  ;
    }



}
