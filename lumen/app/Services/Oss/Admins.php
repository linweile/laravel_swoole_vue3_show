<?php

namespace App\Services\Oss;

use App\Services\Settings\Server as SettingServer;
use  App\Utils\Oss\StsToken;
use  App\Utils\Oss\Proxy as OssProxy;
use  App\Dao\Mongo\FileSlice;

class Admins extends \App\Services\BaseService
{
   public static function del(string $admins_id,int $is_common,string $oid){
        $file = FileSlice::oidFindOne($admins_id, $is_common, $oid);
        if($file){
           $res = (new Server())->control()->deleteObject(  $file->file_url );
           if($res){
                FileSlice::del($admins_id, $is_common,[$file->_id]);
           }
        }
   }

}
