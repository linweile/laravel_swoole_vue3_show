<?php

namespace App\Services\Oss;

use App\Services\Settings\Server as SettingServer;
use  App\Utils\Oss\StsToken;
use  App\Utils\Oss\Proxy as OssProxy;

class Server extends \App\Services\BaseService
{

    private readonly array $settings ;

    public function __construct()
    {
      $this->settings =  SettingServer::dev()->getItem(["upload_file_type", "upload_check_file_type", "upload_max_size", "upload_slice_size", "oss_start", "oss_access_key_id", "oss_access_key_secret", "oss_endpoint", "oss_bucket", "oss_role_arr"]);
    }

    public function getSettings(){
        return  $this->settings;
    }


    public function stsToken(){
        return  (new StsToken(
            $this->settings["oss_access_key_id"],
            $this->settings["oss_access_key_secret"],
            $this->settings["oss_endpoint"],
            $this->settings["oss_role_arr"],
        ));
    }


    public function control(){
        return (new OssProxy(
            $this->settings["oss_access_key_id"],
            $this->settings["oss_access_key_secret"],
            $this->settings["oss_endpoint"],
            $this->settings["oss_bucket"],
        ));
    }





}
