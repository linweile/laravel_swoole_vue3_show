<?php
namespace App\Exceptions;
use Exception;
use App\Common\Http\Result;
abstract class BaseExceptions extends Exception
{

    public function __construct(
        public readonly Result $result,
        private Exception|null $exception = null
    )
    {
        parent::__construct($this->result->getMessage() );
    }


    public function getResult():Result
    {
         return $this->result;
    }

    public function getException():Exception|null{
        return $this->exception;
    }
}
