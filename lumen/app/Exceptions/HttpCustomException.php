<?php
namespace App\Exceptions;
use App\Common\Http\Interface\Index;
use App\Common\Http\Result;
class HttpCustomException extends BaseExceptions
{
    public function __construct(
        public readonly Index $http,
        public readonly array  $replace = [],
        public readonly \Exception|null  $exception = null
    )
    {
        parent::__construct(new Result([],$this->http,[],$this->replace) , $this->exception );
    }
}
