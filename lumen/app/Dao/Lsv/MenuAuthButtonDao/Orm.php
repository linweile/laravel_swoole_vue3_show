<?php

namespace  App\Dao\Lsv\MenuAuthButtonDao;

use App\Dao\Lsv\BaseLsv;
use App\Models\Mysql\Orm\Lsv\MenuAuthButtons;

class Orm extends BaseLsv
{
    public function __construct()
    {
        $this->query = new MenuAuthButtons;
    }

    public  function inAuthId(array $auth):Orm
    {
        $this->query =  $this->query->where(function ($query) use ($auth) {
            foreach ($auth as $val) $query->orWhere('auth_id', $val['auth_id']);
        });
        return $this;
    }

    public function add(array $params):Orm{
        $this->query->buttons = $params['buttons'];
        $this->query->auth_id = $params['auth_id'];
        $this->query->save();
        return $this;
    }

    public function whereButtonsId(int $button_id):Orm{
        $this->query = $this->query->where('buttons_id',$button_id);
        return $this;
    }

    public function edit(array $params):Orm{
        $this->query->update(['buttons'=> $params['buttons'],'auth_id' => $params['auth_id']]);
        return $this;
    }


    // public function add(array $params):Orm{
    //     $this->query->buttons = $params['buttons'];
    //     $this->query->auth_id = $params['auth_id'];
    //     return $this;
    // }


    // function ($query) use ($menu, $auth) {
    //     $query->where(function ($que) use ($menu) {
    //         foreach (array_keys($menu) as $val) $que->orWhere('menu_id', $val);
    //     });
    //     $query->where(function ($que) use ($auth) {
    //         foreach ($auth as $val) $que->orWhere('auth_id', $val);
    //     });
    // }
}
