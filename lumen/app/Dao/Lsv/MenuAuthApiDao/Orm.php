<?php

namespace  App\Dao\Lsv\MenuAuthApiDao;

use App\Dao\Lsv\BaseLsv;
use App\Models\Mysql\Orm\Lsv\MenuAuthApi;

class Orm extends BaseLsv
{
    public function __construct()
    {
        $this->query = new MenuAuthApi;
    }

    public  function inAuthId(array $auth): Orm
    {
        $this->query =  $this->query->where(function ($query) use ($auth) {
            foreach ($auth as $val) $query->orWhere('auth_id', $val['auth_id']);
        });
        return $this;
    }


    public function  menuJoin(): Orm
    {
        $this->query =  $this->query->join('menu', 'menu.menu_id', '=', 'menu_auth_api.menu_id');
        return $this;
    }


    public function  menuAuthLeftJoin(): Orm
    {
        $this->query =  $this->query->leftJoin('menu_auth', 'menu_auth.auth_id', '=', 'menu_auth_api.auth_id');
        return $this;
    }


    public function wherePath(string $path): Orm
    {
        $this->query =  $this->query->where('menu.path', $path);
        return $this;
    }

    public function whereMenuAuthApi(string $api): Orm
    {
        $this->query =  $this->query->where('menu_auth_api.api', $api);
        return $this;
    }

    public function whereMenuApiId(int $api_id): Orm
    {
        $this->query =  $this->query->where('menu_auth_api.api_id', $api_id);
        return $this;
    }


    public function whereMenuId(int $menu_id): Orm
    {
        $this->query =  $this->query->where('menu_auth_api.menu_id', $menu_id);
        return $this;
    }

    public function whereApi(string $api)
    {
        $this->query =  $this->query->where('menu_auth_api.api', $api);
        return $this;
    }

    public function whereNotApiId(int $api_id)
    {
        $this->query =  $this->query->where('menu_auth_api.api_id', '<>', $api_id);
        return $this;
    }


    public function add(array $params)
    {
        $this->query->api = $params['api'];
        $this->query->menu_id = $params['menu_id'];
        $this->query->add_log = $params['add_log'];
        $this->query->api_name = $params['api_name'] ?? "";
        $this->query->auth_id = $params['auth_id'] ?? null;
        $this->query->save();
    }

    public function whereAuthId(null|int $auth_id)
    {
        $this->query =  $this->query->where('menu_auth_api.auth_id', $auth_id);
        return $this;
    }

    public function whereApiId(int $api_id)
    {
        $this->query =  $this->query->where('menu_auth_api.api_id', $api_id);
        return $this;
    }

    public function whereInApiId(array $api_id)
    {
        $this->query =  $this->query->whereIn('menu_auth_api.api_id', $api_id);
        return $this;
    }

    public function edit(array $params)
    {
        return  $this->query = $this->query->update(array_merge(
            [
                'api' => $params['api'],
                'add_log' => $params['add_log'],
            ],
            isset($params['api_name']) ? ['api_name' =>  $params['api_name']] : [],
            isset($params['auth_id']) ? ['auth_id' =>  $params['auth_id']] : []
        ));
    }

    public function addLog(array $params)
    {
        return  $this->query = $this->query->update(['add_log' => $params['add_log']]);
    }

}
