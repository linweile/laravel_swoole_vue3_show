<?php

namespace App\Dao\Lsv\AdminDao;

use App\Models\Mysql\Orm\Lsv\Admins;
use App\Dao\Lsv\BaseLsv;
use App\Dao\Lsv\AdminDao\Enums\Status;
use App\Utils\Nanoid\Control as Nanoid;
class Orm extends BaseLsv
{
    public function __construct()
    {
        $this->query = new Admins;
    }

    public function withRole(){
        $this->query = $this->query->with(['role']);
        return $this;
    }

    public function checkUser(string $username): Orm
    {
        $this->query = $this->query->where('username', $username);
        return $this;
    }

    public function whereAdminsId(string $admins_id): Orm{
        $this->query = $this->query->where('admins_id', $admins_id);
        return $this;
    }
    public function editPass(string $password):int{
        //非开发者账号才能修改密码
         return  $this->query->where('role_id' , '>=' , 0)->update(['password'=> $password]);
    }

    public function wherePassWord(string $password){
        $this->query = $this->query->where('password' , $password );
        return $this;
    }

    public function checkNotAdminsId(string $admins_id): Orm
    {
        $this->query = $this->query->where('admins_id','<>',$admins_id);
        return $this;
    }

    public function checkPass(string $pass): Orm
    {
        $this->query = $this->query->where('password', $pass);
        return $this;
    }

    public function checkAdminsId(string $admins_id): Orm
    {
        $this->query = $this->query->where('admins_id', $admins_id);
        return $this;
    }

    public function whereAdminsIdIn(array $ids): Orm
    {
        $this->query =  $this->query->whereIn('admins_id', $ids);
        return $this;
    }

    public function whereRoleId(int $role_id): Orm
    {
        $this->query = $this->query->where('admins.role_id', $role_id);
        return $this;
    }

    public function whereNickName(string $nickName): Orm
    {
        $this->query = $this->query->where('admins.nickName', 'like', $nickName . '%');
        return $this;
    }

    public function whereRealName(string $realName): Orm
    {
        $this->query = $this->query->where('admins.realName', 'like', $realName . '%');
        return $this;
    }

    public function whereUserName(string $user_name): Orm
    {
        $this->query = $this->query->where('admins.username', 'like', $user_name . '%');
        return $this;
    }


    public function leftJoinRole(): Orm
    {
        $this->query =  $this->query->leftJoin('role', 'role.role_id', '=', 'admins.role_id');
        return $this;
    }

    /**
     * @description:  非超级管理员
     * @return {*}
     */
    public function notSuper(): Orm
    {
        $this->query =  $this->query->where('admins.role_id', '>', '0');
        return $this;
    }

    public function sortDesc(): Orm
    {
        $this->query =  $this->query->orderBy('admins.admins_id', 'desc');
        return $this;
    }


    public function updateStatus($status): Orm
    {
        $this->query =  $this->query->update(['status' => Status::from($status)->value]);
        return $this;
    }



    public function add(array $params)
    {
        $this->query->username = $params['username'];
        $this->query->admins_id = Nanoid::create();
        $this->query->password = $params['password'];
        $this->query->status   = $params['status'];
        $this->query->nickName = $params['nickName'];
        $this->query->realName = $params['realName'];
        $this->query->phone = $params['phone'];
        if (isset($params['portrait'])) $this->query->portrait = $params['portrait'];
        $this->query->role_id = $params['role_id'];
        $this->query->dev_pwd = $params['dev_pwd'];
        $this->query->save();
        return $this->query;
    }


    public function edit(array $params)
    {
       return $this->query->where('role_id', '>', 0)->where('admins_id', $params['admins_id'])->update(
            array_merge(
                [
                    'status' => $params['status'],
                    'username' => $params['username'],
                    'status' => $params['status'],
                    'nickName' => $params['nickName'],
                    'realName' => $params['realName'],
                    'phone' => $params['phone'],
                    'portrait' => $params['portrait'] ?? "",
                    'role_id' => $params['role_id'],
                ],
                isset($params['password']) ? ['password' => $params['password']] : [],
            )
        );
    }

    public function editUserInfo(string $admins_id,array $params)
    {
       return $this->query->where('role_id', '>=', 0)->where('admins_id', $admins_id)->update(
            array_merge(
                [
                    'username' => $params['username'],
                    'nickName' => $params['nickName'],
                    'realName' => $params['realName'],
                    'phone' => $params['phone'],
                    'portrait' => $params['portrait'] ?? "",
                ],
                isset($params['password']) ? ['password' => $params['password']] : [],
            )
        );
    }


    public function reloadPass(array $params){
        $this->query->where('role_id',-1)->update(['dev_pwd'=>$params['dev_pwd'],'password' => $params['password'] ]);
        return $this ;
    }


}
