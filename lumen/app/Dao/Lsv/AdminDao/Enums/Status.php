<?php
namespace App\Dao\Lsv\AdminDao\Enums;

enum Status: int{
    case ENABLE = 1;
    case FORBIDDEN = 0;
}
