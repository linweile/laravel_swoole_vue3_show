<?php

namespace  App\Dao\Lsv\RoleDao;

use App\Models\Mysql\Orm\Lsv\Role;
use App\Dao\Lsv\BaseLsv;

class Orm extends BaseLsv
{

    public function __construct()
    {
        $this->query = new Role;
    }


    public function whereRole(int $role_id){
        $this->query =  $this->query->where('role_id', $role_id);
        return $this;
    }

    public function orderByRoleSort(string $sort = 'desc'){
        $this->query =  $this->query->orderBy('role_sort', $sort);
        return $this;
    }

    public function orderByRoleId(string $sort = 'desc'){
        $this->query =  $this->query->orderBy('role_id', $sort);
        return $this;
    }


    public function add(array $params){
        $this->query->role_name = $params['role_name'];
        $this->query->role_json = [];
        $this->query->role_sort = $params['role_sort'];
        $this->query->role_describe = $params['role_describe'];
        $this->query->save();
    }


    public function roleNameLike(string $role_name){

        $this->query = $this->query->where('role_name','like', $role_name.'%');

        return $this;

    }


    public function del(array $ids){
        $this->query = $this->query->destroy($ids);
        return $this;
    }

    public function edit(array $params,int $role_id){
       return $this->query->where('role_id', $role_id)->update([
            "role_name" => $params['role_name'],
            "role_sort" => $params['role_sort'],
            "role_describe" => $params['role_describe']
        ]);
    }


    public  function set(array $params, int $role_id)
    {
        return  $this->query->where('role_id', $role_id)->update([
            "role_json" => serialize( $params['role_json'] ),
        ]);
    }



    // ->orderBy('role_sort','desc')->orderBy('role_id','desc')->get(['role_name','role_id'])




}
