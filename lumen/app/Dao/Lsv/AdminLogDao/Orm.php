<?php

namespace App\Dao\Lsv\AdminLogDao;

use App\Dao\Lsv\BaseLsv;
use App\Models\Mysql\Orm\Lsv\AdminsLog;

class Orm extends BaseLsv
{

    public function __construct()
    {
        $this->query = new AdminsLog;
    }

    public function logIdSort(string $sort = 'desc'): Orm
    {
        $this->query = $this->query->orderBy('admins_log.log_id', $sort);
        return $this;
    }

    public function joinAdmin(): Orm
    {
        $this->query = $this->query->join('admins', 'admins.admins_id', '=', 'admins_log.admins_id');
        return $this;
    }

    public function whereLogId(int $log_id): Orm
    {
        $this->query = $this->query->where('admins_log.log_id', $log_id);
        return $this;
    }

    public function whereMenuTitle(string $menu_title): Orm
    {
        $this->query = $this->query->where('admins_log.menu_title', 'like', $menu_title . '%');
        return $this;
    }

    public function whereRouterPath(string $router_path): Orm
    {
        $this->query = $this->query->where('admins_log.router_path', 'like', $router_path . '%');
        return $this;
    }

    public function whereApi(string $api): Orm
    {
        $this->query = $this->query->where('admins_log.api', 'like', $api . '%');
        return $this;
    }

    public function whereOperationTime(string $operation_time): Orm
    {
        $this->query = $this->query->whereDate('admins_log.operation_time', $operation_time);
        return $this;
    }

    public function whereAuthName(string $auth_name): Orm
    {
        $this->query = $this->query->where('admins_log.auth_name', 'like', $auth_name . '%');
        return $this;
    }

    public function whereIp(string $ip): Orm
    {
        $this->query = $this->query->where('admins_log.ip', 'like', $ip . '%');
        return $this;
    }

    public function whereAdminsNickName(string $nickName): Orm
    {
        $this->query = $this->query->where('admins.nickName', 'like', $nickName . '%');
        return $this;
    }

    public function whereAdminsRealName(string $realName): Orm
    {
        $this->query = $this->query->where('admins.realName', 'like', $realName . '%');
        return $this;
    }


    // 'admins_log.log_id' => ['value' => $params['log_id'] ?? "", 'type' => 'equal'],
    // 'admins_log.menu_title' => $params['menu_title'] ?? "",
    // 'admins_log.router_path' => $params['router_path'] ?? "",
    // 'admins_log.api' => $params['api'] ?? "",
    // 'admins_log.operation_time' => ['value' => $params['operation_time'] ?? "" , 'type' => 'date' ],
    // 'admins_log.auth_name' => $params['auth_name'] ?? "",
    // 'admins_log.ip' => $params['ip'] ?? "",
    // 'admins.nickName' => $params['nickName'] ?? "",
    // 'admins.realName' => $params['realName'] ?? "",

}
