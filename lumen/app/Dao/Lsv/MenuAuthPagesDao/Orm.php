<?php

namespace  App\Dao\Lsv\MenuAuthPagesDao;

use App\Models\Mysql\Orm\Lsv\MenuAuthPages;
use Illuminate\Support\Facades\DB;
use App\Common\Helper;
use App\Dao\Lsv\BaseLsv;

class Orm extends BaseLsv
{
    public function __construct()
    {
        $this->query = new MenuAuthPages;
    }

    public function add(array $params)
    {
        $this->query->path = $params['path'];
        $this->query->auth_id = $params['auth_id'];
        $this->query->name = $params['name'];
        $this->query->redirect = $params['redirect'];
        $this->query->component = $params['component'];
        // $this->query->menu_sort = $params['menu_sort'];
        $this->query->icon = $params['icon'];
        $this->query->title = $params['title'];
        $this->query->isLink = $params['isLink'];
        $this->query->isFull = $params['isFull'];
        $this->query->isAffix = $params['isAffix'];
        $this->query->isKeepAlive = $params['isKeepAlive'];
        $this->query->save();
    }

    public function wherePagesId(int $pages_id): Orm
    {
        $this->query = $this->query->where('pages_id', $pages_id);

        return  $this;
    }

    public function edit(array $params)
    {
        return $this->query->update([
            'path' => $params['path'],
            'component' => $params['component'],
            'auth_id'  => $params['auth_id'],
            'name' => $params['name'],
            'redirect' => $params['redirect'],
            'icon' => $params['icon'],
            'title' => $params['title'],
            'isLink' => $params['isLink'],
            'isFull' => $params['isFull'],
            'isAffix' => $params['isAffix'],
            'isKeepAlive' => $params['isKeepAlive'],
        ]);
    }

    public function whereInAuthId(array $auth_id):Orm
    {
        $this->query = $this->query->where(function($query) use ($auth_id){
            if(!empty($auth_id)) $query->whereIn('auth_id',$auth_id);
        });

        return  $this;
    }
}
