<?php

namespace App\Dao\Lsv\AdminDelDao;

use App\Models\Mysql\Orm\Lsv\AdminsDel;
use App\Dao\Lsv\BaseLsv;
use Closure;

// use App\Dao\Lsv\AdminDao\Enums\Status;

class Orm extends BaseLsv
{

    public function __construct()
    {
        $this->query = new AdminsDel;
    }

    public function addAll(array $data,Closure $func){
        $this->query->insertAll($data,[],$func);
    }
}
