<?php

namespace  App\Dao\Lsv;

use  App\Models\Mysql\Orm\Lsv\UploadDir;
use App\Common\Helper;
use Illuminate\Support\Facades\DB;

class UploadDirDao extends BaseLsv
{

    public static function add(array $params, int|null $admins_id)
    {
        $model =  static::loadOrm(UploadDir::class);
        $model->dir_name =  $params['dir_name'];
        $model->admins_id =  $admins_id;
        $model->dir_pid =  $params['dir_pid'] == 0 ? null : $params['dir_pid'];
        $model->dir_sort =  $params['dir_sort'];
        $model->save();
        return  $model;
    }
    public static function edit(array $params, null|int $admins_id)
    {
        $model =  static::loadOrm(UploadDir::class)->where('dir_id', $params['dir_id'])->where('admins_id', $admins_id)->update([
            'dir_name' => $params['dir_name'],
            'dir_sort' => $params['dir_sort'],
        ]);
        return  $model;
    }

    public static function del(array $params, null|int $admins_id)
    {
        $model =  static::loadOrm(UploadDir::class)->where('admins_id', $admins_id)->where(function($query) use ($params){
            foreach($params['ids'] as $val) $query->orWhere('dir_id',$val);
        })->delete();
        return  $model;
    }
    public static function all(null|int $admins_id)
    {
        $data =  static::loadOrm(UploadDir::class)->where('admins_id', $admins_id)->get([
            'dir_id', 'dir_name', 'dir_sort', 'dir_pid', 'admins_id'
        ])->toArray();

        return  Helper::createTree($data, 'dir_id', 'dir_pid');
    }
}
