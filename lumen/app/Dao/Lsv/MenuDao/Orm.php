<?php

namespace  App\Dao\Lsv\MenuDao;

use App\Models\Mysql\Orm\Lsv\Menu;
use Illuminate\Support\Facades\DB;
use App\Common\Helper;
use App\Dao\Lsv\BaseLsv;

class Orm extends BaseLsv
{
    public function __construct()
    {
        $this->query = new Menu;
    }

    public function inMenuId(array $menu_id_arr): Orm
    {
        $this->query =  $this->query->where(function ($query) use ($menu_id_arr) {
            if (!empty($menu_id_arr)) $query->whereIn('menu_id', $menu_id_arr);
        });

        return $this;
    }


    public function inMenuIdOrAuth(array $menu_id_arr,array $auth_id):Orm
    {
        $this->query =  $this->query->where(function ($query) use ($menu_id_arr,$auth_id) {
         if(!empty($menu_id_arr)) $query->whereIn('menu_id', $menu_id_arr);
         if(!empty($auth_id)) $query->orWhereIn('auth_id', $auth_id);
        });

        return $this;
    }



    public function whereMenuId(int $menu_id): Orm
    {

        $this->query =  $this->query->where('menu_id', $menu_id);

        return $this;
    }

    public function menuSort($sort = 'desc'): Orm
    {
        $this->query =  $this->query->orderBy('menu_sort', $sort);
        return $this;
    }

    public function menuIdSort($sort = 'desc'): Orm
    {
        $this->query =  $this->query->orderBy('menu_id', $sort);
        return $this;
    }


    public function withAuth(): Orm
    {

        $this->query =  $this->query->with(['auth']);
        // (new Menu)->with(['auth'])->orderBy('menu_sort', 'desc')->orderBy('menu_id', 'desc')->get([
        //     'menu_id',
        //     'title',
        //     'menu_pid',
        // ]);
        return $this;
    }

    public function whereAuthId(int|null $auth_id)
    {
        $this->query =  $this->query->where('auth_id', $auth_id);
        return $this;
    }


    public function wherePath(string $path)
    {
        $this->query =  $this->query->where('path', $path);
        return $this;
    }

    public function whereNotMenuId(int $menu_id)
    {
        $this->query =  $this->query->where('menu_id', '<>', $menu_id);
        return $this;
    }


    public function whereAuth()
    {
        $this->query =  $this->query->where('auth_id', '<>', null);
        return $this;
    }




    // public function menuSort($sort = 'desc'):Orm{
    // $this->query =  $this->query->orderBy('menu_sort',$sort);
    // (new Menu)->with(['auth'])->orderBy('menu_sort', 'desc')->orderBy('menu_id', 'desc')->get([
    //     'menu_id',
    //     'title',
    //     'menu_pid',
    // ]);
    //     return $this;
    // }




    public  function add(array $params)
    {
        $this->query->menu_pid = $params['menu_pid'] == 0 ? null : $params['menu_pid'];
        $this->query->path = $params['path'];
        $this->query->name = $params['name'];
        $this->query->redirect = $params['redirect'];
        $this->query->component = $params['component'];
        $this->query->icon = $params['icon'];
        $this->query->title = $params['title'];
        $this->query->isLink = $params['isLink'];

        $this->query->isFull = $params['isFull'];
        $this->query->isAffix = $params['isAffix'];
        $this->query->isKeepAlive = $params['isKeepAlive'];
        if (isset($params['auth_id'])) {
            //添加权限
            $this->query->auth_id = $params['auth_id'];
            $this->query->isHide = 1;
        } else {
            $this->query->isHide = $params['isHide'];
            $this->query->menu_sort = $params['menu_sort'];
        }

        $this->query->save();
    }



    public function whereInId(array $ids = [])
    {
        $this->query =  $this->query->whereIn('menu_id', $ids);
        return $this;
    }

    public function whereInAuthId(array $auth_id = [])
    {
        $this->query =  $this->query->whereIn('auth_id', $auth_id);
        return $this;
    }

    public function whereMenu(){
        $this->query =  $this->query->where('auth_id', null);
        return $this;
    }



    // public static function all(bool $onlyTree = true,array $menu_id_arr = [])
    // {
    //     $tree = static::loadOrm(Menu::class)
    //         ->where(function($query) use ($menu_id_arr ){
    //             if(!empty($menu_id_arr)) $query->where('menu_id','in',$menu_id_arr);
    //         })
    //         ->orderBy('menu_sort', 'desc')
    //         ->orderBy('menu_id', 'desc')
    //         ->get([
    //             'menu_id',
    //             'menu_pid',
    //             'path',
    //             'name',
    //             'redirect',
    //             'component',
    //             'menu_sort',
    //             'icon',
    //             'title',
    //             'isLink',
    //             'isHide',
    //             'isFull',
    //             'isAffix',
    //             'isKeepAlive'
    //         ]);
    //     return   $onlyTree ? Helper::createTree($tree->toArray(), 'menu_id', 'menu_pid') : $tree->toArray() ;
    // }

    // public static function del(array $params)
    // {
    //     return static::loadOrm(Menu::class)->where('menu_id', $params['menu_id'])->delete();
    // }

    public  function edit(array $params)
    {
        $base = [
            // "menu_pid" => $params['menu_pid'] == 0 ? null : $params['menu_pid'],
            "path" => $params['path'],
            "name" => $params['name'],
            "redirect" => $params['redirect'],
            "component" => $params['component'],
            "icon" => $params['icon'],
            "title" => $params['title'],
            "isLink" => $params['isLink'],
            "isFull" => $params['isFull'],
            "isAffix" => $params['isAffix'],
            "isKeepAlive" => $params['isKeepAlive']
        ];

        if (isset($params['menu_pid']))  $base["menu_pid"] = $params['menu_pid'] == 0 ? null : $params['menu_pid'];

        if (isset($params['isHide']))  $base["isHide"] = $params['isHide'] ;

        if (isset($params['menu_sort']))  $base["menu_sort"] = $params['menu_sort'] ;

        return    $this->query->update($base);
    }


    // public  static  function getMenuAuthTree()
    // {
    //     $all =  static::loadOrm(Menu::class)->with(['auth'])->orderBy('menu_sort', 'desc')->orderBy('menu_id', 'desc')->get([
    //         'menu_id',
    //         'title',
    //         'menu_pid',
    //     ]);
    //     return Helper::createTree($all->toArray(), 'menu_id', 'menu_pid');
    // }

    // public static function checkExists($params)
    // {
    //     return static::loadOrm(Menu::class)->where(
    //         array_merge(
    //             array(['path', $params['path']]),
    //             isset($params['menu_id']) ? array(['menu_id', '<>', $params['menu_id']]) : []
    //         )
    //     )->exists();
    // }
}
