<?php

namespace App\Dao\Logs;

use App\Jobs\MongoLog;
use App\Models\Mongo\Server\ErrorLog;
use Illuminate\Http\Request;
use App\Common\Helper;
use App\Utils\Nanoid\Control as Nanoid;
use Illuminate\Support\Facades\Log;

class ErrorDao extends BaseLogs
{
    /**
     * @description:      异步 添加日志错误日志
     * @param {array} $data
     * @return {*}
     */
    public static function asyncAdd(array $data, Request $request) :array
    {
        $time = time();

        $resultData = self::createData($data, $request, $time);

        $dateYmd =  date('Ymd', $time);

        try {
            dispatch((new MongoLog($dateYmd, $resultData))->onConnection('rabbitmq')->onQueue('log'));
        } catch (\Exception $e) {
            Log::channel('systemerror')->error(json_encode(['data' => $resultData, 'rabbitmq_error' => $e->getMessage()]));
        }

        return ['date' => $dateYmd , 'oid' =>  $resultData['oid'] ];
    }

    public static function add(array $data, Request $request):array
    {
        $time = time();

        $resultData = self::createData($data, $request, $time);

        try {
            //加入mongodb
            $obj = ErrorLog::create(date('Ymd', $time))->insertOne($resultData);

            return ['oid' => $obj->getInsertedId()->__toString() , 'date' => date('Ymd', $time) , 'type' => 'mongo'];

        } catch (\Exception $e) {
            //mongodb 错误时加入记事本

            $nanoid = Nanoid::create();

            $result = ['oid' =>  $nanoid , 'date' => date('Ymd', $time) , 'type' => 'text'];

            Log::channel('systemerror')->error(json_encode( array_merge( $result , [ 'data' => $resultData , 'mongo_error' => $e->getMessage()  ] ) ));

            return $result;

        }


    }


    public static function createData(array $data, Request $request, int $time)
    {

        return  [
            "oid" => Nanoid::create() ,  //日志标识
            "line" => $data["line"],
            "file" => $data["file"],
            "error" => $data["error"],
            "enum" => $data["enum"],
            "params" => $request->input(),
            "full_url" =>  $request->url(),
            "ip" => $request->ip(),
            "method" =>  $request->method(),
            "create_time" => date('Y-m-d H:i:s', $time)
        ];
    }


    /**
     * @description:      查找所有集合
     * @param {array} $data
     * @return {*}
     */
    public static function getTableName()
    {
        $data = [];
        foreach (ErrorLog::create(null)->listCollectionNames() as $val) $data[] = $val;
        return $data;
    }

    /**
     * @description:      查找所有集合
     * @param {array} $data
     * @return {*}
     */
    public static function list($data)
    {
        $arr = [];
        $date = $data['ymd'];
        foreach (ErrorLog::create($date)->find([], [
            'projection' => [
                "_id" => [
                    '$toString' =>  '$_id'
                ],
                "line" => 1,
                "file" => 1,
                "error" => 1,
                "params" => 1,
                "full_url" => 1,
                "enum" => 1,
                "ip" => 1,
                "method" => 1,
                "create_time" => 1,
            ],
            "sort" => [
                "create_time" => -1
            ],
            "skip" => ($data['pageNum'] - 1) * $data['pageSize'],
            "limit" => $data['pageSize']
        ]) as $val) {
            $val = (array)$val;
            $arr[] = $val;
        };
        return $arr;
    }


    public static function total($data)
    {
        $date = $data['ymd'];
        return ErrorLog::create($date)->countDocuments();
    }
}
