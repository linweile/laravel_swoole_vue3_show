<?php

namespace App\Dao;

use Illuminate\Support\Facades\DB;
use App\Models\Mysql\Orm\BaseOrm;
use App\Common\Helper;
use App\Common\Http\Basics\Error;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;
use Closure;

class BaseDao
{


    protected   $query;

    public final function end():Builder|BaseOrm|Collection
    {
        return $this->query;
    }

    public  function pages(int $pageNum,int $pageSize)
    {
        $this->query = $this->query->offset(($pageNum - 1) * $pageSize)->limit($pageSize);
        return $this;
    }


    public final static function transaction(Closure $func)
    {
        DB::transaction(fn () => $func());
    }

    public final  function disableCache()
    {
        $this->query = $this->query->disableCache();
        return $this;
    }

}
