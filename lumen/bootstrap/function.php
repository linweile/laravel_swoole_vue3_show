<?php

function config_path(string $path)
{
    if (substr($path, 0, 1) !== '/')  $path  = '/' . $path;
    return dirname(__DIR__ . $path);
}
