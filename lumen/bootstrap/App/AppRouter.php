<?php
//请求前缀
enum AppRouter
{
  case  ADMIN_API;   //后台请求

  /**
   * @description:  路由前缀
   * @return string
   */
  public function prefix():string
  {
      return match ($this) {
        AppRouter::ADMIN_API => 'admin_api'
      };
  }

  /**
   * @description:  controller命名空间
   * @return string
   */
  public function namespace():string
  {
      return match ($this) {
        AppRouter::ADMIN_API => 'App\Http\Controllers\AdminApi'
      };
  }


   /**
   * @description:  中间件
   * @return array
   */
  public function middleware():array
  {
      return match ($this) {
        AppRouter::ADMIN_API => [
            'adminTokenCheck' => \App\Http\Middleware\AdminTokenCheck::class,
            'adminRefreshToken' => \App\Http\Middleware\AdminRefreshToken::class,
            'adminReferer' => \App\Http\Middleware\Referer\AdminApi::class,
            'adminStatusPass' => \App\Http\Middleware\AdminStatusPass::class,
            'adminFrequency' => \App\Http\Middleware\Frequency\AdminApi::class,
            'devCheck' => \App\Http\Middleware\DevCheck::class,
            'adminCheckApiAuth' => \App\Http\Middleware\AdminCheckApiAuth::class,
            'md5' => \App\Http\Middleware\Sign\Md5::class
        ],
          default => []
      };
  }

   /**
   * @description:  加载的文件
   * @return string
   */
  public function require():string
  {
    return match ($this) {
        AppRouter::ADMIN_API => __DIR__ . '/../../app/Http/Routes/AdminApi.php'
    };
  }

}

