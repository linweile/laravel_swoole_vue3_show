<?php
require_once __DIR__ . '/AppRouter.php';
class AppLoad
{

    public function __construct(
        public readonly Laravel\Lumen\Application $app
    ) {
    }



    public  function services()
    {




        //开启orm
        $this->app->withEloquent();
        //开启门面模式
        $this->app->withFacades();

        $this->app->register(\SwooleTW\Http\LumenServiceProvider::class);

        $this->app->register(\GeneaLabs\LaravelModelCaching\Providers\Service::class);
        $this->app->register(\Illuminate\Redis\RedisServiceProvider::class);

        //表单验证
        $this->app->register(\Pearl\RequestValidate\RequestServiceProvider::class);

        $this->app->register(\VladimirYuldashev\LaravelQueueRabbitMQ\LaravelQueueRabbitMQServiceProvider::class);


        return $this;
    }

    public function configure()
    {
        $this->app->configure('app');
        $this->app->configure('database');
        $this->app->configure('queue');
        $this->app->configure('logging');
        return $this;
    }

    public function singleton()
    {
        $this->app->singleton(
            Illuminate\Contracts\Debug\ExceptionHandler::class,
            App\Exceptions\Handler::class
        );

        $this->app->singleton(
            Illuminate\Contracts\Console\Kernel::class,
            App\Console\Kernel::class
        );
        return $this;
    }

    public  function router()
    {
        foreach (AppRouter::cases() as $val) $this->app->routeMiddleware($val->middleware())->router->group(['namespace' => $val->namespace(), 'prefix' => $val->prefix()], fn ($router) => require $val->require());
        return $this;
    }
}
