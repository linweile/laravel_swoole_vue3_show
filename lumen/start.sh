#!/bin/bash


# 获取当前脚本文件的相对路径
script_relative_path=$0

# 获取当前工作目录的绝对路径
working_directory=$(pwd)

composer_directory="$working_directory/vendor/autoload.php"

# 获取脚本文件的绝对路径
# script_absolute_path=$(cd $(dirname $script_relative_path) && pwd)/$(basename $script_relative_path)

# echo "脚本文件的绝对路径：$script_absolute_path"
if [ ! -f $composer_directory ]; then
    # 运行 composer install
    php  "$working_directory/composer.phar" install
fi

# 执行其他命令
cd $working_directory &&  supervisord -c ./supervisord.conf && /bin/sh



