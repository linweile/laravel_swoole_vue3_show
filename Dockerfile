FROM php:7.2-alpine
LABEL MAINTAINER="linweile<674969619@qq.com>"

ARG DEBIAN_FRONTEND=noninteractive
# 取消命令行交互

USER root
# 切换用户
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apk/repositories



# 编译swoole 所需的环境
RUN apk add  autoconf
RUN apk add  openssl
RUN apk add gcc
RUN apk add make
RUN apk add pkgconf
RUN apk add libxml2-dev
RUN apk add sqlite-dev
RUN apk add oniguruma-dev
RUN apk add curl-dev
RUN apk add openssl-dev
RUN apk add libzip-dev
RUN apk add zlib-dev
RUN apk add libpng-dev
RUN apk add build-base
ADD ./zip/swoole-v4.8.10.tar /
#  -----------

#  -----------  php.ini 所在位置 /etc/php/8.3/cli
RUN cd /swoole-v4.8.10 && phpize && ./configure --enable-openssl --with-openssl-dir=/usr/include/openssl && make && make install
# 编译安装swoole

COPY ./volumes/smproxy/php.ini  /usr/local/etc/php

