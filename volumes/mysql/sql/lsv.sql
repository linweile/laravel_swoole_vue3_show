/*
 Navicat Premium Data Transfer

 Source Server         : lsv_lumen
 Source Server Type    : MySQL
 Source Server Version : 80034
 Source Host           : localhost:9308
 Source Schema         : lsv

 Target Server Type    : MySQL
 Target Server Version : 80034
 File Encoding         : 65001

 Date: 12/02/2024 15:04:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for lsv_admins
-- ----------------------------
DROP TABLE IF EXISTS `lsv_admins`;
CREATE TABLE `lsv_admins`  (
  `username` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '账号',
  `password` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '密码',
  `nickName` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '昵称',
  `realName` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '真实姓名',
  `phone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '手机号',
  `portrait` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '头像',
  `status` tinyint(0) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1启用 0禁用',
  `role_id` int(0) NOT NULL DEFAULT 0 COMMENT '-1开发者 0超级管理员 大于0指定role_id',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `dev_pwd` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '开发者账号密码',
  `admins_id` char(21) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'nanoid',
  UNIQUE INDEX `username_only`(`username`) USING BTREE COMMENT '用户名唯一',
  INDEX `user`(`password`) USING BTREE,
  INDEX `admins_id`(`admins_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lsv_admins
-- ----------------------------
INSERT INTO `lsv_admins` VALUES ('17325764719', 'e10adc3949ba59abbe56e057f20f883e', '222', '312312321', '17325764719', '', 1, 13, '2024-02-05 23:13:25', '2024-02-09 20:00:17', '', 'aqOgqxBuNuLIIeH5BuE4g');
INSERT INTO `lsv_admins` VALUES ('1PuEkrdDHMffngvyMllU', 'a96bb4d13d18f44e891e918cfb3358f1', '开发者', '', '', '', 1, -1, '2023-05-04 03:08:24', '2023-05-05 19:50:30', 'OwcfsooZhYONq4miHVEb', 'cUDTHfZVcXk-ZHmqHOKO2');
INSERT INTO `lsv_admins` VALUES ('22223213124', 'd41d8cd98f00b204e9800998ecf8427e', '222', '222', '17325764719', '', 1, 14, '2024-02-05 22:14:58', '2024-02-06 22:42:10', '', 't0PA7UQgYlCA16QPVDrpk');
INSERT INTO `lsv_admins` VALUES ('321321', '69855c5d1164c5607ab82bb64e6df4cb', '321', '31231', '17325764719', '', 1, 0, '2023-04-20 18:34:38', '2023-04-20 18:34:38', '', 'cUDTHfZVcXk-ZHmqHOKO6');
INSERT INTO `lsv_admins` VALUES ('321321312', 'd66373348acedb8c399cc73061fecee6', '222', '222', '17325764718', 'https://lwl-study.oss-cn-guangzhou.aliyuncs.com/admins/cUDTHfZVcXk-ZHmqHOKO20/9f818923-1791-4be8-9ad3-199c723dcd2b.jpeg', 1, 9, '2024-02-05 23:10:03', '2024-02-05 23:10:03', '', 'PaGmFnqQ6u1Ycz9pzYSCH');
INSERT INTO `lsv_admins` VALUES ('admin', '827ccb0eea8a706c4c34a16891f84e7b', '123444', '123456', '17325764719', 'https://lwl-study.oss-cn-guangzhou.aliyuncs.com/admins/cUDTHfZVcXk-ZHmqHOKO90/f7888ea3-6cab-4074-b0c3-2545c25ccbde.jpeg', 1, 0, '2023-04-24 16:09:56', '2024-02-10 01:11:11', '', 'cUDTHfZVcXk-ZHmqHOKO9');

-- ----------------------------
-- Table structure for lsv_admins_del
-- ----------------------------
DROP TABLE IF EXISTS `lsv_admins_del`;
CREATE TABLE `lsv_admins_del`  (
  `del_id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admins_id` char(21) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'nanoid',
  `username` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '账号',
  `password` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '密码',
  `nickName` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '昵称',
  `realName` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '真实姓名',
  `phone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '手机号',
  `portrait` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '头像',
  `status` tinyint(0) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1启用 0禁用',
  `role_id` int(0) NOT NULL DEFAULT 0 COMMENT '-1开发者 0超级管理员 大于0指定role_id',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `dev_pwd` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '开发者密码',
  PRIMARY KEY (`del_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 138 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lsv_admins_del
-- ----------------------------
INSERT INTO `lsv_admins_del` VALUES (131, 'cUDTHfZVcXk-ZHmqHOKO1', '17325764723', '856589a5cbc83357bf800a7b8864dc89', '22', '2222', '17325764728', '', 1, 9, '2023-10-27 00:30:51', '2023-10-27 00:30:51', '');
INSERT INTO `lsv_admins_del` VALUES (132, 'cUDTHfZVcXk-ZHmqHOKO4', '321312312', 'c6d68eae1f79aed9263c73b19526fd23', '22', '32131231', '17325764718', '', 1, 9, '2023-10-27 00:30:51', '2023-10-27 00:30:51', '');
INSERT INTO `lsv_admins_del` VALUES (133, 'cUDTHfZVcXk-ZHmqHOKO5', '3213123123123', 'ee17d96c644f83b02b0e27b7925666cd', '22', '32131231', '17325764718', '', 1, 9, '2023-10-27 00:30:51', '2023-10-27 00:30:51', '');
INSERT INTO `lsv_admins_del` VALUES (134, 'cUDTHfZVcXk-ZHmqHOKO7', '432432432', '3c3839b6a1c387864ca752e654101a52', '2', '4324324', '17325764719', '', 1, 14, '2023-10-27 00:30:51', '2023-10-27 00:30:51', '');
INSERT INTO `lsv_admins_del` VALUES (135, 'ddft2tSCDzzhtG4y-Zm95', '22232131', 'c8290a560cdbedb92c7e67e7bdaca0a1', '22', '3123123', '17325764718', '', 1, 13, '2023-10-27 00:57:50', '2023-10-27 00:57:50', '');
INSERT INTO `lsv_admins_del` VALUES (136, 'OONu8loTXVOVDHjWn2b8T', '312312312', '52cf16d27b6f17f7d319e0af042f7c5d', '222', '3213123', '17325764719', '', 1, 9, '2024-02-05 23:09:46', '2024-02-05 23:09:46', '');
INSERT INTO `lsv_admins_del` VALUES (137, 'cUDTHfZVcXk-ZHmqHOKO8', 'admin', 'd41d8cd98f00b204e9800998ecf8427e', '123212222222', '22344444', '17325764719', '', 1, 9, '2024-02-05 23:09:51', '2024-02-05 23:09:51', '');
INSERT INTO `lsv_admins_del` VALUES (138, 'd72Gh54BbgYGTxaakd4_Q', '21312321', '2a3f6ba86eb47b275195fd3549205b24', '222', '321312', '17325764719', '', 1, 9, '2024-02-05 23:09:51', '2024-02-05 23:09:51', '');

-- ----------------------------
-- Table structure for lsv_admins_log
-- ----------------------------
DROP TABLE IF EXISTS `lsv_admins_log`;
CREATE TABLE `lsv_admins_log`  (
  `log_id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `params` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '请求参数 序列化',
  `admins_id` char(21) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '管理员id',
  `router_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '前端路由路径',
  `api` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '请求路径',
  `auth_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '操作名称',
  `sql` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'sql 语句 序列化 数组 Array<string>',
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'ip地址',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '生成时间',
  `update_time` datetime(0) NULL DEFAULT NULL,
  `operation_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  `menu_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '栏目名称',
  `enum` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '枚举名称',
  PRIMARY KEY (`log_id`) USING BTREE,
  INDEX `admins_id`(`admins_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 60 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lsv_admins_log
-- ----------------------------
INSERT INTO `lsv_admins_log` VALUES (44, 'a:8:{s:8:\"username\";s:5:\"admin\";s:8:\"nickName\";s:5:\"12321\";s:6:\"status\";s:1:\"1\";s:5:\"phone\";s:11:\"17325764719\";s:8:\"portrait\";s:0:\"\";s:7:\"role_id\";s:1:\"9\";s:9:\"admins_id\";s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";s:8:\"realName\";s:5:\"22344\";}', 'cUDTHfZVcXk-ZHmqHOKO2', '/account/index', 'admin_api/admin/edit', '编辑', 'a:2:{i:0;a:3:{s:3:\"sql\";s:95:\"select exists(select * from `lsv_admins` where `username` = ? and `admins_id` <> ?) as `exists`\";s:6:\"params\";a:2:{i:0;s:5:\"admin\";i:1;s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";}s:4:\"time\";d:0.72;}i:1;a:3:{s:3:\"sql\";s:200:\"update `lsv_admins` set `status` = ?, `username` = ?, `nickName` = ?, `realName` = ?, `phone` = ?, `portrait` = ?, `role_id` = ?, `lsv_admins`.`update_time` = ? where `role_id` > ? and `admins_id` = ?\";s:6:\"params\";a:10:{i:0;s:1:\"1\";i:1;s:5:\"admin\";i:2;s:5:\"12321\";i:3;s:5:\"22344\";i:4;s:11:\"17325764719\";i:5;s:0:\"\";i:6;s:1:\"9\";i:7;s:19:\"2023-10-27 01:11:52\";i:8;i:0;i:9;s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";}s:4:\"time\";d:227.81;}}', '172.16.1.1', '2023-10-27 01:11:52', '2023-10-27 01:11:52', '2023-10-27 01:11:52', '账号管理', 'a:4:{s:5:\"class\";s:30:\"App\\Common\\Http\\Basics\\Success\";s:4:\"name\";s:12:\"EDIT_SUCCESS\";s:12:\"trueCodeName\";s:7:\"SUCCESS\";s:4:\"code\";i:200;}');
INSERT INTO `lsv_admins_log` VALUES (45, 'a:8:{s:8:\"username\";s:5:\"admin\";s:8:\"nickName\";s:8:\"12321222\";s:6:\"status\";s:1:\"1\";s:5:\"phone\";s:11:\"17325764719\";s:8:\"portrait\";s:0:\"\";s:7:\"role_id\";s:1:\"9\";s:9:\"admins_id\";s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";s:8:\"realName\";s:6:\"223444\";}', 'cUDTHfZVcXk-ZHmqHOKO2', '/account/index', 'admin_api/admin/edit', '编辑', 'a:2:{i:0;a:3:{s:3:\"sql\";s:95:\"select exists(select * from `lsv_admins` where `username` = ? and `admins_id` <> ?) as `exists`\";s:6:\"params\";a:2:{i:0;s:5:\"admin\";i:1;s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";}s:4:\"time\";d:64.4;}i:1;a:3:{s:3:\"sql\";s:200:\"update `lsv_admins` set `status` = ?, `username` = ?, `nickName` = ?, `realName` = ?, `phone` = ?, `portrait` = ?, `role_id` = ?, `lsv_admins`.`update_time` = ? where `role_id` > ? and `admins_id` = ?\";s:6:\"params\";a:10:{i:0;s:1:\"1\";i:1;s:5:\"admin\";i:2;s:8:\"12321222\";i:3;s:6:\"223444\";i:4;s:11:\"17325764719\";i:5;s:0:\"\";i:6;s:1:\"9\";i:7;s:19:\"2023-10-27 01:30:04\";i:8;i:0;i:9;s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";}s:4:\"time\";d:38.61;}}', '172.16.1.1', '2023-10-27 01:30:04', '2023-10-27 01:30:04', '2023-10-27 01:30:04', '账号管理', 'a:4:{s:5:\"class\";s:30:\"App\\Common\\Http\\Basics\\Success\";s:4:\"name\";s:12:\"EDIT_SUCCESS\";s:12:\"trueCodeName\";s:7:\"SUCCESS\";s:4:\"code\";i:200;}');
INSERT INTO `lsv_admins_log` VALUES (46, 'a:8:{s:8:\"username\";s:5:\"admin\";s:8:\"nickName\";s:12:\"123212222222\";s:6:\"status\";s:1:\"1\";s:5:\"phone\";s:11:\"17325764719\";s:8:\"portrait\";s:0:\"\";s:7:\"role_id\";s:1:\"9\";s:9:\"admins_id\";s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";s:8:\"realName\";s:6:\"223444\";}', 'cUDTHfZVcXk-ZHmqHOKO2', '/account/index', 'admin_api/admin/edit', '编辑', 'a:2:{i:0;a:3:{s:3:\"sql\";s:95:\"select exists(select * from `lsv_admins` where `username` = ? and `admins_id` <> ?) as `exists`\";s:6:\"params\";a:2:{i:0;s:5:\"admin\";i:1;s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";}s:4:\"time\";d:60.61;}i:1;a:3:{s:3:\"sql\";s:200:\"update `lsv_admins` set `status` = ?, `username` = ?, `nickName` = ?, `realName` = ?, `phone` = ?, `portrait` = ?, `role_id` = ?, `lsv_admins`.`update_time` = ? where `role_id` > ? and `admins_id` = ?\";s:6:\"params\";a:10:{i:0;s:1:\"1\";i:1;s:5:\"admin\";i:2;s:12:\"123212222222\";i:3;s:6:\"223444\";i:4;s:11:\"17325764719\";i:5;s:0:\"\";i:6;s:1:\"9\";i:7;s:19:\"2023-10-27 01:35:03\";i:8;i:0;i:9;s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";}s:4:\"time\";d:110.82;}}', '172.16.1.1', '2023-10-27 01:35:06', '2023-10-27 01:35:06', '2023-10-27 01:35:03', '账号管理', 'a:4:{s:5:\"class\";s:30:\"App\\Common\\Http\\Basics\\Success\";s:4:\"name\";s:12:\"EDIT_SUCCESS\";s:12:\"trueCodeName\";s:7:\"SUCCESS\";s:4:\"code\";i:200;}');
INSERT INTO `lsv_admins_log` VALUES (47, 'a:8:{s:8:\"username\";s:5:\"admin\";s:8:\"nickName\";s:12:\"123212222222\";s:6:\"status\";s:1:\"1\";s:5:\"phone\";s:11:\"17325764719\";s:8:\"portrait\";s:0:\"\";s:7:\"role_id\";s:1:\"9\";s:9:\"admins_id\";s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";s:8:\"realName\";s:7:\"2234444\";}', 'cUDTHfZVcXk-ZHmqHOKO2', '/account/index', 'admin_api/admin/edit', '编辑', 'a:2:{i:0;a:3:{s:3:\"sql\";s:95:\"select exists(select * from `lsv_admins` where `username` = ? and `admins_id` <> ?) as `exists`\";s:6:\"params\";a:2:{i:0;s:5:\"admin\";i:1;s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";}s:4:\"time\";d:66.59;}i:1;a:3:{s:3:\"sql\";s:200:\"update `lsv_admins` set `status` = ?, `username` = ?, `nickName` = ?, `realName` = ?, `phone` = ?, `portrait` = ?, `role_id` = ?, `lsv_admins`.`update_time` = ? where `role_id` > ? and `admins_id` = ?\";s:6:\"params\";a:10:{i:0;s:1:\"1\";i:1;s:5:\"admin\";i:2;s:12:\"123212222222\";i:3;s:7:\"2234444\";i:4;s:11:\"17325764719\";i:5;s:0:\"\";i:6;s:1:\"9\";i:7;s:19:\"2023-10-27 01:35:16\";i:8;i:0;i:9;s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";}s:4:\"time\";d:137.86;}}', '172.16.1.1', '2023-10-27 01:35:19', '2023-10-27 01:35:19', '2023-10-27 01:35:16', '账号管理', 'a:4:{s:5:\"class\";s:30:\"App\\Common\\Http\\Basics\\Success\";s:4:\"name\";s:12:\"EDIT_SUCCESS\";s:12:\"trueCodeName\";s:7:\"SUCCESS\";s:4:\"code\";i:200;}');
INSERT INTO `lsv_admins_log` VALUES (48, 'a:8:{s:8:\"username\";s:5:\"admin\";s:8:\"nickName\";s:12:\"123212222222\";s:6:\"status\";s:1:\"1\";s:5:\"phone\";s:11:\"17325764719\";s:8:\"portrait\";s:0:\"\";s:7:\"role_id\";s:1:\"9\";s:9:\"admins_id\";s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";s:8:\"realName\";s:7:\"2234444\";}', 'cUDTHfZVcXk-ZHmqHOKO2', '/account/index', 'admin_api/admin/edit', '编辑', 'a:2:{i:0;a:3:{s:3:\"sql\";s:95:\"select exists(select * from `lsv_admins` where `username` = ? and `admins_id` <> ?) as `exists`\";s:6:\"params\";a:2:{i:0;s:5:\"admin\";i:1;s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";}s:4:\"time\";d:43.64;}i:1;a:3:{s:3:\"sql\";s:200:\"update `lsv_admins` set `status` = ?, `username` = ?, `nickName` = ?, `realName` = ?, `phone` = ?, `portrait` = ?, `role_id` = ?, `lsv_admins`.`update_time` = ? where `role_id` > ? and `admins_id` = ?\";s:6:\"params\";a:10:{i:0;s:1:\"1\";i:1;s:5:\"admin\";i:2;s:12:\"123212222222\";i:3;s:7:\"2234444\";i:4;s:11:\"17325764719\";i:5;s:0:\"\";i:6;s:1:\"9\";i:7;s:19:\"2023-10-27 01:36:10\";i:8;i:0;i:9;s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";}s:4:\"time\";d:703.32;}}', '172.16.1.1', '2023-10-27 01:36:15', '2023-10-27 01:36:15', '2023-10-27 01:36:11', '账号管理', 'a:4:{s:5:\"class\";s:30:\"App\\Common\\Http\\Basics\\Success\";s:4:\"name\";s:12:\"EDIT_SUCCESS\";s:12:\"trueCodeName\";s:7:\"SUCCESS\";s:4:\"code\";i:200;}');
INSERT INTO `lsv_admins_log` VALUES (49, 'a:8:{s:8:\"username\";s:5:\"admin\";s:8:\"nickName\";s:12:\"123212222222\";s:6:\"status\";s:1:\"1\";s:5:\"phone\";s:11:\"17325764719\";s:8:\"portrait\";s:0:\"\";s:7:\"role_id\";s:1:\"9\";s:9:\"admins_id\";s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";s:8:\"realName\";s:8:\"22344444\";}', 'cUDTHfZVcXk-ZHmqHOKO2', '/account/index', 'admin_api/admin/edit', '编辑', 'a:2:{i:0;a:3:{s:3:\"sql\";s:95:\"select exists(select * from `lsv_admins` where `username` = ? and `admins_id` <> ?) as `exists`\";s:6:\"params\";a:2:{i:0;s:5:\"admin\";i:1;s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";}s:4:\"time\";d:13.14;}i:1;a:3:{s:3:\"sql\";s:200:\"update `lsv_admins` set `status` = ?, `username` = ?, `nickName` = ?, `realName` = ?, `phone` = ?, `portrait` = ?, `role_id` = ?, `lsv_admins`.`update_time` = ? where `role_id` > ? and `admins_id` = ?\";s:6:\"params\";a:10:{i:0;s:1:\"1\";i:1;s:5:\"admin\";i:2;s:12:\"123212222222\";i:3;s:8:\"22344444\";i:4;s:11:\"17325764719\";i:5;s:0:\"\";i:6;s:1:\"9\";i:7;s:19:\"2023-10-29 23:24:53\";i:8;i:0;i:9;s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";}s:4:\"time\";d:39.64;}}', '172.16.1.1', '2023-10-29 23:24:56', '2023-10-29 23:24:56', '2023-10-29 23:24:53', '账号管理', 'a:4:{s:5:\"class\";s:30:\"App\\Common\\Http\\Basics\\Success\";s:4:\"name\";s:12:\"EDIT_SUCCESS\";s:12:\"trueCodeName\";s:7:\"SUCCESS\";s:4:\"code\";i:200;}');
INSERT INTO `lsv_admins_log` VALUES (50, 'a:7:{s:8:\"username\";s:8:\"21312321\";s:8:\"password\";s:32:\"2a3f6ba86eb47b275195fd3549205b24\";s:8:\"nickName\";s:3:\"222\";s:6:\"status\";s:1:\"1\";s:5:\"phone\";s:11:\"17325764719\";s:7:\"role_id\";s:1:\"9\";s:8:\"realName\";s:6:\"321312\";}', 'cUDTHfZVcXk-ZHmqHOKO2', '/account/index', 'admin_api/admin/add', '添加', 'a:2:{i:0;a:3:{s:3:\"sql\";s:74:\"select exists(select * from `lsv_admins` where `username` = ?) as `exists`\";s:6:\"params\";a:1:{i:0;s:8:\"21312321\";}s:4:\"time\";d:140.62;}i:1;a:3:{s:3:\"sql\";s:198:\"insert into `lsv_admins` (`username`, `admins_id`, `password`, `status`, `nickName`, `realName`, `phone`, `role_id`, `dev_pwd`, `update_time`, `create_time`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)\";s:6:\"params\";a:11:{i:0;s:8:\"21312321\";i:1;s:21:\"d72Gh54BbgYGTxaakd4_Q\";i:2;s:32:\"2a3f6ba86eb47b275195fd3549205b24\";i:3;s:1:\"1\";i:4;s:3:\"222\";i:5;s:6:\"321312\";i:6;s:11:\"17325764719\";i:7;s:1:\"9\";i:8;s:0:\"\";i:9;s:19:\"2024-02-05 21:48:28\";i:10;s:19:\"2024-02-05 21:48:28\";}s:4:\"time\";d:109.18;}}', '172.16.1.1', '2024-02-05 21:48:35', '2024-02-05 21:48:35', '2024-02-05 21:48:28', '账号管理', 'a:4:{s:5:\"class\";s:30:\"App\\Common\\Http\\Basics\\Success\";s:4:\"name\";s:11:\"ADD_SUCCESS\";s:12:\"trueCodeName\";s:7:\"SUCCESS\";s:4:\"code\";i:200;}');
INSERT INTO `lsv_admins_log` VALUES (51, 'a:7:{s:8:\"username\";s:9:\"312312312\";s:8:\"password\";s:32:\"52cf16d27b6f17f7d319e0af042f7c5d\";s:8:\"nickName\";s:3:\"222\";s:6:\"status\";s:1:\"1\";s:5:\"phone\";s:11:\"17325764719\";s:7:\"role_id\";s:1:\"9\";s:8:\"realName\";s:7:\"3213123\";}', 'cUDTHfZVcXk-ZHmqHOKO2', '/account/index', 'admin_api/admin/add', '添加', 'a:2:{i:0;a:3:{s:3:\"sql\";s:74:\"select exists(select * from `lsv_admins` where `username` = ?) as `exists`\";s:6:\"params\";a:1:{i:0;s:9:\"312312312\";}s:4:\"time\";d:14.88;}i:1;a:3:{s:3:\"sql\";s:198:\"insert into `lsv_admins` (`username`, `admins_id`, `password`, `status`, `nickName`, `realName`, `phone`, `role_id`, `dev_pwd`, `update_time`, `create_time`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)\";s:6:\"params\";a:11:{i:0;s:9:\"312312312\";i:1;s:21:\"OONu8loTXVOVDHjWn2b8T\";i:2;s:32:\"52cf16d27b6f17f7d319e0af042f7c5d\";i:3;s:1:\"1\";i:4;s:3:\"222\";i:5;s:7:\"3213123\";i:6;s:11:\"17325764719\";i:7;s:1:\"9\";i:8;s:0:\"\";i:9;s:19:\"2024-02-05 22:14:19\";i:10;s:19:\"2024-02-05 22:14:19\";}s:4:\"time\";d:204.39;}}', '172.16.1.1', '2024-02-05 22:14:21', '2024-02-05 22:14:21', '2024-02-05 22:14:20', '账号管理', 'a:4:{s:5:\"class\";s:30:\"App\\Common\\Http\\Basics\\Success\";s:4:\"name\";s:11:\"ADD_SUCCESS\";s:12:\"trueCodeName\";s:7:\"SUCCESS\";s:4:\"code\";i:200;}');
INSERT INTO `lsv_admins_log` VALUES (52, 'a:7:{s:8:\"username\";s:10:\"2222321312\";s:8:\"password\";s:32:\"d41d8cd98f00b204e9800998ecf8427e\";s:8:\"nickName\";s:3:\"222\";s:6:\"status\";s:1:\"1\";s:5:\"phone\";s:11:\"17325764719\";s:7:\"role_id\";s:2:\"14\";s:8:\"realName\";s:3:\"222\";}', 'cUDTHfZVcXk-ZHmqHOKO2', '/account/index', 'admin_api/admin/add', '添加', 'a:2:{i:0;a:3:{s:3:\"sql\";s:74:\"select exists(select * from `lsv_admins` where `username` = ?) as `exists`\";s:6:\"params\";a:1:{i:0;s:10:\"2222321312\";}s:4:\"time\";d:2.16;}i:1;a:3:{s:3:\"sql\";s:198:\"insert into `lsv_admins` (`username`, `admins_id`, `password`, `status`, `nickName`, `realName`, `phone`, `role_id`, `dev_pwd`, `update_time`, `create_time`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)\";s:6:\"params\";a:11:{i:0;s:10:\"2222321312\";i:1;s:21:\"t0PA7UQgYlCA16QPVDrpk\";i:2;s:32:\"d41d8cd98f00b204e9800998ecf8427e\";i:3;s:1:\"1\";i:4;s:3:\"222\";i:5;s:3:\"222\";i:6;s:11:\"17325764719\";i:7;s:2:\"14\";i:8;s:0:\"\";i:9;s:19:\"2024-02-05 22:14:58\";i:10;s:19:\"2024-02-05 22:14:58\";}s:4:\"time\";d:662.16;}}', '172.16.1.1', '2024-02-05 22:15:01', '2024-02-05 22:15:01', '2024-02-05 22:14:59', '账号管理', 'a:4:{s:5:\"class\";s:30:\"App\\Common\\Http\\Basics\\Success\";s:4:\"name\";s:11:\"ADD_SUCCESS\";s:12:\"trueCodeName\";s:7:\"SUCCESS\";s:4:\"code\";i:200;}');
INSERT INTO `lsv_admins_log` VALUES (53, 'a:1:{s:3:\"ids\";a:1:{i:0;s:21:\"OONu8loTXVOVDHjWn2b8T\";}}', 'cUDTHfZVcXk-ZHmqHOKO2', '/account/index', 'admin_api/admin/del', '删除', 'a:3:{i:0;a:3:{s:3:\"sql\";s:82:\"select * from `lsv_admins` where `lsv_admins`.`role_id` > ? and `admins_id` in (?)\";s:6:\"params\";a:2:{i:0;s:1:\"0\";i:1;s:21:\"OONu8loTXVOVDHjWn2b8T\";}s:4:\"time\";d:4.79;}i:1;a:3:{s:3:\"sql\";s:333:\"INSERT INTO `lsv_admins_del` (  `username`,`password`,`nickName`,`realName`,`phone`,`portrait`,`status`,`role_id`,`dev_pwd`,`admins_id`,`create_time`,`update_time` ) VALUES (\'312312312\',\'52cf16d27b6f17f7d319e0af042f7c5d\',\'222\',\'3213123\',\'17325764719\',\'\',\'1\',\'9\',\'\',\'OONu8loTXVOVDHjWn2b8T\',\'2024-02-05 23:09:46\',\'2024-02-05 23:09:46\')\";s:6:\"params\";a:0:{}s:4:\"time\";d:830.27;}i:2;a:3:{s:3:\"sql\";s:80:\"delete from `lsv_admins` where `lsv_admins`.`role_id` > ? and `admins_id` in (?)\";s:6:\"params\";a:2:{i:0;s:1:\"0\";i:1;s:21:\"OONu8loTXVOVDHjWn2b8T\";}s:4:\"time\";d:5.86;}}', '172.16.1.1', '2024-02-05 23:09:48', '2024-02-05 23:09:48', '2024-02-05 23:09:47', '账号管理', 'a:4:{s:5:\"class\";s:30:\"App\\Common\\Http\\Basics\\Success\";s:4:\"name\";s:11:\"DEL_SUCCESS\";s:12:\"trueCodeName\";s:7:\"SUCCESS\";s:4:\"code\";i:200;}');
INSERT INTO `lsv_admins_log` VALUES (54, 'a:1:{s:3:\"ids\";a:2:{i:0;s:21:\"d72Gh54BbgYGTxaakd4_Q\";i:1;s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";}}', 'cUDTHfZVcXk-ZHmqHOKO2', '/account/index', 'admin_api/admin/del', '删除', 'a:3:{i:0;a:3:{s:3:\"sql\";s:85:\"select * from `lsv_admins` where `lsv_admins`.`role_id` > ? and `admins_id` in (?, ?)\";s:6:\"params\";a:3:{i:0;s:1:\"0\";i:1;s:21:\"d72Gh54BbgYGTxaakd4_Q\";i:2;s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";}s:4:\"time\";d:7.53;}i:1;a:3:{s:3:\"sql\";s:498:\"INSERT INTO `lsv_admins_del` (  `username`,`password`,`nickName`,`realName`,`phone`,`portrait`,`status`,`role_id`,`dev_pwd`,`admins_id`,`create_time`,`update_time` ) VALUES (\'admin\',\'d41d8cd98f00b204e9800998ecf8427e\',\'123212222222\',\'22344444\',\'17325764719\',\'\',\'1\',\'9\',\'\',\'cUDTHfZVcXk-ZHmqHOKO8\',\'2024-02-05 23:09:51\',\'2024-02-05 23:09:51\'),(\'21312321\',\'2a3f6ba86eb47b275195fd3549205b24\',\'222\',\'321312\',\'17325764719\',\'\',\'1\',\'9\',\'\',\'d72Gh54BbgYGTxaakd4_Q\',\'2024-02-05 23:09:51\',\'2024-02-05 23:09:51\')\";s:6:\"params\";a:0:{}s:4:\"time\";d:220.31;}i:2;a:3:{s:3:\"sql\";s:83:\"delete from `lsv_admins` where `lsv_admins`.`role_id` > ? and `admins_id` in (?, ?)\";s:6:\"params\";a:3:{i:0;s:1:\"0\";i:1;s:21:\"d72Gh54BbgYGTxaakd4_Q\";i:2;s:21:\"cUDTHfZVcXk-ZHmqHOKO8\";}s:4:\"time\";d:2.34;}}', '172.16.1.1', '2024-02-05 23:09:52', '2024-02-05 23:09:52', '2024-02-05 23:09:52', '账号管理', 'a:4:{s:5:\"class\";s:30:\"App\\Common\\Http\\Basics\\Success\";s:4:\"name\";s:11:\"DEL_SUCCESS\";s:12:\"trueCodeName\";s:7:\"SUCCESS\";s:4:\"code\";i:200;}');
INSERT INTO `lsv_admins_log` VALUES (55, 'a:8:{s:8:\"username\";s:9:\"321321312\";s:8:\"password\";s:32:\"d66373348acedb8c399cc73061fecee6\";s:8:\"nickName\";s:3:\"222\";s:6:\"status\";s:1:\"1\";s:5:\"phone\";s:11:\"17325764718\";s:8:\"portrait\";s:119:\"https://lwl-study.oss-cn-guangzhou.aliyuncs.com/admins/cUDTHfZVcXk-ZHmqHOKO20/9f818923-1791-4be8-9ad3-199c723dcd2b.jpeg\";s:7:\"role_id\";s:1:\"9\";s:8:\"realName\";s:3:\"222\";}', 'cUDTHfZVcXk-ZHmqHOKO2', '/account/index', 'admin_api/admin/add', '添加', 'a:2:{i:0;a:3:{s:3:\"sql\";s:74:\"select exists(select * from `lsv_admins` where `username` = ?) as `exists`\";s:6:\"params\";a:1:{i:0;s:9:\"321321312\";}s:4:\"time\";d:15.62;}i:1;a:3:{s:3:\"sql\";s:213:\"insert into `lsv_admins` (`username`, `admins_id`, `password`, `status`, `nickName`, `realName`, `phone`, `portrait`, `role_id`, `dev_pwd`, `update_time`, `create_time`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)\";s:6:\"params\";a:12:{i:0;s:9:\"321321312\";i:1;s:21:\"PaGmFnqQ6u1Ycz9pzYSCH\";i:2;s:32:\"d66373348acedb8c399cc73061fecee6\";i:3;s:1:\"1\";i:4;s:3:\"222\";i:5;s:3:\"222\";i:6;s:11:\"17325764718\";i:7;s:119:\"https://lwl-study.oss-cn-guangzhou.aliyuncs.com/admins/cUDTHfZVcXk-ZHmqHOKO20/9f818923-1791-4be8-9ad3-199c723dcd2b.jpeg\";i:8;s:1:\"9\";i:9;s:0:\"\";i:10;s:19:\"2024-02-05 23:10:03\";i:11;s:19:\"2024-02-05 23:10:03\";}s:4:\"time\";d:56.53;}}', '172.16.1.1', '2024-02-05 23:10:04', '2024-02-05 23:10:04', '2024-02-05 23:10:03', '账号管理', 'a:4:{s:5:\"class\";s:30:\"App\\Common\\Http\\Basics\\Success\";s:4:\"name\";s:11:\"ADD_SUCCESS\";s:12:\"trueCodeName\";s:7:\"SUCCESS\";s:4:\"code\";i:200;}');
INSERT INTO `lsv_admins_log` VALUES (56, 'a:8:{s:8:\"username\";s:9:\"222312321\";s:8:\"password\";s:32:\"4a1190e982ba1a71cea74bcdb6921b4b\";s:8:\"nickName\";s:3:\"222\";s:6:\"status\";s:1:\"1\";s:5:\"phone\";s:11:\"17325764719\";s:8:\"portrait\";s:119:\"https://lwl-study.oss-cn-guangzhou.aliyuncs.com/admins/cUDTHfZVcXk-ZHmqHOKO20/9f818923-1791-4be8-9ad3-199c723dcd2b.jpeg\";s:7:\"role_id\";s:2:\"13\";s:8:\"realName\";s:9:\"312312321\";}', 'cUDTHfZVcXk-ZHmqHOKO2', '/account/index', 'admin_api/admin/add', '添加', 'a:2:{i:0;a:3:{s:3:\"sql\";s:74:\"select exists(select * from `lsv_admins` where `username` = ?) as `exists`\";s:6:\"params\";a:1:{i:0;s:9:\"222312321\";}s:4:\"time\";d:210.31;}i:1;a:3:{s:3:\"sql\";s:213:\"insert into `lsv_admins` (`username`, `admins_id`, `password`, `status`, `nickName`, `realName`, `phone`, `portrait`, `role_id`, `dev_pwd`, `update_time`, `create_time`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)\";s:6:\"params\";a:12:{i:0;s:9:\"222312321\";i:1;s:21:\"aqOgqxBuNuLIIeH5BuE4g\";i:2;s:32:\"4a1190e982ba1a71cea74bcdb6921b4b\";i:3;s:1:\"1\";i:4;s:3:\"222\";i:5;s:9:\"312312321\";i:6;s:11:\"17325764719\";i:7;s:119:\"https://lwl-study.oss-cn-guangzhou.aliyuncs.com/admins/cUDTHfZVcXk-ZHmqHOKO20/9f818923-1791-4be8-9ad3-199c723dcd2b.jpeg\";i:8;s:2:\"13\";i:9;s:0:\"\";i:10;s:19:\"2024-02-05 23:13:25\";i:11;s:19:\"2024-02-05 23:13:25\";}s:4:\"time\";d:55.82;}}', '172.16.1.1', '2024-02-05 23:13:28', '2024-02-05 23:13:28', '2024-02-05 23:13:25', '账号管理', 'a:4:{s:5:\"class\";s:30:\"App\\Common\\Http\\Basics\\Success\";s:4:\"name\";s:11:\"ADD_SUCCESS\";s:12:\"trueCodeName\";s:7:\"SUCCESS\";s:4:\"code\";i:200;}');
INSERT INTO `lsv_admins_log` VALUES (57, 'a:7:{s:8:\"username\";s:9:\"222312321\";s:8:\"nickName\";s:3:\"222\";s:6:\"status\";s:1:\"1\";s:5:\"phone\";s:11:\"17325764719\";s:7:\"role_id\";s:2:\"13\";s:9:\"admins_id\";s:21:\"aqOgqxBuNuLIIeH5BuE4g\";s:8:\"realName\";s:9:\"312312321\";}', 'cUDTHfZVcXk-ZHmqHOKO2', '/account/index', 'admin_api/admin/edit', '编辑', 'a:2:{i:0;a:3:{s:3:\"sql\";s:95:\"select exists(select * from `lsv_admins` where `username` = ? and `admins_id` <> ?) as `exists`\";s:6:\"params\";a:2:{i:0;s:9:\"222312321\";i:1;s:21:\"aqOgqxBuNuLIIeH5BuE4g\";}s:4:\"time\";d:6.36;}i:1;a:3:{s:3:\"sql\";s:200:\"update `lsv_admins` set `status` = ?, `username` = ?, `nickName` = ?, `realName` = ?, `phone` = ?, `portrait` = ?, `role_id` = ?, `lsv_admins`.`update_time` = ? where `role_id` > ? and `admins_id` = ?\";s:6:\"params\";a:10:{i:0;s:1:\"1\";i:1;s:9:\"222312321\";i:2;s:3:\"222\";i:3;s:9:\"312312321\";i:4;s:11:\"17325764719\";i:5;s:0:\"\";i:6;s:2:\"13\";i:7;s:19:\"2024-02-06 00:47:14\";i:8;i:0;i:9;s:21:\"aqOgqxBuNuLIIeH5BuE4g\";}s:4:\"time\";d:50.58;}}', '172.16.1.1', '2024-02-06 00:47:15', '2024-02-06 00:47:15', '2024-02-06 00:47:14', '账号管理', 'a:4:{s:5:\"class\";s:30:\"App\\Common\\Http\\Basics\\Success\";s:4:\"name\";s:12:\"EDIT_SUCCESS\";s:12:\"trueCodeName\";s:7:\"SUCCESS\";s:4:\"code\";i:200;}');
INSERT INTO `lsv_admins_log` VALUES (58, 'a:8:{s:8:\"username\";s:9:\"222312321\";s:8:\"nickName\";s:3:\"222\";s:6:\"status\";s:1:\"1\";s:5:\"phone\";s:11:\"17325764719\";s:8:\"portrait\";s:119:\"https://lwl-study.oss-cn-guangzhou.aliyuncs.com/admins/cUDTHfZVcXk-ZHmqHOKO20/9f818923-1791-4be8-9ad3-199c723dcd2b.jpeg\";s:7:\"role_id\";s:2:\"13\";s:9:\"admins_id\";s:21:\"aqOgqxBuNuLIIeH5BuE4g\";s:8:\"realName\";s:9:\"312312321\";}', 'cUDTHfZVcXk-ZHmqHOKO2', '/account/index', 'admin_api/admin/edit', '编辑', 'a:2:{i:0;a:3:{s:3:\"sql\";s:95:\"select exists(select * from `lsv_admins` where `username` = ? and `admins_id` <> ?) as `exists`\";s:6:\"params\";a:2:{i:0;s:9:\"222312321\";i:1;s:21:\"aqOgqxBuNuLIIeH5BuE4g\";}s:4:\"time\";d:7.12;}i:1;a:3:{s:3:\"sql\";s:200:\"update `lsv_admins` set `status` = ?, `username` = ?, `nickName` = ?, `realName` = ?, `phone` = ?, `portrait` = ?, `role_id` = ?, `lsv_admins`.`update_time` = ? where `role_id` > ? and `admins_id` = ?\";s:6:\"params\";a:10:{i:0;s:1:\"1\";i:1;s:9:\"222312321\";i:2;s:3:\"222\";i:3;s:9:\"312312321\";i:4;s:11:\"17325764719\";i:5;s:119:\"https://lwl-study.oss-cn-guangzhou.aliyuncs.com/admins/cUDTHfZVcXk-ZHmqHOKO20/9f818923-1791-4be8-9ad3-199c723dcd2b.jpeg\";i:6;s:2:\"13\";i:7;s:19:\"2024-02-06 00:47:20\";i:8;i:0;i:9;s:21:\"aqOgqxBuNuLIIeH5BuE4g\";}s:4:\"time\";d:49.29;}}', '172.16.1.1', '2024-02-06 00:47:21', '2024-02-06 00:47:21', '2024-02-06 00:47:20', '账号管理', 'a:4:{s:5:\"class\";s:30:\"App\\Common\\Http\\Basics\\Success\";s:4:\"name\";s:12:\"EDIT_SUCCESS\";s:12:\"trueCodeName\";s:7:\"SUCCESS\";s:4:\"code\";i:200;}');
INSERT INTO `lsv_admins_log` VALUES (59, 'a:8:{s:8:\"username\";s:11:\"22223213124\";s:8:\"nickName\";s:3:\"222\";s:6:\"status\";s:1:\"1\";s:5:\"phone\";s:11:\"17325764719\";s:8:\"portrait\";s:0:\"\";s:7:\"role_id\";s:2:\"14\";s:9:\"admins_id\";s:21:\"t0PA7UQgYlCA16QPVDrpk\";s:8:\"realName\";s:3:\"222\";}', 'cUDTHfZVcXk-ZHmqHOKO2', '/account/index', 'admin_api/admin/edit', '编辑', 'a:2:{i:0;a:3:{s:3:\"sql\";s:95:\"select exists(select * from `lsv_admins` where `username` = ? and `admins_id` <> ?) as `exists`\";s:6:\"params\";a:2:{i:0;s:11:\"22223213124\";i:1;s:21:\"t0PA7UQgYlCA16QPVDrpk\";}s:4:\"time\";d:26.79;}i:1;a:3:{s:3:\"sql\";s:200:\"update `lsv_admins` set `status` = ?, `username` = ?, `nickName` = ?, `realName` = ?, `phone` = ?, `portrait` = ?, `role_id` = ?, `lsv_admins`.`update_time` = ? where `role_id` > ? and `admins_id` = ?\";s:6:\"params\";a:10:{i:0;s:1:\"1\";i:1;s:11:\"22223213124\";i:2;s:3:\"222\";i:3;s:3:\"222\";i:4;s:11:\"17325764719\";i:5;s:0:\"\";i:6;s:2:\"14\";i:7;s:19:\"2024-02-06 22:42:10\";i:8;i:0;i:9;s:21:\"t0PA7UQgYlCA16QPVDrpk\";}s:4:\"time\";d:508.67;}}', '172.16.1.1', '2024-02-06 22:42:16', '2024-02-06 22:42:16', '2024-02-06 22:42:10', '账号管理', 'a:4:{s:5:\"class\";s:30:\"App\\Common\\Http\\Basics\\Success\";s:4:\"name\";s:12:\"EDIT_SUCCESS\";s:12:\"trueCodeName\";s:7:\"SUCCESS\";s:4:\"code\";i:200;}');
INSERT INTO `lsv_admins_log` VALUES (60, 'a:9:{s:8:\"username\";s:11:\"17325764719\";s:8:\"password\";s:32:\"e10adc3949ba59abbe56e057f20f883e\";s:8:\"nickName\";s:3:\"222\";s:6:\"status\";s:1:\"1\";s:5:\"phone\";s:11:\"17325764719\";s:8:\"portrait\";s:119:\"https://lwl-study.oss-cn-guangzhou.aliyuncs.com/admins/cUDTHfZVcXk-ZHmqHOKO20/9f818923-1791-4be8-9ad3-199c723dcd2b.jpeg\";s:7:\"role_id\";s:2:\"13\";s:9:\"admins_id\";s:21:\"aqOgqxBuNuLIIeH5BuE4g\";s:8:\"realName\";s:9:\"312312321\";}', 'cUDTHfZVcXk-ZHmqHOKO2', '/account/index', 'admin_api/admin/edit', '编辑', 'a:2:{i:0;a:3:{s:3:\"sql\";s:95:\"select exists(select * from `lsv_admins` where `username` = ? and `admins_id` <> ?) as `exists`\";s:6:\"params\";a:2:{i:0;s:11:\"17325764719\";i:1;s:21:\"aqOgqxBuNuLIIeH5BuE4g\";}s:4:\"time\";d:7.25;}i:1;a:3:{s:3:\"sql\";s:216:\"update `lsv_admins` set `status` = ?, `username` = ?, `nickName` = ?, `realName` = ?, `phone` = ?, `portrait` = ?, `role_id` = ?, `password` = ?, `lsv_admins`.`update_time` = ? where `role_id` > ? and `admins_id` = ?\";s:6:\"params\";a:11:{i:0;s:1:\"1\";i:1;s:11:\"17325764719\";i:2;s:3:\"222\";i:3;s:9:\"312312321\";i:4;s:11:\"17325764719\";i:5;s:119:\"https://lwl-study.oss-cn-guangzhou.aliyuncs.com/admins/cUDTHfZVcXk-ZHmqHOKO20/9f818923-1791-4be8-9ad3-199c723dcd2b.jpeg\";i:6;s:2:\"13\";i:7;s:32:\"e10adc3949ba59abbe56e057f20f883e\";i:8;s:19:\"2024-02-09 18:51:40\";i:9;i:0;i:10;s:21:\"aqOgqxBuNuLIIeH5BuE4g\";}s:4:\"time\";d:92.68;}}', '172.16.1.1', '2024-02-09 18:51:48', '2024-02-09 18:51:48', '2024-02-09 18:51:41', '账号管理', 'a:4:{s:5:\"class\";s:30:\"App\\Common\\Http\\Basics\\Success\";s:4:\"name\";s:12:\"EDIT_SUCCESS\";s:12:\"trueCodeName\";s:7:\"SUCCESS\";s:4:\"code\";i:200;}');

-- ----------------------------
-- Table structure for lsv_dev_settings
-- ----------------------------
DROP TABLE IF EXISTS `lsv_dev_settings`;
CREATE TABLE `lsv_dev_settings`  (
  `upload_slice_size` bigint(0) UNSIGNED NOT NULL COMMENT '切片大小 单位kb',
  `upload_max_size` bigint(0) NULL DEFAULT NULL COMMENT '文件上传最大大小',
  `upload_file_type` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '文件类型 [\'type\' => \'image\' , \'name\' => \'图片\'] 序列化',
  `upload_check_file_type` tinyint(0) UNSIGNED NOT NULL COMMENT '是否检查文件上传类型',
  `oss_start` tinyint(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否开启oss校验',
  `oss_access_key_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'oss_access_key_id',
  `oss_access_key_secret` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'oss_access_key_secret',
  `oss_endpoint` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'oss endpoint  https://oss-cn-hangzhou.aliyuncs.com',
  `oss_bucket` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'oss bucket',
  `oss_role_arr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'oss roleArn',
  `prefix_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '图片地址前缀',
  `library_max_capacity` bigint(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '素材库最大容量 单位KB'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lsv_dev_settings
-- ----------------------------
INSERT INTO `lsv_dev_settings` VALUES (100, 204800, 'a:2:{i:0;a:2:{s:4:\"type\";s:40:\"image/png,image/jpg,image/jpeg,image/gif\";s:4:\"name\";s:6:\"图片\";}i:1;a:2:{s:4:\"type\";s:24:\"application/x-msdownload\";s:4:\"name\";s:3:\"exe\";}}', 1, 1, 'LTAI5tCHQATNkYMuCB9XgfBD', 'rkDP1SShsfgszl1fOUHYOLvVwJtpzu', 'guangzhou.aliyuncs.com', 'lwl-study', 'acs:ram::1722093308882937:role/sts', 'https://lwl-study.oss-cn-guangzhou.aliyuncs.com', 1048576);

-- ----------------------------
-- Table structure for lsv_menu
-- ----------------------------
DROP TABLE IF EXISTS `lsv_menu`;
CREATE TABLE `lsv_menu`  (
  `menu_id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_pid` bigint(0) UNSIGNED NULL DEFAULT NULL COMMENT '父id',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '网站路径',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '路由name',
  `redirect` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '重定向不填到子级第一个',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '所属组件 文件路径',
  `menu_sort` int(0) UNSIGNED NOT NULL DEFAULT 50 COMMENT '排序越大越往前显示',
  `icon` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '图标',
  `title` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '标题',
  `isLink` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '是否外链',
  `isHide` tinyint(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否隐藏',
  `isFull` tinyint(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否全屏',
  `isAffix` tinyint(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否在头部tabs显示',
  `isKeepAlive` tinyint(0) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否缓存',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `auth_id` bigint(0) UNSIGNED NULL DEFAULT NULL COMMENT '权限id',
  PRIMARY KEY (`menu_id`) USING BTREE,
  UNIQUE INDEX `only_one_path`(`path`) USING BTREE COMMENT '路径唯一',
  INDEX `menu`(`menu_pid`) USING BTREE,
  INDEX `auth_id`(`auth_id`) USING BTREE,
  CONSTRAINT `auth_id` FOREIGN KEY (`auth_id`) REFERENCES `lsv_menu_auth` (`auth_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `menu` FOREIGN KEY (`menu_pid`) REFERENCES `lsv_menu` (`menu_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 68 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lsv_menu
-- ----------------------------
INSERT INTO `lsv_menu` VALUES (65, NULL, '/user', '', '/account/index', '', 50, 'User', '管理员', '', 0, 0, 0, 1, '2023-04-06 19:52:40', '2023-04-12 20:42:17', NULL);
INSERT INTO `lsv_menu` VALUES (66, 65, '/account/index', 'account', '', '/account/index', 50, 'Avatar', '账号管理', '', 0, 0, 0, 1, '2023-04-06 20:14:56', '2024-02-07 22:43:04', NULL);
INSERT INTO `lsv_menu` VALUES (67, 65, '/account/role', 'role', '', '/account/role/index', 49, 'Lock', '权限管理', '', 0, 0, 0, 1, '2023-04-10 18:59:35', '2024-02-07 01:22:25', NULL);
INSERT INTO `lsv_menu` VALUES (68, 65, '/account/operation', 'operationLog', '', '/account/operation/index', 48, 'Notebook', '操作日志', '', 0, 0, 0, 1, '2023-04-28 18:33:07', '2024-02-07 01:22:36', NULL);

-- ----------------------------
-- Table structure for lsv_menu_auth
-- ----------------------------
DROP TABLE IF EXISTS `lsv_menu_auth`;
CREATE TABLE `lsv_menu_auth`  (
  `auth_id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `auth_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '权限名称  添加删除修改等等操作',
  `menu_id` bigint(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '栏目id',
  `auth_sort` int(0) UNSIGNED NULL DEFAULT 50 COMMENT '权限排序显示',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `common` tinyint(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '1公共 0非公共 接口',
  PRIMARY KEY (`auth_id`) USING BTREE,
  INDEX `menu del`(`menu_id`) USING BTREE,
  CONSTRAINT `menu del` FOREIGN KEY (`menu_id`) REFERENCES `lsv_menu` (`menu_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lsv_menu_auth
-- ----------------------------
INSERT INTO `lsv_menu_auth` VALUES (19, '添加', 67, 50, '2023-04-20 18:16:36', '2023-04-20 18:16:36', 0);
INSERT INTO `lsv_menu_auth` VALUES (20, '编辑', 67, 50, '2023-04-20 18:16:41', '2023-04-20 18:16:41', 0);
INSERT INTO `lsv_menu_auth` VALUES (21, '删除', 67, 50, '2023-04-20 18:16:46', '2023-04-20 18:16:46', 0);
INSERT INTO `lsv_menu_auth` VALUES (23, '设置权限', 67, 50, '2023-04-20 18:17:46', '2023-04-20 18:17:46', 0);
INSERT INTO `lsv_menu_auth` VALUES (24, '添加', 66, 50, '2023-04-20 18:18:35', '2023-04-20 18:18:35', 0);
INSERT INTO `lsv_menu_auth` VALUES (25, '编辑', 66, 50, '2023-04-20 18:18:41', '2023-04-20 18:18:41', 0);
INSERT INTO `lsv_menu_auth` VALUES (26, '删除', 66, 50, '2023-04-20 18:18:46', '2023-06-27 15:24:41', 0);

-- ----------------------------
-- Table structure for lsv_menu_auth_api
-- ----------------------------
DROP TABLE IF EXISTS `lsv_menu_auth_api`;
CREATE TABLE `lsv_menu_auth_api`  (
  `api_id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `api` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '请求api作为主键',
  `menu_id` bigint(0) UNSIGNED NOT NULL COMMENT '栏目id',
  `add_log` tinyint(0) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1加入日志 0不加入',
  `auth_id` bigint(0) UNSIGNED NULL DEFAULT NULL COMMENT '权限id',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `api_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '接口名称 公共接口时调用',
  PRIMARY KEY (`api_id`) USING BTREE,
  UNIQUE INDEX `api menu`(`api`, `menu_id`) USING BTREE COMMENT 'api和栏目id不重复',
  INDEX `auth del`(`auth_id`) USING BTREE,
  INDEX `menu_del`(`menu_id`) USING BTREE,
  CONSTRAINT `auth del` FOREIGN KEY (`auth_id`) REFERENCES `lsv_menu_auth` (`auth_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `menu_del` FOREIGN KEY (`menu_id`) REFERENCES `lsv_menu` (`menu_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lsv_menu_auth_api
-- ----------------------------
INSERT INTO `lsv_menu_auth_api` VALUES (3, 'admin/del', 66, 1, 26, '2023-04-22 00:51:33', '2023-04-25 10:55:36', '');
INSERT INTO `lsv_menu_auth_api` VALUES (9, 'role/set', 67, 1, 23, '2023-04-23 23:35:53', '2023-04-24 00:07:29', '');
INSERT INTO `lsv_menu_auth_api` VALUES (16, 'role/treeData', 67, 0, 23, '2023-04-24 02:39:53', '2023-04-24 02:39:56', '');
INSERT INTO `lsv_menu_auth_api` VALUES (17, 'role/del', 67, 1, 21, '2023-04-24 02:40:22', '2023-04-24 02:40:22', '');
INSERT INTO `lsv_menu_auth_api` VALUES (18, 'role/edit', 67, 1, 20, '2023-04-24 02:40:44', '2023-04-24 02:40:44', '');
INSERT INTO `lsv_menu_auth_api` VALUES (19, 'role/add', 67, 1, 19, '2023-04-24 02:40:52', '2023-04-24 02:40:52', '');
INSERT INTO `lsv_menu_auth_api` VALUES (21, 'admin/edit', 66, 1, 25, '2023-04-25 10:55:54', '2023-04-25 10:55:54', '');
INSERT INTO `lsv_menu_auth_api` VALUES (22, 'admin/add', 66, 1, 24, '2023-04-25 10:56:52', '2023-04-25 10:56:52', '');
INSERT INTO `lsv_menu_auth_api` VALUES (23, 'admin/status', 66, 1, 25, '2023-04-25 10:58:49', '2023-04-25 10:58:49', '');
INSERT INTO `lsv_menu_auth_api` VALUES (38, 'role/list', 67, 0, NULL, '2023-04-27 21:54:15', '2023-04-27 23:10:33', '权限列表');
INSERT INTO `lsv_menu_auth_api` VALUES (39, 'role/all', 66, 0, NULL, '2023-04-28 15:16:33', '2023-04-28 15:16:33', '所有权限');
INSERT INTO `lsv_menu_auth_api` VALUES (40, 'admin/list', 66, 0, NULL, '2023-04-28 15:16:44', '2023-04-28 15:16:44', '列表');
INSERT INTO `lsv_menu_auth_api` VALUES (41, 'operation/log/list', 68, 0, NULL, '2023-04-28 19:58:00', '2023-04-28 19:58:00', '列表');

-- ----------------------------
-- Table structure for lsv_menu_auth_buttons
-- ----------------------------
DROP TABLE IF EXISTS `lsv_menu_auth_buttons`;
CREATE TABLE `lsv_menu_auth_buttons`  (
  `buttons_id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `buttons` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '按钮权限表示',
  `auth_id` bigint(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '权限id',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`buttons_id`) USING BTREE,
  INDEX `auth`(`auth_id`) USING BTREE,
  CONSTRAINT `auth` FOREIGN KEY (`auth_id`) REFERENCES `lsv_menu_auth` (`auth_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lsv_menu_auth_buttons
-- ----------------------------
INSERT INTO `lsv_menu_auth_buttons` VALUES (9, 'delete', 21, '2023-04-20 18:17:15', '2023-04-20 18:17:15');
INSERT INTO `lsv_menu_auth_buttons` VALUES (10, 'edit', 20, '2023-04-20 18:17:32', '2023-04-20 18:17:32');
INSERT INTO `lsv_menu_auth_buttons` VALUES (11, 'setAuth', 23, '2023-04-20 18:17:54', '2023-04-20 18:17:54');
INSERT INTO `lsv_menu_auth_buttons` VALUES (12, 'add', 19, '2023-04-20 18:18:02', '2023-04-20 18:18:02');
INSERT INTO `lsv_menu_auth_buttons` VALUES (13, 'delete', 26, '2023-04-20 18:22:03', '2023-04-22 03:15:22');
INSERT INTO `lsv_menu_auth_buttons` VALUES (14, 'edit', 25, '2023-04-20 18:22:09', '2023-04-20 18:22:09');
INSERT INTO `lsv_menu_auth_buttons` VALUES (15, 'add', 24, '2023-04-20 18:22:16', '2023-04-20 18:22:16');

-- ----------------------------
-- Table structure for lsv_menu_auth_pages
-- ----------------------------
DROP TABLE IF EXISTS `lsv_menu_auth_pages`;
CREATE TABLE `lsv_menu_auth_pages`  (
  `pages_id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'path',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '组件路径',
  `auth_id` bigint(0) UNSIGNED NOT NULL COMMENT '权限id',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`pages_id`) USING BTREE,
  INDEX `auth del`(`auth_id`) USING BTREE,
  CONSTRAINT `lsv_menu_auth_pages_ibfk_1` FOREIGN KEY (`auth_id`) REFERENCES `lsv_menu_auth` (`auth_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lsv_menu_auth_pages
-- ----------------------------
INSERT INTO `lsv_menu_auth_pages` VALUES (5, '321312221', '32132111', 26, '2023-04-22 00:51:41', '2023-04-22 00:51:46');

-- ----------------------------
-- Table structure for lsv_role
-- ----------------------------
DROP TABLE IF EXISTS `lsv_role`;
CREATE TABLE `lsv_role`  (
  `role_id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '权限名称',
  `role_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '权限序列化格式',
  `role_sort` int(0) NOT NULL DEFAULT 50 COMMENT '越大越往前显示 并且只能往上升级',
  `role_describe` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '权限描述',
  `delete_time` datetime(0) NULL DEFAULT NULL COMMENT '软删除',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lsv_role
-- ----------------------------
INSERT INTO `lsv_role` VALUES (1, '321312555', 'a:7:{i:59;a:0:{}i:60;a:2:{i:0;i:18;i:1;i:17;}i:61;a:0:{}i:63;a:0:{}i:65;a:0:{}i:66;a:0:{}i:67;a:0:{}}', 52, '321312', '2023-04-11 16:30:03', '2023-04-11 00:15:52', '2023-04-11 16:30:03');
INSERT INTO `lsv_role` VALUES (2, '321321', 'a:0:{}', 50, '321312', '2023-04-11 16:30:03', '2023-04-11 16:27:52', '2023-04-11 16:30:03');
INSERT INTO `lsv_role` VALUES (3, '321', 'a:0:{}', 50, '', '2023-04-11 16:30:11', '2023-04-11 16:30:08', '2023-04-11 16:30:11');
INSERT INTO `lsv_role` VALUES (4, '321321', 'a:0:{}', 50, '', NULL, '2023-04-11 16:30:15', '2023-04-11 16:30:15');
INSERT INTO `lsv_role` VALUES (5, '321', 'a:0:{}', 50, '', '2023-04-24 02:40:09', '2023-04-11 16:56:15', '2023-04-24 02:40:09');
INSERT INTO `lsv_role` VALUES (6, '5435345', 'a:0:{}', 50, '', NULL, '2023-04-11 16:56:20', '2023-04-11 16:56:20');
INSERT INTO `lsv_role` VALUES (7, '543', 'a:0:{}', 50, '', NULL, '2023-04-11 16:56:22', '2023-04-11 16:56:22');
INSERT INTO `lsv_role` VALUES (8, '11', 'a:0:{}', 50, '', NULL, '2023-04-11 16:56:25', '2023-04-11 16:56:25');
INSERT INTO `lsv_role` VALUES (9, '2233444556622445522', 'a:4:{i:65;a:0:{}i:66;a:2:{i:0;i:26;i:1;i:25;}i:67;a:2:{i:0;i:23;i:1;i:19;}i:68;a:0:{}}', 60, '55', NULL, '2023-04-11 16:56:28', '2023-10-25 23:53:11');
INSERT INTO `lsv_role` VALUES (10, '44', 'a:0:{}', 50, '', NULL, '2023-04-11 16:56:31', '2023-04-11 16:56:31');
INSERT INTO `lsv_role` VALUES (11, '33', 'a:0:{}', 50, '', NULL, '2023-04-11 16:56:34', '2023-04-11 16:56:34');
INSERT INTO `lsv_role` VALUES (12, '54353', 'a:0:{}', 50, '', NULL, '2023-04-11 16:56:38', '2023-04-11 16:56:38');
INSERT INTO `lsv_role` VALUES (13, '43242', 'a:0:{}', 50, '', NULL, '2023-04-11 16:56:46', '2023-04-11 16:56:46');
INSERT INTO `lsv_role` VALUES (14, '%222', 'a:6:{i:59;a:0:{}i:60;a:0:{}i:61;a:0:{}i:63;a:0:{}i:65;a:0:{}i:66;a:3:{i:0;i:26;i:1;i:25;i:2;i:24;}}', 52, '', NULL, '2023-04-11 16:56:50', '2023-04-27 23:14:39');

-- ----------------------------
-- Table structure for lsv_upload_dir
-- ----------------------------
DROP TABLE IF EXISTS `lsv_upload_dir`;
CREATE TABLE `lsv_upload_dir`  (
  `dir_id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dir_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `dir_sort` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `dir_pid` bigint(0) UNSIGNED NULL DEFAULT NULL,
  `admins_id` char(21) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'null代表公共素材库 非0关联后台用户id',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`dir_id`) USING BTREE,
  INDEX `admins_id`(`admins_id`) USING BTREE,
  INDEX `dir_pid`(`dir_pid`) USING BTREE,
  CONSTRAINT `dir_pid` FOREIGN KEY (`dir_pid`) REFERENCES `lsv_upload_dir` (`dir_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lsv_upload_dir
-- ----------------------------

-- ----------------------------
-- Table structure for lsv_upload_dir_file
-- ----------------------------
DROP TABLE IF EXISTS `lsv_upload_dir_file`;
CREATE TABLE `lsv_upload_dir_file`  (
  `file_id` char(21) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'nanoid file_id',
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '文件名称',
  `dir_id` bigint(0) UNSIGNED NOT NULL COMMENT '文件夹id',
  `is_complete` tinyint(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '1完成 0未完成',
  `admins_id` char(21) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '管理员id',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'url路径',
  `path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '绝对路径',
  `sizes` bigint(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小',
  `delete_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`file_id`) USING BTREE,
  INDEX `dir id`(`dir_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lsv_upload_dir_file
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
